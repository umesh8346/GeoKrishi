package com.pathway.geokrishi.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.vision.text.Text;
import com.pathway.geokrishi.ApiController.ResponseDTO.CropCalenderDTO;
import com.pathway.geokrishi.ApiController.ResponseDTO.CropCategoryResponseDto;
import com.pathway.geokrishi.R;
import com.pathway.geokrishi.fragment.CropFragment;

import java.util.ArrayList;
public class CalendarListAdapter extends PagerAdapter {
    ArrayList<CropCalenderDTO.Data> calenderlist;
    int NumOfTabs;
    String excelurl;
    Context mContext;
CropCalenderRecycleAdapter cropCalenderRecycleAdapter;
    LinearLayoutManager mLayoutManager;
    TextView textNodata;
    public CalendarListAdapter(Context mContext,ArrayList<CropCalenderDTO.Data> p, int NumOfTabs, String excelurl) {

        // TODO Auto-generated constructor stub
        this.calenderlist=p;
        this.NumOfTabs=NumOfTabs;
        this.mContext=mContext;
        this.excelurl=excelurl;
    }


    @Override
    public int getCount() {
        return calenderlist.size();
    }

    public Object instantiateItem(ViewGroup container, int position) {
        RecyclerView recycleviewHourWeather;
        ImageView imageofexcel;
        View view;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        view = inflater.inflate(R.layout.calender_layout, null);

        recycleviewHourWeather=(RecyclerView)view.findViewById(R.id.recycleviewHourWeather);
        imageofexcel=(ImageView)view.findViewById(R.id.imageofexcel) ;
        imageofexcel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri
                        .parse(excelurl));
                mContext.startActivity(intent);
            }
        });
        textNodata=(TextView)view.findViewById(R.id.textNodata);
        if(calenderlist.get(position).Crop.size()<=0){
            textNodata.setVisibility(view.getVisibility());
        }
        mLayoutManager = new LinearLayoutManager(mContext);
        cropCalenderRecycleAdapter=new CropCalenderRecycleAdapter(mContext,calenderlist.get(position).Crop);
        recycleviewHourWeather.setLayoutManager(mLayoutManager);
        recycleviewHourWeather.setAdapter(cropCalenderRecycleAdapter);

         ((ViewPager) container).addView(view);
        return view;
}

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ((ViewPager) container).removeView((LinearLayout) object);
    }


    @Override
    public int getItemPosition(Object object) {
        // TODO Auto-generated method stub


        return POSITION_NONE;
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);

    }

}
