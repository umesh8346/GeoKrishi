package com.pathway.geokrishi;

import android.app.Activity;
import android.os.Bundle;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.pathway.geokrishi.adapter.FragmentViewPagerAdapter;


public class IntroActivity extends AppCompatActivity {
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intro_layout);

        viewPager = (ViewPager) findViewById(R.id.pager);
        String[] groupArray = { "1", "2", "3" };

        FragmentViewPagerAdapter adapter = new FragmentViewPagerAdapter(
                getSupportFragmentManager(), groupArray);

        viewPager.setAdapter(adapter);


    }

}
