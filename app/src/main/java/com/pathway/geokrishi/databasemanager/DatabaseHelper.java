package com.pathway.geokrishi.databasemanager;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.pathway.geokrishi.utils.AppConstant;

public class DatabaseHelper extends SQLiteOpenHelper {

    String DistrictSqlTable = "CREATE TABLE IF NOT EXISTS " + AppConstant.DISTRICT_INFO + " ( "
            + AppConstant.ID + " INTEGER PRIMARY KEY, " + AppConstant.DISTRICT_ID + " INTEGER, " + AppConstant.DISTRICT_NAME
            + " TEXT )";



    String VDCSqlTable = "CREATE TABLE IF NOT EXISTS " + AppConstant.VDC_INFO + " ( "
            + AppConstant.ID + " INTEGER PRIMARY KEY, " +  AppConstant.DISTRICT_ID + " INTEGER, "+ AppConstant.VDC_ID + " INTEGER, " + AppConstant.VDC_NAME
            + " TEXT )";

    public DatabaseHelper(Context context) {
        super(context, AppConstant.DATABASE_NAME, null, 1);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DistrictSqlTable);
        db.execSQL(VDCSqlTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
