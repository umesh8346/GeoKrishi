package com.pathway.geokrishi.utils;

interface OnAnimationEndListener {

    public void onAnimationEnd();
}
