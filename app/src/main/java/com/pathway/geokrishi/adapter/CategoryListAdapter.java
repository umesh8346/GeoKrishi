package com.pathway.geokrishi.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pathway.geokrishi.ApiController.ApiConfig;
import com.pathway.geokrishi.ApiController.ResponseDTO.SuitableListResponseDto;
import com.pathway.geokrishi.R;


import java.util.ArrayList;

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.ViewHolder> implements Filterable {

    private LinearLayoutManager layoutManager;
    private ArrayList<SuitableListResponseDto.Data> vegitableCategoryDtos=new ArrayList<>();
    private ArrayList<SuitableListResponseDto.Data> filteredcategoryLogDto=new ArrayList<>();
    SuitableFilter suitableFilter;

    private Context context;
    public CategoryListAdapter(ArrayList<SuitableListResponseDto.Data> vegitableCategoryDtos, Context context){
        this.context = context;
        this.vegitableCategoryDtos = vegitableCategoryDtos;
        this.filteredcategoryLogDto = vegitableCategoryDtos;
        getFilter();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_vegitable_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CategoryListAdapter.ViewHolder holder, int position) {

        holder.title.setText(vegitableCategoryDtos.get(position).getCropName());
        holder.varieties.setText(vegitableCategoryDtos.get(position).getSuitabilityCropVarieties());
        holder.type.setText(vegitableCategoryDtos.get(position).getCropVarietyCategory());
        holder.defination.setText(vegitableCategoryDtos.get(position).getDescription());
        holder.tv_number.setText((position+1)+"");
        holder.tv_type.setText(vegitableCategoryDtos.get(position).getCropVarietyCategory());
        System.out.println("image=="+vegitableCategoryDtos.get(position).getCropImage());
        Glide.with(context)
                .load(ApiConfig.ImageUrl+ vegitableCategoryDtos.get(position).getCropImage())
                .into(holder.image );
    }

    @Override
    public int getItemCount() {
        return filteredcategoryLogDto.size();
    }

    @Override
    public Filter getFilter() {
        if (suitableFilter == null) {
            suitableFilter = new SuitableFilter();
        }

        return suitableFilter;
    }


    public class ViewHolder extends RecyclerView.ViewHolder{


        TextView title,tv_number,tv_type;
        TextView varieties;
        TextView type;
        TextView defination;
        ImageView image;

        public ViewHolder(View view) {
            super(view);
            title = (TextView)view.findViewById(R.id.tv_title);
            tv_number= (TextView)view.findViewById(R.id.tv_number);
            tv_type= (TextView)view.findViewById(R.id.tv_type);
            varieties = (TextView)view.findViewById(R.id.tv_varieties);
            type = (TextView)view.findViewById(R.id.tv_type);
            defination = (TextView)view.findViewById(R.id.tv_defination);
            image = (ImageView) view.findViewById(R.id.iv_image);
            layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false);

        }

        }

    /**
     * Custom filter for friend list
     * Filter content in friend list according to the search text
     */
    private class SuitableFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint!=null && constraint.length()>0) {
                ArrayList<SuitableListResponseDto.Data> tempList = new ArrayList<>();

                // search content in friend list
                for (SuitableListResponseDto.Data suitabeData : vegitableCategoryDtos) {
                    if (suitabeData.getCropValues().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempList.add(suitabeData);
                    }
                }

                filterResults.count = tempList.size();
                filterResults.values = tempList;
            } else {
                filterResults.count = vegitableCategoryDtos.size();
                filterResults.values = vegitableCategoryDtos;
            }

            return filterResults;
        }
        
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredcategoryLogDto = (ArrayList<SuitableListResponseDto.Data>) results.values;
            notifyDataSetChanged();
        }
    }
}
