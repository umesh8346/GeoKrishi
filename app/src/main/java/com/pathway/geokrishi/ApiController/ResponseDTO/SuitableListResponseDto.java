package com.pathway.geokrishi.ApiController.ResponseDTO;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by dell on 5/21/2017.
 */

public class SuitableListResponseDto {

    @SerializedName("Status")
    public int Status;
    @SerializedName("Message")
    public String Message;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public ArrayList<SuitableListResponseDto.Data> getData() {
        return Data;
    }

    public void setData(ArrayList<SuitableListResponseDto.Data> data) {
        Data = data;
    }

    @SerializedName("Data")
    public ArrayList<Data> Data;

    public static class Data {
        public int getCropVarietyCategoryID() {
            return CropVarietyCategoryID;
        }

        public void setCropVarietyCategoryID(int cropVarietyCategoryID) {
            CropVarietyCategoryID = cropVarietyCategoryID;
        }

        public String getCropName() {
            return CropName;
        }

        public void setCropName(String cropName) {
            CropName = cropName;
        }

        public String getCropImage() {
            return CropImage;
        }

        public void setCropImage(String cropImage) {
            CropImage = cropImage;
        }

        public String getCropTypes() {
            return CropTypes;
        }

        public void setCropTypes(String cropTypes) {
            CropTypes = cropTypes;
        }

        public String getCropValues() {
            return CropValues;
        }

        public void setCropValues(String cropValues) {
            CropValues = cropValues;
        }

        public String getDescription() {
            return Description;
        }

        public void setDescription(String description) {
            Description = description;
        }

        public String getSuitabilityCropVarieties() {
            return SuitabilityCropVarieties;
        }

        public void setSuitabilityCropVarieties(String suitabilityCropVarieties) {
            SuitabilityCropVarieties = suitabilityCropVarieties;
        }

        public String getCropVarietyCategory() {
            return CropVarietyCategory;
        }

        public void setCropVarietyCategory(String cropVarietyCategory) {
            CropVarietyCategory = cropVarietyCategory;
        }

        public String getRegion() {
            return Region;
        }

        public void setRegion(String region) {
            Region = region;
        }

        @SerializedName("CropVarietyCategoryID")
        public int CropVarietyCategoryID;
        @SerializedName("CropName")
        public String CropName;
        @SerializedName("CropImage")
        public String CropImage;
        @SerializedName("CropTypes")
        public String CropTypes;
        @SerializedName("CropValues")
        public String CropValues;
        @SerializedName("Description")
        public String Description;
        @SerializedName("SuitabilityCropVarieties")
        public String SuitabilityCropVarieties;
        @SerializedName("CropVarietyCategory")
        public String CropVarietyCategory;
        @SerializedName("Region")
        public String Region;
    }
}
