package com.pathway.geokrishi;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.pathway.geokrishi.utils.AppConstant;

public class AboutActivity extends BaseActivity implements View.OnClickListener{


    TextView textmontoring,textviewphone,textviewmobile,textviewemail,textviewwebsite;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        textmontoring=(TextView)findViewById(R.id.textmontoring);
        textviewphone=(TextView)findViewById(R.id.textviewphone);
                textviewmobile=(TextView)findViewById(R.id.textviewmobile);
        textviewemail=(TextView)findViewById(R.id.textviewemail);
                textviewwebsite=(TextView)findViewById(R.id.textviewwebsite);
        //textmontoring.setText(Html.fromHtml(String.valueOf(getText(R.string.monotoring))));
        textmontoring.setText(Html.fromHtml(String.valueOf(getText(R.string.about_us))));


        textviewphone.setOnClickListener(this);
                textviewmobile.setOnClickListener(this);
        textviewemail.setOnClickListener(this);
                textviewwebsite.setOnClickListener(this);
    }

    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }

    @Override
    protected int getActivityID() {
        return 0;
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.about;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.about_layout;
    }

    @Override
    protected int getDrawerLayout() {
        return 0;
    }

    @Override
    protected int getNavigationView() {
        return 0;
    }

    @Override
    protected Activity getInstance() {
        return null;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        if(v==textviewphone){
            callPhone(AppConstant.phone_no);
        }
                if(v==textviewmobile){
                    callPhone(AppConstant.mobile_no);
                }
        if(v==textviewemail){
            sendEmail("info@db2map.com");
        }
        if(v==textviewwebsite){
            getWebsite("http://www.db2map.com/");
        }

    }

    // website
    public void getWebsite(String website) {
        //System.out.println("Search==" + website);

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(website));
        startActivity(intent);
    }


    // phone
    public void callPhone(String phone) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"
                + phone));
        startActivity(callIntent);
    }

    // email
    public void sendEmail(String email) {

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", email, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback to Geokrishi");
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }



}
