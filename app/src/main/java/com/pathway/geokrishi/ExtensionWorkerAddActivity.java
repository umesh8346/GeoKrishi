package com.pathway.geokrishi;


import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.pathway.geokrishi.ApiController.ApiManager;
import com.pathway.geokrishi.ApiController.ResponseDTO.ProfileResponseDto;
import com.pathway.geokrishi.utils.AppConstant;
import com.pathway.geokrishi.utils.AppUtils;
import com.pathway.geokrishi.utils.CircularProgressButton;

import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class ExtensionWorkerAddActivity extends BaseActivity implements View.OnClickListener {

    Spinner spinnerqualification, spinnerexpertise;
    String qulificationString, expertiseString;
    EditText edittextName, edittextContactdetail, edittextmobile, edittextFarmerno;
    CircularProgressButton circularButton2;
    String nameString, contactString, mobileString, farmernoString, radiointernetvalue, radiophonevalue;
//    RadioGroup radiophonetypeGroup, radiointernetGroup;
//    RadioButton radiosamrt, radioordinary, radiohome, radiomobiledata;
    JsonObject  extensionjson;
    String qualification, expertise;
    int qualificationint, expertiseint;
    Gson gson;
    private Callback<ProfileResponseDto> profileRespnseDtoCallback;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      //  animateButtons();
        init();
        qualificationadpater();
        expertiseadpater();
    }

    private void init() {
        gson = new Gson();
        spinnerqualification = (Spinner) findViewById(R.id.spinnerqualification);
        spinnerexpertise = (Spinner) findViewById(R.id.spinnerexpertise);

        edittextName = (EditText) findViewById(R.id.edittextName);
        edittextContactdetail = (EditText) findViewById(R.id.edittextContactdetail);
        edittextmobile = (EditText) findViewById(R.id.edittextmobile);
        edittextFarmerno = (EditText) findViewById(R.id.edittextFarmerno);
//        radiophonetypeGroup = (RadioGroup) findViewById(R.id.radiophonetypeGroup);
//        radiointernetGroup = (RadioGroup) findViewById(R.id.radiointernetGroup);
//        radiosamrt = (RadioButton) findViewById(R.id.radiosamrt);
//        radioordinary = (RadioButton) findViewById(R.id.radioordinary);
//        radiohome = (RadioButton) findViewById(R.id.radiohome);
//        radiomobiledata = (RadioButton) findViewById(R.id.radiomobiledata);
        circularButton2 = (CircularProgressButton) findViewById(R.id.circularButton1);
        circularButton2.setOnClickListener(this);
        edittextName.addTextChangedListener(new addListenerOnTextChange());
        edittextContactdetail.addTextChangedListener(new addListenerOnTextChange());
        edittextmobile.addTextChangedListener(new addListenerOnTextChange());
        edittextFarmerno.addTextChangedListener(new addListenerOnTextChange());



        profileRespnseDtoCallback = new Callback<ProfileResponseDto>() {
            @Override
            public void onResponse(Call<ProfileResponseDto> call, retrofit2.Response<ProfileResponseDto> response) {
                //AppLog.d(TAG, response.body().toString());
                // pDialog.dismiss();
                if (response.isSuccessful()) {
                    AppUtils.simulateErrorProgress(circularButton2);
                    ProfileResponseDto ProfileResponse = response.body();
                    if (ProfileResponse.getStatus() == 0) {
                        AppUtils.showdailog("User profile Edit successfully",ExtensionWorkerAddActivity.this);
                    } else if (ProfileResponse.getStatus() == 1) {
                        AppUtils.simulateErrorProgress(circularButton2);
                        AppUtils.errordialog(ExtensionWorkerAddActivity.this,ProfileResponse.getMessage());
                    }

                } else {
                    //  AppUtils.errordialog(ProfileActivity.this, AppConstant.Server_Error);

                }
            }

            @Override
            public void onFailure(Call<ProfileResponseDto> call, Throwable t) {
                // pDialog.dismiss();
                AppUtils.errordialog(ExtensionWorkerAddActivity.this, t.getMessage());
                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        };

    }
    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }

    @Override
    protected int getActivityID() {
        return 0;
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.extension_worker;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.extension_layout;
    }

    @Override
    protected int getDrawerLayout() {
        return 0;
    }

    @Override
    protected int getNavigationView() {
        return 0;
    }

    @Override
    protected Activity getInstance() {
        return null;
    }

    public void getstring() {

        nameString = AppUtils.gettextstring(edittextName);
        contactString = AppUtils.gettextstring(edittextContactdetail);
        mobileString = AppUtils.gettextstring(edittextmobile);
        farmernoString = AppUtils.gettextstring(edittextFarmerno);
//
//        int selectedinternetType = radiointernetGroup.getCheckedRadioButtonId();
//        radiointernetvalue = "";
//        if (selectedinternetType == R.id.radiohome) {
//            radiointernetvalue = "1";
//        } else if (selectedinternetType == R.id.radiomobiledata) {
//            radiointernetvalue = "2";
//        }
//
//        int selectphonetyoetype = radiophonetypeGroup.getCheckedRadioButtonId();
//        radiophonevalue = "";
//        if (selectphonetyoetype == R.id.radiosamrt) {
//            radiophonevalue = "1";
//        } else if (selectphonetyoetype == R.id.radioordinary) {
//            radiophonevalue = "2";
//        }


    }

    public boolean isvalidation() {

        getstring();
        if (!AppUtils.isvalid(nameString)) {
            AppUtils.simulateErrorProgress(circularButton2);
            edittextName.setError(AppConstant.required_field);
            return false;
        }

        if (!AppUtils.isvalid(contactString)) {
            AppUtils.simulateErrorProgress(circularButton2);
            edittextContactdetail.setError(AppConstant.required_field);
            return false;
        }
        if (mobileString.length() != 10) {
            AppUtils.simulateErrorProgress(circularButton2);
            edittextmobile.setError(AppConstant.required_field);
            return false;
        }

        if (!AppUtils.isvalid(farmernoString)) {
            AppUtils.simulateErrorProgress(circularButton2);
            edittextFarmerno.setError(AppConstant.required_field);
            return false;
        }

        return true;
    }

    public void qualificationadpater() {
        List<String> qualificattiongroup = Arrays.asList(getResources().getStringArray(R.array.qualification_array));

        ArrayAdapter<String> qualificationadapter = new ArrayAdapter<>(getApplicationContext(),
                R.layout.spinner_list_item, qualificattiongroup);
        spinnerqualification.setAdapter(qualificationadapter);
        spinnerqualification.setSelection(qualificationint);
        spinnerqualification.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub

                qulificationString = String.valueOf(spinnerqualification
                        .getSelectedItem());
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

    }

    public void expertiseadpater() {
        List<String> expertisergroup = Arrays.asList(getResources().getStringArray(R.array.sector_array));

        ArrayAdapter<String> expertiseadapter = new ArrayAdapter<>(getApplicationContext(),
                R.layout.spinner_list_item, expertisergroup);
        spinnerexpertise.setAdapter(expertiseadapter);
        spinnerexpertise.setSelection(expertiseint);
        spinnerexpertise.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub

                expertiseString = String.valueOf(spinnerexpertise
                        .getSelectedItem());
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

    }


    @Override
    public void onClick(View v) {
        if (v == circularButton2) {
            if (circularButton2.getProgress() == 0) {


            } else {
                circularButton2.setProgress(0);
            }
            if (isvalidation()) {

                extensionjson();
                System.out.println("extensionjson==="+extensionjson);
                ApiManager.UserProfileResponse( profileRespnseDtoCallback,"2", extensionjson);
            }
        }

    }

    public void extensionjson() {

        extensionjson = new JsonObject();

        try {
            extensionjson.addProperty("UserID", AppUtils.userId(getApplicationContext()));
            extensionjson.addProperty("Name", nameString);
            extensionjson.addProperty("ContactDetails", contactString);
            extensionjson.addProperty("Qualification", qulificationString);
            extensionjson.addProperty("Expertise", expertiseString);
            extensionjson.addProperty("MobileNumber", mobileString);
//            extensionjson.addProperty("PhoneType", Integer.valueOf(radiophonevalue));
//            extensionjson.addProperty("Internet", Integer.valueOf(radiointernetvalue));
            extensionjson.addProperty("NoOfFarmers", Integer.valueOf(farmernoString));

        } catch (Exception ex) {

        }

    }


    private class addListenerOnTextChange implements TextWatcher {
        public addListenerOnTextChange() {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            circularButton2.setProgress(0);
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
