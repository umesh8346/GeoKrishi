package com.pathway.geokrishi.adapter;


import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.pathway.geokrishi.R;
import java.util.ArrayList;

public class CropAdpter extends BaseAdapter {
    private Context mContext;
    public ArrayList<String> selections1 = new ArrayList<String>();
    public CropAdpter(Context c,  ArrayList<String> selections1) {
        this.mContext = c;
        this.selections1=selections1;
    }

    @Override
    public int getCount() {
        return selections1.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textviewCropname;
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.horizonatal_item_layout, null);
        textviewCropname=(TextView)view.findViewById(R.id.textviewCropname);
        textviewCropname.setText(selections1.get(position));
        return view;

    }
}
