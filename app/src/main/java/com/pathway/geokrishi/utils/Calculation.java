package com.pathway.geokrishi.utils;



public class Calculation {
     public static double ureaCalculation(double nitrogen, double phose){
         double urea=0.0;

         urea=2.17*(nitrogen-(0.39*phose));
         return urea;
     }

     public static  double dapcalculation(double phose){
         double dap=0.0;
         dap=2.17*phose;
         return dap;

     }

      public static double mopcalculation(double potassium){
          double mop=0.0;
          mop=1.667*potassium;
          return mop;
      }

       public static double getnpksoil(double npksoil){
           double npk=0.0;
           npk=npksoil*(2*10000);
           return npk;
       }

       public  static double getdoublevalue(String getdoublevalue){
           double npksoilvalue=0.0;
           npksoilvalue=Double.parseDouble(getdoublevalue);
           return npksoilvalue;
       }
}
