package com.pathway.geokrishi.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pathway.geokrishi.ApiController.ResponseDTO.CommentResponseDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.SuitableCropsDetail;
import com.pathway.geokrishi.R;

import java.util.ArrayList;

public class CommentAddAdapter extends BaseAdapter {
Context mcontext;
    ArrayList<CommentResponseDto.Data> Commentlist = new ArrayList<>();
public CommentAddAdapter(Context c, ArrayList<CommentResponseDto.Data> Commentlist ){
  this.mcontext=c;
    this.Commentlist=Commentlist;
}
    @Override
    public int getCount() {
        return Commentlist.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder mHolder;
        View view;
        LayoutInflater inflater = (LayoutInflater) mcontext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = new View(mcontext);
        view = inflater.inflate(R.layout.comment_item_layout, null);
        mHolder = new ViewHolder();
        mHolder.imageviewprofile=(ImageView)view.findViewById(R.id.imageviewprofile);
        mHolder.textviewUsername=(TextView)view.findViewById(R.id.textviewUsername);
        mHolder. textviewprofessional=(TextView)view.findViewById(R.id.textviewprofessional);
        mHolder.textviewDate=(TextView)view.findViewById(R.id.textviewDate);
        mHolder.textviewComments=(TextView)view.findViewById(R.id.textviewComments);
        mHolder.textviewUserlocationinfo=(TextView)view.findViewById(R.id.textviewUserlocationinfo);
        mHolder.textviewUsername.setText(Commentlist.get(position).getUserName());

        mHolder.textviewDate.setText(Commentlist.get(position).getCommentDate());
        mHolder. textviewComments.setText(Commentlist.get(position).getCommentText());


        return view;
    }

    private class ViewHolder {

        private ImageView imageviewprofile;
        private TextView textviewUsername,textviewprofessional,textviewDate,textviewComments,textviewUserlocationinfo;

    }
}
