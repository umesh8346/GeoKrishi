package com.pathway.geokrishi;


import android.animation.ValueAnimator;
import android.app.Activity;

import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.pathway.geokrishi.ApiController.ApiManager;
import com.pathway.geokrishi.ApiController.ResponseDTO.ProfileResponseDto;
import com.pathway.geokrishi.databasemanager.DatabaseManager;
import com.pathway.geokrishi.dtos.DistrictInfo;
import com.pathway.geokrishi.utils.AppConstant;
import com.pathway.geokrishi.utils.AppUtils;
import com.pathway.geokrishi.utils.CircularProgressButton;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;


public class CommunityActivity extends BaseActivity implements View.OnClickListener {

    AutoCompleteTextView autoCompletedistrict;
    Spinner spinnersector, spinnerfarmer;
    String farmertype, sectortype, districtstring, internetstring, emailString;
    RadioGroup radiointernetGroup;
    RadioButton radiono, radioyes;
    DatabaseManager db;
    ArrayList<DistrictInfo> districtlist;

    EditText edittextCommunityName, edittextWard, edittextEmail, edittextVdc, edittextContactdetail,
            edittextContactName, edittextContactPhone, edittextContactmobile, edittextExtentionworker, edittextServiceArea, edittextFarmerno;

    String communityanameString, wardString, vdcString, commnunityorganizationString, contactdetailString, contactnameString, contactphoneString,
            contactmobileString, extensionworkerString, serviceareaString, farmernoString;
    Gson gson;
    String servicesector, assosiation;
    int sectorint, assositationint;
    CircularProgressButton circularButton2;
    JsonObject userjson, communityjson;
    private Callback<ProfileResponseDto> profileRespnseDtoCallback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //animateButtons();
        init();

        districtadapter();
    }

    private void init() {
        db = new DatabaseManager(this);
        gson = new Gson();


        if (db.getdistrictcount() < 1) {
            System.out.println("hello");
            new Districttask().execute();
        }

        circularButton2 = (CircularProgressButton) findViewById(R.id.circularButton1);

        circularButton2.setOnClickListener(this);

        autoCompletedistrict = (AutoCompleteTextView) findViewById(R.id.autoCompletedistrict);
        spinnersector = (Spinner) findViewById(R.id.spinnersector);
        spinnerfarmer = (Spinner) findViewById(R.id.spinnerfarmer);
        radiointernetGroup = (RadioGroup) findViewById(R.id.radiointernetGroup);

        radiono = (RadioButton) findViewById(R.id.radiono);
        radioyes = (RadioButton) findViewById(R.id.radioyes);
        edittextEmail = (EditText) findViewById(R.id.edittextEmail);
        edittextCommunityName = (EditText) findViewById(R.id.edittextCommunityName);
        edittextWard = (EditText) findViewById(R.id.edittextWard);
        edittextVdc = (EditText) findViewById(R.id.edittextVdc);

        edittextContactdetail = (EditText) findViewById(R.id.edittextContactdetail);
        edittextContactName = (EditText) findViewById(R.id.edittextContactName);
        edittextContactPhone = (EditText) findViewById(R.id.edittextContactPhone);
        edittextContactmobile = (EditText) findViewById(R.id.edittextContactmobile);
        edittextExtentionworker = (EditText) findViewById(R.id.edittextExtentionworker);
        edittextServiceArea = (EditText) findViewById(R.id.edittextServiceArea);
        edittextFarmerno = (EditText) findViewById(R.id.edittextFarmerno);


        autoCompletedistrict.addTextChangedListener(new addListenerOnTextChange());
        edittextEmail.addTextChangedListener(new addListenerOnTextChange());
        edittextCommunityName.addTextChangedListener(new addListenerOnTextChange());
        edittextFarmerno.addTextChangedListener(new addListenerOnTextChange());
        edittextContactdetail.addTextChangedListener(new addListenerOnTextChange());
        edittextContactName.addTextChangedListener(new addListenerOnTextChange());
        edittextContactPhone.addTextChangedListener(new addListenerOnTextChange());
        edittextContactmobile.addTextChangedListener(new addListenerOnTextChange());
        edittextExtentionworker.addTextChangedListener(new addListenerOnTextChange());
        edittextFarmerno.addTextChangedListener(new addListenerOnTextChange());
        edittextWard.addTextChangedListener(new addListenerOnTextChange());
        profileRespnseDtoCallback = new Callback<ProfileResponseDto>() {
            @Override
            public void onResponse(Call<ProfileResponseDto> call, retrofit2.Response<ProfileResponseDto> response) {
                //AppLog.d(TAG, response.body().toString());
                // pDialog.dismiss();
                if (response.isSuccessful()) {

                    ProfileResponseDto ProfileResponse = response.body();
                    if (ProfileResponse.getStatus() == 0) {
                        AppUtils.simulateSuccessProgress(circularButton2);
                        AppUtils.showdailog("User profile Edit successfully", CommunityActivity.this);
                    } else if (ProfileResponse.getStatus() == 1) {
                        AppUtils.simulateErrorProgress(circularButton2);
                        AppUtils.errordialog(CommunityActivity.this, ProfileResponse.getMessage());
                    }

                } else {
                    //  AppUtils.errordialog(ProfileActivity.this, AppConstant.Server_Error);

                }
            }

            @Override
            public void onFailure(Call<ProfileResponseDto> call, Throwable t) {
                // pDialog.dismiss();
                AppUtils.errordialog(CommunityActivity.this, t.getMessage());
                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        };


        settextedittext();
    }

    public void settextedittext() {
        edittextCommunityName.setText(ProfileActivity.profiledata.getCommunityName());
        edittextWard.setText(ProfileActivity.profiledata.getWard() + "");
        edittextVdc.setText(ProfileActivity.profiledata.getVDC());
        autoCompletedistrict.setText(ProfileActivity.profiledata.getDistrict());
        edittextContactdetail.setText(ProfileActivity.profiledata.getContact_Details());
        edittextContactName.setText(ProfileActivity.profiledata.getContactName());
        edittextContactPhone.setText(ProfileActivity.profiledata.getContactPhone());
        edittextContactmobile.setText(ProfileActivity.profiledata.getContactMobile());
        edittextExtentionworker.setText(ProfileActivity.profiledata.getNoOfExtensionWorker() + "");
        edittextFarmerno.setText(ProfileActivity.profiledata.getNo_Of_Farmers() + "");
        edittextServiceArea.setText(ProfileActivity.profiledata.getServiceAreaNoOfWards() + "");
        servicesector = ProfileActivity.profiledata.getServiceSector();
        edittextEmail.setText(ProfileActivity.profiledata.getEmail());
        assosiation = ProfileActivity.profiledata.getAssociation();

        if (ProfileActivity.profiledata.getInternet() == 1) {
            radioyes.setChecked(true);
        } else {
            radiono.setChecked(true);
        }

        for (int i = 0; i < Arrays.asList(getResources().getStringArray(R.array.sector_array)).size(); i++) {
            if (servicesector.equals(Arrays.asList(getResources().getStringArray(R.array.sector_array)).get(i))) {
                sectorint = i;
            }

        }

        for (int j = 0; j < Arrays.asList(getResources().getStringArray(R.array.farmer_group_array)).size(); j++) {
            if (assosiation.equals(Arrays.asList(getResources().getStringArray(R.array.farmer_group_array)).get(j))) {
                assositationint = j;
            }

        }
        farmeradpater();
        sectoradpater();

    }

    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }

    @Override
    protected int getActivityID() {
        return 0;
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.community_organization_profile;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.community_layout;
    }

    @Override
    protected int getDrawerLayout() {
        return 0;
    }

    @Override
    protected int getNavigationView() {
        return 0;
    }

    @Override
    protected Activity getInstance() {
        return null;
    }


    public void farmeradpater() {
        List<String> farmergroup = Arrays.asList(getResources().getStringArray(R.array.farmer_group_array));

        ArrayAdapter<String> farmeradapter = new ArrayAdapter<>(getApplicationContext(),
                R.layout.spinner_list_item, farmergroup);
        spinnerfarmer.setAdapter(farmeradapter);
        spinnerfarmer.setSelection(assositationint);
        spinnerfarmer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub

                farmertype = String.valueOf(spinnerfarmer
                        .getSelectedItem());
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

    }


    public void sectoradpater() {
        List<String> sectorgroup = Arrays.asList(getResources().getStringArray(R.array.sector_array));

        ArrayAdapter<String> sectoradapter = new ArrayAdapter<>(getApplicationContext(),
                R.layout.spinner_list_item, sectorgroup);
        spinnersector.setAdapter(sectoradapter);
        spinnersector.setSelection(sectorint);

        spinnersector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub


                sectortype = String.valueOf(spinnersector
                        .getSelectedItem());


            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

    }

    @Override
    public void onClick(View v) {


        if (v == circularButton2) {
            if (circularButton2.getProgress() == 0) {


            } else {
                circularButton2.setProgress(0);
            }
            submit();

        }


    }

    public void districtadapter() {
        districtlist = new ArrayList<>();
        districtlist = db.getalldistrict();
        final ArrayAdapter<DistrictInfo> districtistadapter = new ArrayAdapter<>(
                getApplicationContext(), R.layout.spinner_list_item,
                districtlist);

        autoCompletedistrict.setAdapter(districtistadapter);

        autoCompletedistrict.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {

                districtstring = districtistadapter.getItem(arg2).getId();

            }
        });


    }

    public void submit() {
        if (AppUtils.checkInternetConnection(this)) {
            if (isvalidation()) {
                if (!autoCompletedistrict.getText().toString().equals("")) {

                    String districtcity = autoCompletedistrict.getText().toString().toLowerCase();
                    String upperStringdistricy = districtcity.substring(0, 1).toUpperCase() + districtcity.substring(1);

                    for (int i = 0; i < districtlist.size(); i++) {
                        if (upperStringdistricy.equals(districtlist.get(i).getDistrictname())) {
                            districtstring = districtlist.get(i).getDistrictname();
                        }
                    }

                    if (districtstring != null) {
                        communityjson();
                        ApiManager.UserProfileResponse(profileRespnseDtoCallback, "3", communityjson);

                    } else {
                        AppUtils.errordialog(CommunityActivity.this, "please check the district");
                    }
                }
            }
        } else {

        }

    }

    public void getString() {
        emailString = AppUtils.gettextstring(edittextEmail);
        communityanameString = AppUtils.gettextstring(edittextCommunityName);
        wardString = AppUtils.gettextstring(edittextWard);
        vdcString = AppUtils.gettextstring(edittextVdc);
        contactdetailString = AppUtils.gettextstring(edittextContactdetail);
        contactnameString = AppUtils.gettextstring(edittextContactName);
        contactphoneString = AppUtils.gettextstring(edittextContactPhone);
        contactmobileString = AppUtils.gettextstring(edittextContactmobile);
        extensionworkerString = AppUtils.gettextstring(edittextExtentionworker);
        serviceareaString = AppUtils.gettextstring(edittextServiceArea);
        farmernoString = AppUtils.gettextstring(edittextFarmerno);
        int selectinternettype = radiointernetGroup.getCheckedRadioButtonId();
        internetstring = "";

        if (selectinternettype == R.id.radiono) {
            internetstring = "0";
        } else if (selectinternettype == R.id.radioyes) {
            internetstring = "1";
        }

    }

    public boolean isvalidation() {

        getString();
        if (!AppUtils.isvalid(communityanameString)) {
            AppUtils.simulateErrorProgress(circularButton2);
            edittextCommunityName.setError(getText(R.string.reduired_field));
            return false;
        }
        if (!AppUtils.isvalid(autoCompletedistrict.getText().toString())) {
            AppUtils.simulateErrorProgress(circularButton2);
            autoCompletedistrict.setError(getText(R.string.reduired_field));
            return false;
        }

        if (!AppUtils.isvalid(wardString)) {
            AppUtils.simulateErrorProgress(circularButton2);
            edittextWard.setError(getText(R.string.reduired_field));
            return false;
        }

        if (!AppUtils.isvalid(vdcString)) {
            AppUtils.simulateErrorProgress(circularButton2);
            edittextVdc.setError(getText(R.string.reduired_field));
            return false;
        }
        if (!AppUtils.isvalid(contactdetailString)) {
            edittextContactdetail.setError(getText(R.string.reduired_field));
            return false;
        }
        if (!AppUtils.isvalid(contactnameString)) {
            AppUtils.simulateErrorProgress(circularButton2);
            edittextContactName.setError(getText(R.string.reduired_field));
            return false;
        }

        if (contactphoneString.length() < 7) {
            AppUtils.simulateErrorProgress(circularButton2);
            edittextContactPhone.setError(getText(R.string.reduired_field));
            return false;
        }
        if (contactmobileString.length() != 10) {
            AppUtils.simulateErrorProgress(circularButton2);
            edittextContactmobile.setError(getText(R.string.reduired_field));
            return false;
        }
        if (!AppUtils.isEmailValid(emailString)) {
            AppUtils.simulateErrorProgress(circularButton2);
            edittextEmail.setError(getText(R.string.reduired_field));
            return false;
        }

        if (!AppUtils.isvalid(serviceareaString)) {
            AppUtils.simulateErrorProgress(circularButton2);
            edittextServiceArea.setError(getText(R.string.reduired_field));
            return false;
        }
        if (!AppUtils.isvalid(farmernoString)) {
            AppUtils.simulateErrorProgress(circularButton2);
            edittextFarmerno.setError(getText(R.string.reduired_field));
            return false;
        }
        return true;
    }

    public void communityjson() {
        communityjson = new JsonObject();
        try {
            communityjson.addProperty(AppConstant.UserID,"6");
            communityjson.addProperty(AppConstant.CommunityName, communityanameString);
            communityjson.addProperty(AppConstant.District, districtstring);
            communityjson.addProperty(AppConstant.VDC, vdcString);
            communityjson.addProperty(AppConstant.Ward, Integer.valueOf(wardString));
            communityjson.addProperty(AppConstant.Contact_Details, contactdetailString);
            communityjson.addProperty(AppConstant.Contact_Name, contactnameString);
            communityjson.addProperty(AppConstant.Contact_Phone, contactphoneString);
            communityjson.addProperty(AppConstant.Contact_Mobile, contactmobileString);
            communityjson.addProperty(AppConstant.email, emailString);
            communityjson.addProperty(AppConstant.Latitude, "27.5");
            communityjson.addProperty(AppConstant.Longitude, "85.98");
            communityjson.addProperty(AppConstant.Association, farmertype);
            communityjson.addProperty(AppConstant.Internet_Facility, Integer.valueOf(internetstring));
            communityjson.addProperty(AppConstant.Service_sector, sectortype);
            communityjson.addProperty(AppConstant.No_of_extension_worker, Integer.valueOf(extensionworkerString));
            communityjson.addProperty(AppConstant.Service_area_No_Of_Wards, Integer.valueOf(serviceareaString));
            communityjson.addProperty(AppConstant.No_of_farmers, Integer.valueOf(farmernoString));
        } catch (Exception ex) {

        }


    }

    private class Districttask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... unused) {
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(
                        new InputStreamReader(getAssets().open("district.csv")));

                // do reading, usually loop until end of file reading
                String mLine;

                while ((mLine = reader.readLine()) != null) {
                    //process line
                    db.insertdistrict(mLine);
                }

            } catch (IOException e) {
                //log the exception
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        //log the exception
                    }
                }
            }
            return ""; // don't interact with the ui!
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

        }
    }

    private class addListenerOnTextChange implements TextWatcher {
        public addListenerOnTextChange() {

        }
        @Override
        public void afterTextChanged(Editable s) {
            districtstring = null;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            districtstring = null;
            circularButton2.setProgress(0);
        }
    }

}
