
package com.pathway.geokrishi.Openweather.WeatherHourDTO;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Snow implements Parcelable
{

    @SerializedName("3h")
    @Expose
    private Double _3h;
    public final static Creator<Snow> CREATOR = new Creator<Snow>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Snow createFromParcel(Parcel in) {
            Snow instance = new Snow();
            instance._3h = ((Double) in.readValue((Double.class.getClassLoader())));
            return instance;
        }

        public Snow[] newArray(int size) {
            return (new Snow[size]);
        }

    }
    ;

    public Double get3h() {
        return _3h;
    }

    public void set3h(Double _3h) {
        this._3h = _3h;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(_3h);
    }

    public int describeContents() {
        return  0;
    }

}
