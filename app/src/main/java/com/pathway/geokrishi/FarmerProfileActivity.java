package com.pathway.geokrishi;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pathway.geokrishi.ApiController.ApiManager;
import com.pathway.geokrishi.ApiController.CustomDialogs;
import com.pathway.geokrishi.ApiController.ResponseDTO.ProfileResponseDto;
import com.pathway.geokrishi.dtos.Item;
import com.pathway.geokrishi.utils.AppConstant;
import com.pathway.geokrishi.utils.AppUtils;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class FarmerProfileActivity extends  BaseActivity implements View.OnClickListener{

ImageButton imageButtonEdituser,ImagebuttonCamera;
TextView textviewUsername,textage,textviewNooffarmer,textviewlandholding,
        textviewmobile,textviewgender,textviewusername,textviewtotalpost,textviewjoindate;
    String picturepath;
    static final int REQUEST_TAKE_PHOTO = 1;
    private static int RESULT_LOAD_IMAGE = 2;
    private Callback<ProfileResponseDto> profileRespnseDtoCallback;
ImageView imageviewprofile;
    Dialog profiledailog;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }
     public void init(){
         profiledailog = CustomDialogs.progressDialog(FarmerProfileActivity.this, "Loading");
         imageButtonEdituser=(ImageButton)findViewById(R.id.imageButtonEdituser);
         ImagebuttonCamera=(ImageButton)findViewById(R.id.ImagebuttonCamera);
         imageviewprofile=(ImageView) findViewById(R.id.imageviewprofile);
         textviewtotalpost=(TextView)findViewById(R.id.textviewtotalpost);
         textviewjoindate=(TextView)findViewById(R.id.textviewjoindate);
         imageButtonEdituser.setOnClickListener(this);
         ImagebuttonCamera.setOnClickListener(this);
         textviewUsername=(TextView)findViewById(R.id.textviewUsername);
         textviewusername=(TextView)findViewById(R.id.textviewusername);
         textage=(TextView)findViewById(R.id.textage);
         textviewNooffarmer=(TextView)findViewById(R.id.textviewnumberfarmer);
         textviewlandholding=(TextView)findViewById(R.id.textviewlandholding);
//         textviewrepresentiverelation=(TextView)findViewById(R.id.textviewrepresentiverelation);
         textviewmobile=(TextView)findViewById(R.id.textviewmobile);
         textviewgender=(TextView)findViewById(R.id.textviewgender);
         textviewNooffarmer.setText(String.valueOf(ProfileActivity.profiledata.getNoOfFamilyMembers()));
         textviewlandholding.setText(ProfileActivity.profiledata.getLandHoldingUnitArea()+" "+ProfileActivity.profiledata.getLandUnit());
//         textviewrepresentiverelation.setText(ProfileActivity.profiledata.getFarmerRepresentativeRelation());
         textviewmobile.setText(ProfileActivity.profiledata.getMobile());
         if(ProfileActivity.profiledata.getGender().equals("M")){
             textviewgender.setText("Male");
         }
         else{
             textviewgender.setText("Female");
         }

         if(!ProfileActivity.profiledata.getAvatarImage().equals("http://")) {
             {
                 Glide.with(this)
                         .load(ProfileActivity.profiledata.getAvatarImage())
                         .into(imageviewprofile);
             }
         }
         else{
             imageviewprofile.setImageResource(R.drawable.ic_profiley);
         }


         textviewtotalpost.setText(String.valueOf(ProfileActivity.profiledata.getNumberOfComments()));
         textviewjoindate.setText(ProfileActivity.profiledata.getJoinedDate());
         textviewUsername.setText(ProfileActivity.profiledata.getFarmerName());
         textviewusername.setText(ProfileActivity.profiledata.getFarmerName());
         textage.setText(ProfileActivity.profiledata.getAge());
         profileRespnseDtoCallback = new Callback<ProfileResponseDto>() {
             @Override
             public void onResponse(Call<ProfileResponseDto> call, retrofit2.Response<ProfileResponseDto> response) {
                 //AppLog.d(TAG, response.body().toString());
                 // pDialog.dismiss();
                 if (response.isSuccessful()) {
                     profiledailog.dismiss();
                     ProfileResponseDto ProfileResponse = response.body();
                     if (ProfileResponse.getStatus() == 0) {

                         AppUtils.showdailog("User profile Edit successfully", FarmerProfileActivity.this);
                     } else if (ProfileResponse.getStatus() == 1) {

                         AppUtils.errordialog(FarmerProfileActivity.this, ProfileResponse.getMessage());
                     }

                 } else {
                     profiledailog.dismiss();
                     AppUtils.errordialog(FarmerProfileActivity.this, AppConstant.Server_Error);

                 }
             }

             @Override
             public void onFailure(Call<ProfileResponseDto> call, Throwable t) {
                 profiledailog.dismiss();
                 AppUtils.errordialog(FarmerProfileActivity.this, t.getMessage());
                 // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

             }
         };

     }

    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }

    @Override
    protected int getActivityID() {
        return 0;
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.extension_worker;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.farmerprofile_layout;
    }

    @Override
    protected int getDrawerLayout() {
        return 0;
    }

    @Override
    protected int getNavigationView() {
        return 0;
    }

    @Override
    protected Activity getInstance() {
        return null;
    }

    @Override
    public void onClick(View v) {
        if(v==imageButtonEdituser){
            startActivity(new Intent(this, FarmerActivity.class));
        }
        if(v==ImagebuttonCamera){
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            else {
                showAlertDialogs();
            }
        }

    }

    private void showAlertDialogs() {
        final Item[] items = {
                new Item("Camera", android.R.drawable.ic_menu_camera),

                new Item("Gallery", android.R.drawable.ic_menu_gallery),

        };

        ListAdapter adapter = new ArrayAdapter<Item>(getApplicationContext(),
                android.R.layout.select_dialog_item, android.R.id.text1, items) {
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                TextView tv = (TextView) v.findViewById(android.R.id.text1);
                tv.setCompoundDrawablesWithIntrinsicBounds(
                        items[position].icon, 0, 0, 0);
                int dp5 = (int) (5 * getResources().getDisplayMetrics().density + 0.5f);
                tv.setCompoundDrawablePadding(dp5);
                return v;
            }
        };

        new AlertDialog.Builder(FarmerProfileActivity.this, R.style.AppCompatAlertDialogStyle).setTitle("  Choose Option")
                .setAdapter(adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        // TODO Auto-generated method stub
                        SelectItem(item);
                    }

                }).show();

    }

    public void SelectItem(int item) {
        switch (item) {
            case 0:
                takephoto();
                break;
            case 1:
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, RESULT_LOAD_IMAGE);
                break;

        }


    }

    private void takephoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK
                && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturepath = cursor.getString(columnIndex);
            cursor.close();
            uploadimageintoserver();
        }
        if (requestCode == REQUEST_TAKE_PHOTO
                && resultCode == Activity.RESULT_OK) {
            uploadimageintoserver();
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date(0));
        String imageFileName = "Jpeg_" + timeStamp + "_";
        String storageDir = Environment.getExternalStorageDirectory()
                + "/picupload";
        File dir = new File(storageDir);
        if (!dir.exists())
            dir.mkdir();

        File image = new File(storageDir + "/" + imageFileName + ".jpg");


        picturepath = image.getAbsolutePath();
        System.out.println("image path==="+picturepath);
        return image;
    }
    public void uploadimageintoserver() {
        File file = new File(picturepath);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part userimage =
                MultipartBody.Part.createFormData("File", file.getName(), requestFile);

        Map<String, RequestBody> map = new HashMap<>();
        map.put("UserID",AppUtils.toRequestBody(AppUtils.userId(FarmerProfileActivity.this)));
        profiledailog.show();
        ApiManager.UserProfileImageResponse(profileRespnseDtoCallback, map, userimage);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(FarmerProfileActivity.this, MainActivity.class));
        finish();
    }
}
