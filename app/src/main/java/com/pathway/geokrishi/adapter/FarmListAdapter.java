package com.pathway.geokrishi.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.pathway.geokrishi.fragment.NPKCalculationFragment;



public class FarmListAdapter extends FragmentStatePagerAdapter {

    int NumOfTabs;
    public FarmListAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.NumOfTabs=NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        return NPKCalculationFragment.newInstance();
    }

    @Override
    public int getCount() {
        return NumOfTabs;
    }
}
