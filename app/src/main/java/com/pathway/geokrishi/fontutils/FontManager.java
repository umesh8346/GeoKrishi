package com.pathway.geokrishi.fontutils;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by dell on 5/20/2017.
 */

public class FontManager {


    public static Typeface getTypeface(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "icomoon.woff");
    }

}