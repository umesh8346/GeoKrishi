package com.pathway.geokrishi.ApiController.ResponseDTO;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CropCalenderDTO {
    @SerializedName("Status")
    public int Status;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public ArrayList<Data> Data;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public ArrayList<CropCalenderDTO.Data> getData() {
        return Data;
    }

    public void setData(ArrayList<CropCalenderDTO.Data> data) {
        Data = data;
    }

    public static class Crop {
        @SerializedName("CropName")
        public String CropName;

        public String getCropName() {
            return CropName;
        }

        public void setCropName(String cropName) {
            CropName = cropName;
        }
    }

    public static class Data {
        @SerializedName("Month")
        public String Month;
        @SerializedName("Crop")
        public ArrayList<Crop> Crop;

        public String getMonth() {
            return Month;
        }

        public void setMonth(String month) {
            Month = month;
        }
    }
}
