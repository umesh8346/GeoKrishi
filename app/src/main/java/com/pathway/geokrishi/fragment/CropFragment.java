package com.pathway.geokrishi.fragment;

import android.content.Intent;
import android.net.Network;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.pathway.geokrishi.ApiController.ApiManager;
import com.pathway.geokrishi.ApiController.Networkcontroller;
import com.pathway.geokrishi.ApiController.ResponseDTO.SuitableListResponseDto;
import com.pathway.geokrishi.ChangepasswordActivity;
import com.pathway.geokrishi.DetailActivity;
import com.pathway.geokrishi.Interface.NetworkInterFace;
import com.pathway.geokrishi.R;
import com.pathway.geokrishi.adapter.CategoryListAdapter;
import com.pathway.geokrishi.utils.AppConstant;
import com.pathway.geokrishi.utils.AppUtils;
import com.pathway.geokrishi.utils.ItemClickSupport;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CropFragment extends Fragment implements NetworkInterFace {
    String cropid;
    CategoryListAdapter vegetableListAdapter;
    ArrayList<SuitableListResponseDto.Data> vegetableList = new ArrayList<>();
    RecyclerView recyclerView;
    NetworkInterFace networkInterFace;
    TextView tv_des,textview;
    private Callback<SuitableListResponseDto> suitableCropResponseDtoCallback;
    SwipeRefreshLayout swipeContainer;
    public static CropFragment newInstance(int id) {
        // TODO Auto-generated method stub
        CropFragment fragment = new CropFragment();
        Bundle bundle = new Bundle();
        bundle.putString("Cropid", String.valueOf(id));
        fragment.setArguments(bundle);
        System.out.println("cropid==="+id);
        return fragment;
    } @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Bundle bundle = getArguments();
        cropid = bundle.getString("Cropid");
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method
        View view = inflater.inflate(R.layout.croptool_layout, null);
        textview=(TextView)view.findViewById(R.id.textview) ;
        networkInterFace=this;
        tv_des= (TextView)view.findViewById(R.id.tv_des);
        recyclerView=(RecyclerView)view.findViewById(R.id.rv_vegitable_categoty) ;

//        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
//        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//
//                ApiManager.SuitableCropsTool(suitableCropResponseDtoCallback,cropid,AppUtils.apilanguage(getActivity()));
//            }
//        });
//
//        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
//                android.R.color.holo_green_light,
//                android.R.color.holo_orange_light,
//                android.R.color.holo_red_light);

        suitableCropResponseDtoCallback  = new Callback<SuitableListResponseDto>() {
            @Override
            public void onResponse(Call<SuitableListResponseDto> call, Response<SuitableListResponseDto> response) {
                if (response.isSuccessful()) {
//                    swipeContainer.setRefreshing(false);
                    vegetableList = new ArrayList<>();
                    SuitableListResponseDto suitableListResponseDto = response.body();
                    if (suitableListResponseDto.getStatus()==0) {

                        if(suitableListResponseDto.getData().size()<=0){
                          AppUtils.errordialog(getActivity(),"There is no data");
                            tv_des.setText("Found no suitable argoproduct");

                        }
                        else {
                            textview.setVisibility(View.VISIBLE);
                            tv_des.setText(getText(R.string.found)+" " +suitableListResponseDto.getData().size()+" "+getText(R.string.suitable_crop));
                            vegetableList = suitableListResponseDto.getData();
                            initRecyclerView();
                        }

                    } else if(suitableListResponseDto.getStatus()==1){
                       // swipeContainer.setRefreshing(false);
                        AppUtils.errordialog(getActivity(), suitableListResponseDto.getMessage());
                    }

                } else {
                   // swipeContainer.setRefreshing(false);
                    AppUtils.errordialog(getActivity(), AppConstant.Server_Error);
                }

            }

            @Override
            public void onFailure(Call<SuitableListResponseDto> call, Throwable t) {
            }
        };
        Networkcontroller.checkInternetConnection(getActivity(),this,"crop");

        return view;
    }

    private void initRecyclerView() {
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        vegetableListAdapter = new CategoryListAdapter(vegetableList, getActivity());
        recyclerView.setAdapter(vegetableListAdapter);
        vegetableListAdapter.notifyDataSetChanged();
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {

                Intent intent = new Intent(getActivity(),
                        DetailActivity.class);
                intent.putExtra("Cropid", vegetableList.get(position).getCropVarietyCategoryID()+"");
                intent.putExtra("value", vegetableList.get(position).getCropVarietyCategory()+"");
                startActivity(intent);
            }
        });
    }


    @Override
    public void network(Boolean aBoolean,String s) {
        if(aBoolean){
            ApiManager.SuitableCropsTool(suitableCropResponseDtoCallback,cropid,AppUtils.apilanguage(getActivity()));
        }
        else{
            AppUtils.errordialog(getActivity(), AppConstant.no_network);
        }
    }
}
