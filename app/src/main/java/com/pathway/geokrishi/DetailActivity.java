package com.pathway.geokrishi;


import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.pathway.geokrishi.ApiController.ApiConfig;
import com.pathway.geokrishi.ApiController.ApiManager;
import com.pathway.geokrishi.ApiController.ResponseDTO.CommentResponseDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.SuitableCropsDetail;
import com.pathway.geokrishi.Interface.EditInterface;
import com.pathway.geokrishi.adapter.CommentAdapter;
import com.pathway.geokrishi.adapter.CommentAddAdapter;
import com.pathway.geokrishi.adapter.CropRequirementAdpter;
import com.pathway.geokrishi.utils.AppConstant;
import com.pathway.geokrishi.utils.AppUtils;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailActivity extends BaseActivity implements EditInterface,View.OnClickListener{

    TextView textviewCropname, textviewCroptype,textviewcropcont,textviewtitle,textvalue;

    ListView listviewComment, croplistviewCroprequirement;
    Dialog commentdailog,feedbackdailog;
    ImageView imageviewCrop;
    ArrayList<SuitableCropsDetail.Containers> ContainerList = new ArrayList<>();
    ArrayList<SuitableCropsDetail.Comment> Commentlist = new ArrayList<>();
    ArrayList<CommentResponseDto.Data> NewCommentlist = new ArrayList<>();
    CommentAdapter commentAdapter;
    CommentAddAdapter commentAddedAdapter;

    String CropcatagoryId;
    CropRequirementAdpter cropRequirementAdpter;
    EditText edittextCropvalue;
    String CropvalueString,CropcontainerString,CropidString;
    Button btnSubmit,btnComment,btncommentsubmit;

    private Callback<SuitableCropsDetail> suitableCropDetailResponseDtoCallback;
    private Callback<CommentResponseDto> commentResponseDtoCallback;
    private Callback<CommentResponseDto> RemarkResponseDtoCallback;
    String valueString;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CropcatagoryId = getIntent().getExtras().getString("Cropid");
        valueString= getIntent().getExtras().getString("value");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    public void init() {
        textviewCropname = (TextView) findViewById(R.id.textviewCropname);
        textviewCroptype = (TextView) findViewById(R.id.textviewCroptype);
        listviewComment = (ListView) findViewById(R.id.listviewComment);
        textvalue= (TextView) findViewById(R.id.textvalue);
        btnComment=(Button)findViewById(R.id.btnComment) ;
        btnComment.setOnClickListener(this);
        croplistviewCroprequirement = (ListView) findViewById(R.id.croplistviewCroprequirement);

        imageviewCrop = (ImageView) findViewById(R.id.imageviewCrop);

        suitableCropDetailResponseDtoCallback = new Callback<SuitableCropsDetail>() {
            @Override
            public void onResponse(Call<SuitableCropsDetail> call, Response<SuitableCropsDetail> response) {
                if (response.isSuccessful()) {

                    SuitableCropsDetail suitableDetailListResponseDto = response.body();
                    if (suitableDetailListResponseDto.getStatus()==0) {

                        ContainerList = suitableDetailListResponseDto.getData().getContainers();
                        Commentlist = suitableDetailListResponseDto.getData().getComment();
                        textviewCropname.setText(suitableDetailListResponseDto.getData().CropName);
                        textviewCroptype.setText(suitableDetailListResponseDto.getData().getSuitableVarieties());
                        textvalue.setText(valueString);
                        Glide.with(DetailActivity.this)
                                .load(ApiConfig.ImageUrl+ suitableDetailListResponseDto.getData().getCropImage())
                                .into(imageviewCrop );
                        croprequirementAdapter();
                        commentadapter();
                        return;

                    } else if (suitableDetailListResponseDto.getStatus() == 1) {


                       // commentdailog.dismiss();
                       AppUtils.errordialog(DetailActivity.this, suitableDetailListResponseDto.getMessage());

                    }

                } else {
                    //commentdailog.dismiss();
                    AppUtils.errordialog(DetailActivity.this, AppConstant.Server_Error);
                }

            }

            @Override
            public void onFailure(Call<SuitableCropsDetail> call, Throwable t) {

            }
        };


        commentResponseDtoCallback = new Callback<CommentResponseDto>() {
            @Override
            public void onResponse(Call<CommentResponseDto> call, Response<CommentResponseDto> response) {
                if (response.isSuccessful()) {

                    CommentResponseDto commentResponseDto = response.body();
                    if (commentResponseDto.getStatus() == 0) {
                        NewCommentlist = commentResponseDto.getData();
                        commentaddadapter();

                    } else if (commentResponseDto.getStatus() == 1) {
                        // dialog.dismiss();
                        AppUtils.errordialog(DetailActivity.this, commentResponseDto.getMessage());

                    }

                } else {
                    // dialog.dismiss();
                    AppUtils.errordialog(DetailActivity.this, AppConstant.Server_Error);
                }

            }

            @Override
            public void onFailure(Call<CommentResponseDto> call, Throwable t) {

            }
        };



        RemarkResponseDtoCallback = new Callback<CommentResponseDto>() {
            @Override
            public void onResponse(Call<CommentResponseDto> call, Response<CommentResponseDto> response) {
                if (response.isSuccessful()) {

                    CommentResponseDto commentResponseDto = response.body();
                    if (commentResponseDto.getStatus() == 0) {
                        AppUtils.errordialog(DetailActivity.this, commentResponseDto.getMessage());
                        feedbackdailog.dismiss();
                    } else if (commentResponseDto.getStatus() == 1) {
                        feedbackdailog.dismiss();
                        AppUtils.errordialog(DetailActivity.this, commentResponseDto.getMessage());

                    }

                } else {
                    feedbackdailog.dismiss();
                    AppUtils.errordialog(DetailActivity.this, AppConstant.Server_Error);
                }

            }

            @Override
            public void onFailure(Call<CommentResponseDto> call, Throwable t) {

            }
        };



        callapi();

    }

    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }

    @Override
    protected int getActivityID() {
        return 0;
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.app_name;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.detail_layout;
    }
    @Override
    protected int getDrawerLayout() {
        return 0;
    }

    @Override
    protected int getNavigationView() {
        return 0;
    }

    @Override
    protected Activity getInstance() {
        return null;
    }

    @Override
    public void EditButtonListner(View view, int position) {
        CropvalueString=ContainerList.get(position).getContainerName();
                CropcontainerString=ContainerList.get(position).getContainerValue();
        CropidString=position+"";
        FeedbackLayout();
    }

    public void croprequirementAdapter() {
        cropRequirementAdpter = new CropRequirementAdpter(getApplicationContext(), this, ContainerList);
        croplistviewCroprequirement.setAdapter(cropRequirementAdpter);
    }

    public void commentadapter() {
        commentAdapter = new CommentAdapter(this, Commentlist);
        listviewComment.setAdapter(commentAdapter);

    }



    public void commentaddadapter() {
        commentAddedAdapter = new CommentAddAdapter(this, NewCommentlist);
        listviewComment.setAdapter(commentAddedAdapter);

    }


    public void callapi() {
        if (AppUtils.checkInternetConnection(DetailActivity.this)) {

            ApiManager.SuitableCropsDetail(suitableCropDetailResponseDtoCallback, CropcatagoryId,AppUtils.apilanguage(getApplicationContext()));

        } else {

        }
    }


    public void FeedbackLayout() {

        feedbackdailog= new Dialog(DetailActivity.this);
        feedbackdailog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        feedbackdailog.setContentView(R.layout.feedback_layout);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(feedbackdailog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        edittextCropvalue=(EditText)feedbackdailog.findViewById(R.id.edittextCropvalue);
        textviewcropcont=(TextView)feedbackdailog.findViewById(R.id.textviewcropcont);
        btnSubmit=(Button)feedbackdailog.findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);
        edittextCropvalue.setText(CropcontainerString);
        textviewcropcont.setText(CropvalueString);
        feedbackdailog.getWindow().setAttributes(lp);
        feedbackdailog.show();
    }

    @Override
    public void onClick(View v) {
        if(v==btnSubmit){
            if(isvalidation()){
                if (AppUtils.checkInternetConnection(DetailActivity.this)) {
                    ApiManager.UserRemark(RemarkResponseDtoCallback, AppUtils.userId(DetailActivity.this),CropcatagoryId,
                            CropvalueString, AppUtils.gettextstring(edittextCropvalue));
                }
            }

        }
        if(v==btnComment){
            commentlayout();
        }
        if(v==btncommentsubmit){
            if(isvalidation()){
                if (AppUtils.checkInternetConnection(DetailActivity.this)) {
                    commentdailog.dismiss();

                    ApiManager.UserComment(commentResponseDtoCallback, AppUtils.userId(DetailActivity.this),CropcatagoryId,
                            AppUtils.gettextstring(edittextCropvalue),"5");
                }
            }
        }

    }

    public Boolean isvalidation(){
        if(!AppUtils.isvalid(AppUtils.gettextstring(edittextCropvalue))){
            edittextCropvalue.setError(AppConstant.required_field);
            return false;
        }
        return true;

    }


    public void commentlayout() {
        commentdailog = new Dialog(DetailActivity.this);
        commentdailog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        commentdailog.setContentView(R.layout.feedback_layout);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(commentdailog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        edittextCropvalue=(EditText)commentdailog.findViewById(R.id.edittextCropvalue);
        textviewcropcont=(TextView)commentdailog.findViewById(R.id.textviewcropcont);
        textviewcropcont.setVisibility(View.GONE);
        textviewtitle=(TextView)commentdailog.findViewById(R.id.textviewtitle);
        textviewtitle.setText(getText(R.string.Comment));
        btncommentsubmit=(Button)commentdailog.findViewById(R.id.btnSubmit);
        btncommentsubmit.setOnClickListener(this);
        commentdailog.getWindow().setAttributes(lp);
        commentdailog.show();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}