package com.pathway.geokrishi.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.pathway.geokrishi.Interface.Sidemenu;
import com.pathway.geokrishi.MainActivity;
import com.pathway.geokrishi.R;

import java.util.EventListener;


/**
 * Created by kishor on 15/05/17.
 */

public class SettingFragment extends Fragment implements View.OnClickListener{
    private Sidemenu listener;
    public SettingFragment(){

    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        try
        {
            listener = ( Sidemenu)activity;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString() + "must implement OnDeviceSelectedListner");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.comming_soon_layout, container, false);


        listener.sendDataToActivity("abc");
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }



    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

        }
    }


}
