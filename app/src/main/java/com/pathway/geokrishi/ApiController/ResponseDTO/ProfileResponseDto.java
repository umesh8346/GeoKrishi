package com.pathway.geokrishi.ApiController.ResponseDTO;

import com.google.gson.annotations.SerializedName;


public class ProfileResponseDto {
    @SerializedName("Status")

    private Integer status;
    @SerializedName("Message")

    private String message;
    @SerializedName("Data")

    private Object data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
