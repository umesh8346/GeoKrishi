package com.pathway.geokrishi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import android.widget.Spinner;
import android.widget.TextView;


import com.pathway.geokrishi.MainDashBoard.DashboardActivity;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class SettingActivity extends BaseActivity {


    Spinner languagespinner;
    String languageString, lang;

    TextView texttext;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        texttext = (TextView) findViewById(R.id.texttext);
        languagespinner = (Spinner) findViewById(R.id.languagespinner);
        languageadpater();
        SharedPreferences sh_PrefN = this.getSharedPreferences(
                "Language Credential", Context.MODE_PRIVATE);
        String language = sh_PrefN.getString("LANGUAGE", "");
        System.out.println("language1==" + language);

        if (language.equals("en")) {
            languagespinner.setSelection(0);

        } else if (language.equals("np")) {

            languagespinner.setSelection(1);
        }


    }

    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }

    @Override
    protected int getActivityID() {
        return 0;
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.setting;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.setting_layout;
    }

    @Override
    protected int getDrawerLayout() {
        return 0;
    }

    @Override
    protected int getNavigationView() {
        return 0;
    }

    @Override
    protected Activity getInstance() {
        return null;
    }


    public void languageadpater() {
        List<String> Language = Arrays.asList(getResources().getStringArray(R.array.Language));

        ArrayAdapter<String> languageadapter = new ArrayAdapter<>(getApplicationContext(),
                R.layout.spinner_list_item, Language);
        languagespinner.setAdapter(languageadapter);
        languagespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub


                languageString = String.valueOf(languagespinner
                        .getSelectedItem());


                System.out.println("spinner_engNepChoose="
                        + languageString);

                if (languageString.equals("English")) {
                    lang = "en";

                } else if (languageString.equals("Nepali")) {

                    lang = "np";

                }

                System.out.println("mode=" + lang);
                Locale locale = new Locale(lang);
                Locale.setDefault(locale);
                android.content.res.Configuration config = new android.content.res.Configuration();
                config.locale = locale;
                getBaseContext().getResources().updateConfiguration(
                        config,
                        getBaseContext().getResources()
                                .getDisplayMetrics());
                updatetext();
                getToolbarTitle();
                SharedPreferences sh_Pref_S1 = SettingActivity.this
                        .getSharedPreferences("Language Credential",
                                Context.MODE_PRIVATE);
                SharedPreferences.Editor toEdit1 = sh_Pref_S1.edit();
                toEdit1.putString("LANGUAGE", lang);
                toEdit1.commit();

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

    }

    public void updatetext() {
        texttext.setText(R.string.select_language);
        toolbar.setTitle(R.string.setting);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // todo: goto back activity from here

                Intent intent = new Intent(SettingActivity.this,DashboardActivity.class);
                startActivity(intent);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(SettingActivity.this, DashboardActivity.class);

        startActivity(intent);
        finish();
    }

}
