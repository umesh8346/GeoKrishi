package com.pathway.geokrishi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.pathway.geokrishi.dtos.LocationDto;
import com.pathway.geokrishi.dtos.MonthInfo;
import com.pathway.geokrishi.utils.AppUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class LocationSettingActivity extends  BaseActivity {



Spinner locationspinner;
 String locaitonameString,latitudeString,longitudeString;
    List<LocationDto> locationlist;
TextView texttext;
    int locationint;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        texttext=(TextView)findViewById(R.id.texttext) ;
        locationspinner=(Spinner)findViewById(R.id.locationspinner);

      //  setlocationame=AppUtils.locationame(getApplicationContext());
//        if(setlocationame.equals("")){
//
//        }else {
//            for (int j = 0; j < locationlist.size(); j++) {
//
//                if (setlocationame.equals(locationlist.get(j).getLocation())) {
//                    locationint = j;
//                }
//            }
//        }
        locationadapter();
    }

    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }

    @Override
    protected int getActivityID() {
        return 0;
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.setting;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.location_setting;
    }

    @Override
    protected int getDrawerLayout() {
        return 0;
    }

    @Override
    protected int getNavigationView() {
        return 0;
    }

    @Override
    protected Activity getInstance() {
        return null;
    }

    public void locationadapter() {
        ArrayAdapter<LocationDto> locationdapter = new ArrayAdapter<>(
                getApplicationContext(), R.layout.spinner_list_item,
                getlocation());
        locationspinner.setAdapter(locationdapter);

        locationspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub

                locaitonameString = String.valueOf(locationspinner
                        .getSelectedItem());
                latitudeString=locationlist.get(arg2).getLatitude();
                        longitudeString=locationlist.get(arg2).getLongitude();
                AppUtils.locationshareprefrence(getApplicationContext(),locaitonameString,latitudeString,longitudeString);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
    public List getlocation() {
        locationlist = new ArrayList<>();
        locationlist.add(new LocationDto("Bardiya", "28.3102323","81.4278984"));
        locationlist.add(new LocationDto("Surket", "28.36","81.36"));
        locationlist.add(new LocationDto("Jumla", "29.27472","82.18383"));
        locationlist.add(new LocationDto("Nuwakot ", "27.916663","85.249999"));
        locationlist.add(new LocationDto("Kathmandu", "27.71725", "85.32396"));
        locationlist.add(new LocationDto("Lalitpur", "27.66440", "85.31879"));
        return locationlist;
    }

}
