package com.pathway.geokrishi.Interface;

/**
 * Created by Ashish on 5/24/2017.
 * Pathway Technologies
 * pathwayasis@gmail.com
 */

public interface Sidemenu {
    public void sendDataToActivity(String data);

}
