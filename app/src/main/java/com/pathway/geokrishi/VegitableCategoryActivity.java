package com.pathway.geokrishi;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.pathway.geokrishi.ApiController.ApiManager;
import com.pathway.geokrishi.ApiController.ResponseDTO.ProfileResponseDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.SuitableListResponseDto;
import com.pathway.geokrishi.adapter.CategoryListAdapter;
import com.pathway.geokrishi.utils.AppConstant;
import com.pathway.geokrishi.utils.AppUtils;
import com.pathway.geokrishi.utils.Calculation;
import com.pathway.geokrishi.utils.DonutProgress;
import com.pathway.geokrishi.utils.ItemClickSupport;

import java.util.ArrayList;
import java.util.Timer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VegitableCategoryActivity extends BaseActivity implements View.OnClickListener {
    CategoryListAdapter vegetableListAdapter;
    ArrayList<SuitableListResponseDto.Data> vegetableList = new ArrayList<>();
    TextView toolbarName, tv_month, tv_des, textview;
    RecyclerView recyclerView;

    private Callback<SuitableListResponseDto> suitableCropResponseDtoCallback;
    private Callback<ProfileResponseDto> dataresponseResponseDtoCallback;
    Gson gson;
    TextView textviewcropcont;
    JsonArray jsonArray;
    Dialog dialog = null;
    String cropidString;
    String lat, lng;
    Dialog feedbackdailog;
    EditText edittextCropFeedback;
    Button btnSubmit, btnCancel, btnfeedback;
    String UserInputString;
    ProgressDialog progressDialog;

    @Override

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressDialog = new ProgressDialog(VegitableCategoryActivity.this, R.style.AppCompatAlertDialogStyle);
        toolbarName = (TextView) findViewById(R.id.tv_toolbar_name);
        tv_month = (TextView) findViewById(R.id.tv_month);
        textview = (TextView) findViewById(R.id.textview);
        tv_des = (TextView) findViewById(R.id.tv_des);

        btnfeedback = (Button) findViewById(R.id.btnfeedback);
        btnfeedback.setOnClickListener(this);
        tv_month.setText(MainActivity.monthString);
        lat = getIntent().getExtras().getString("latitude");
        lng = getIntent().getExtras().getString("longitude");
        dialog = new Dialog(VegitableCategoryActivity.this, R.style.DialogStyle);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        openLoadingDialog();
        gson = new Gson();
        recyclerView = (RecyclerView) findViewById(R.id.rv_vegitable_categoty);
        jsonArray = new JsonArray();
        StringBuilder cropid = new StringBuilder();
        for (int i = 0; i < MainActivity.selectionsInterger.size(); i++) {
            try {
                cropid.append(MainActivity.selectionsInterger.get(i) + ",");
                jsonArray.add(MainActivity.selectionsInterger.get(i));

            } catch (Exception e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }

        }
        cropidString = cropid.substring(0, cropid.length() - 1);
        suitableCropResponseDtoCallback = new Callback<SuitableListResponseDto>() {
            @Override
            public void onResponse(Call<SuitableListResponseDto> call, Response<SuitableListResponseDto> response) {
                if (response.isSuccessful()) {

                    SuitableListResponseDto suitableListResponseDto = response.body();
                    if (suitableListResponseDto.getStatus() == 0) {

                        if (suitableListResponseDto.getData().size() <= 0) {
                            cropfeedbackdailog();
                            // DataEmpty();
                            // tv_des.setText("Found no suitable argoproduct");
                            dialog.dismiss();
                        } else {
                            textview.setVisibility(View.VISIBLE);
                            btnfeedback.setVisibility(View.VISIBLE);
                            tv_des.setText(getText(R.string.found) + " " + suitableListResponseDto.getData().size() + " " + getText(R.string.suitable_crop));
                            vegetableList = suitableListResponseDto.getData();
                            initRecyclerView();
                        }

                    } else if (suitableListResponseDto.getStatus() == 1) {
                        dialog.dismiss();
                        textview.setVisibility(View.GONE);
                        btnfeedback.setVisibility(View.GONE);
                        AppUtils.errordialog(VegitableCategoryActivity.this, suitableListResponseDto.getMessage());
                    }

                } else {
                    dialog.dismiss();
                    textview.setVisibility(View.GONE);
                    btnfeedback.setVisibility(View.GONE);
                    AppUtils.errordialog(VegitableCategoryActivity.this, AppConstant.Server_Error);
                }
            }

            @Override
            public void onFailure(Call<SuitableListResponseDto> call, Throwable t) {
            }
        };

        dataresponseResponseDtoCallback = new Callback<ProfileResponseDto>() {
            @Override
            public void onResponse(Call<ProfileResponseDto> call, Response<ProfileResponseDto> response) {
                if (response.isSuccessful()) {

                    ProfileResponseDto cropfeedbackDtoResponse = response.body();
                    if (cropfeedbackDtoResponse.getStatus() == 0) {
                        dialog.dismiss();
                        progressDialog.dismiss();
                        feedbackdailog.dismiss();
                        DataEmpty();


                    } else if (cropfeedbackDtoResponse.getStatus() == 1) {
                        dialog.dismiss();
                        progressDialog.dismiss();
                        AppUtils.errordialog(VegitableCategoryActivity.this, cropfeedbackDtoResponse.getMessage());
                    }

                } else {
                    dialog.dismiss();
                    progressDialog.dismiss();
                    AppUtils.errordialog(VegitableCategoryActivity.this, AppConstant.Server_Error);
                }
            }

            @Override
            public void onFailure(Call<ProfileResponseDto> call, Throwable t) {
            }
        };


        if (AppUtils.checkInternetConnection(VegitableCategoryActivity.this)) {
            openLoadingDialog();
            ApiManager.SuitableCrops(suitableCropResponseDtoCallback, cropidString, lat, lng,
                    MainActivity.monthidString, AppUtils.apilanguage(getApplicationContext()));
        } else {
        }
    }

    private void initRecyclerView() {
        dialog.dismiss();
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        vegetableListAdapter = new CategoryListAdapter(vegetableList, VegitableCategoryActivity.this);
        recyclerView.setAdapter(vegetableListAdapter);
        vegetableListAdapter.notifyDataSetChanged();
        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                if (AppUtils.checkInternetConnection(getApplicationContext())) {

                    Intent intent = new Intent(getApplicationContext(),
                            DetailActivity.class);
                    intent.putExtra("Cropid", vegetableList.get(position).getCropVarietyCategoryID() + "");
                    intent.putExtra("value", vegetableList.get(position).getCropVarietyCategory() + "");
                    startActivity(intent);
                } else {
                    AppUtils.errordialog(VegitableCategoryActivity.this, AppConstant.no_network);
                }

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.filter_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.high) {
            vegetableListAdapter.getFilter().filter(getText(R.string.high_value));
        }

        if (id == R.id.low) {
            vegetableListAdapter.getFilter().filter(getText(R.string.low_value));
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }

    @Override
    protected int getActivityID() {
        return 0;
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.space;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.category_vegitable_layout;
    }

    @Override
    protected Activity getInstance() {
        return null;
    }

    @Override
    protected int getDrawerLayout() {
        return 0;
    }

    @Override
    protected int getNavigationView() {
        return 0;
    }

    public void openLoadingDialog() {
        dialog.setContentView(R.layout.loading_dialog_layout);
        TextView tv_dialogTitle = (TextView) dialog.findViewById(R.id.tv_dialogTitle);
        TextView tv_info = (TextView) dialog.findViewById(R.id.tv_info);
        String text = "Geo-<b>Processing";
        String text1 = "This might take time to process based on Internet Speed";
        tv_dialogTitle.setText(Html.fromHtml(text));
        tv_dialogTitle.setTextColor(Color.BLACK);
        final DonutProgress donutProgress = (DonutProgress) dialog.findViewById(R.id.donut_progress_1);
        tv_info.setText(text1);

        donutProgress.setSuffix("%");
        donutProgress.setProgressWithAnimation(100, 20);
        dialog.show();
    }

    public void DataEmpty() {
        new AlertDialog.Builder(VegitableCategoryActivity.this, R.style.AppCompatAlertDialogStyle)
                .setTitle(getText(R.string.alert))
                .setMessage(getText(R.string.crop_dailog_msg))
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();

                    }
                })
                .show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void cropfeedbackdailog() {
        feedbackdailog = new Dialog(VegitableCategoryActivity.this);
        feedbackdailog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        feedbackdailog.setContentView(R.layout.crop_dailog);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(feedbackdailog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        edittextCropFeedback = (EditText) feedbackdailog.findViewById(R.id.edittextCropFeedback);
        btnSubmit = (Button) feedbackdailog.findViewById(R.id.btnSubmit);
        textviewcropcont = (TextView) feedbackdailog.findViewById(R.id.textviewcropcont);
        textviewcropcont.setText(getText(R.string.suitable_crop_no_found));
        btnCancel = (Button) feedbackdailog.findViewById(R.id.btnCancel);
        btnSubmit.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        feedbackdailog.getWindow().setAttributes(lp);
        feedbackdailog.show();

    }

    public boolean isvarlidation() {
        UserInputString = AppUtils.gettextstring(edittextCropFeedback);
        if (!AppUtils.isvalid(UserInputString)) {
            edittextCropFeedback.setError(getText(R.string.reduired_field));
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v == btnSubmit) {
            if (isvarlidation()) {
                progressDialog.setMessage("Loading....");
                progressDialog.setCancelable(false);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.show();
                ApiManager.CropFeedback(dataresponseResponseDtoCallback, lat, lng,
                        AppUtils.userId(getApplicationContext()), UserInputString);
            }
        }
        if (v == btnCancel) {
            feedbackdailog.dismiss();
            finish();
        }
        if (v == btnfeedback) {
            cropfeedbackdailog();
        }
    }
}
