package com.pathway.geokrishi.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.pathway.geokrishi.BaseActivity;
import com.pathway.geokrishi.CropToolActivity;
import com.pathway.geokrishi.R;
import com.pathway.geokrishi.SettingActivity;

import static com.pathway.geokrishi.MainActivity.fl_floating;
import static com.pathway.geokrishi.MainActivity.relativelayot_ll;



public class SearchFragment extends Fragment implements View.OnClickListener{
    RelativeLayout relativecroptype,relativeuserprofile;

    public SearchFragment(){
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tool_layout, container, false);
        relativecroptype=(RelativeLayout)view.findViewById(R.id.relativecroptype) ;
        relativeuserprofile=(RelativeLayout)view.findViewById(R.id.relativeuserprofile) ;
        relativecroptype.setOnClickListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }



    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onClick(View view) {


        if(view==relativecroptype){
            startActivity(new Intent(getActivity(),CropToolActivity.class));

        }
    }

}
