package com.pathway.geokrishi.Openweather;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.pathway.geokrishi.Openweather.WeatherDTO.List;
import com.pathway.geokrishi.R;

import com.pathway.geokrishi.utils.AppUtils;

import java.util.ArrayList;
import java.util.Random;


public class WeatherAdapter extends PagerAdapter {

    private Context mContext;
    ArrayList<List> daylist = new ArrayList<>();

    public WeatherAdapter(Context c, ArrayList<List> daylist) {
        this.mContext = c;
        this.daylist = daylist;
    }

    @Override
    public int getCount() {
        return daylist.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);

    }

    public Object instantiateItem(ViewGroup container, int position) {
        ImageView weatherimage, weatherimagenight;
        TextView textviewday, textviewIconPhrase, textviewprobabity, textviewdaytemp, textviewrain, textviewPressure,
                textviewSpeed,textvimoring,textviewtemoday,textvievening,textviewnight;
        RelativeLayout relativelayoutWeather;


        View view;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        view = inflater.inflate(R.layout.dayweather_layout, null);
        weatherimage = (ImageView) view.findViewById(R.id.weatherimage);
        textvimoring = (TextView) view.findViewById(R.id.textvimoring);
                textviewtemoday= (TextView) view.findViewById(R.id.textviewtemoday);
        textvievening= (TextView) view.findViewById(R.id.textvievening);
                textviewnight= (TextView) view.findViewById(R.id.textviewnight);

        textviewday = (TextView) view.findViewById(R.id.textviewday);
        textviewIconPhrase = (TextView) view.findViewById(R.id.textviewIconPhrase);
        textviewprobabity = (TextView) view.findViewById(R.id.textviewprobabity);
        textviewdaytemp = (TextView) view.findViewById(R.id.textviewdaytemp);
        textviewrain = (TextView) view.findViewById(R.id.textviewrain);
        textviewPressure= (TextView) view.findViewById(R.id.textviewPressure);
       textviewSpeed = (TextView) view.findViewById(R.id.textviewSpeed);
        relativelayoutWeather= (RelativeLayout) view.findViewById(R.id.relativelayoutWeather);

        final TypedArray imgs = mContext.getResources().obtainTypedArray(R.array.image);
        final Random rand = new Random();
        final int rndInt = rand.nextInt(imgs.length());
        final int resID = imgs.getResourceId(rndInt, 0);

        relativelayoutWeather.setBackgroundDrawable( mContext.getResources().getDrawable(resID) );


        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(weatherimage);
        Glide.with(mContext).load("http://openweathermap.org/img/w/"+daylist.get(position).getWeather().get(0).getIcon()+".png").
                into(imageViewTarget);

        textviewday.setText(String.valueOf(AppUtils.getweatherday(daylist.get(position).getDt())));
        textviewIconPhrase.setText(daylist.get(position).getWeather().get(0).getDescription());
      textviewprobabity.setText(String.valueOf(daylist.get(position).getHumidity())+"%");
        textviewdaytemp.setText(Html.fromHtml(AppUtils.gettemp(daylist.get(position).getTemp().getDay())+"<sup>o</sup>C"));

        textviewtemoday.setText(Html.fromHtml(AppUtils.gettemp(daylist.get(position).getTemp().getDay())+"<sup>o</sup>C"));
        textvimoring.setText(Html.fromHtml(AppUtils.gettemp(daylist.get(position).getTemp().getMorn())+"<sup>o</sup>C"));
        textvievening.setText(Html.fromHtml(AppUtils.gettemp(daylist.get(position).getTemp().getEve())+"<sup>o</sup>C"));
        textviewnight.setText(Html.fromHtml(AppUtils.gettemp(daylist.get(position).getTemp().getNight())+"<sup>o</sup>C"));
        textviewrain.setText(String.valueOf(daylist.get(position).getRain())+"mm");
        textviewPressure.setText(String.valueOf(daylist.get(position).getPressure())+"hPa");
     textviewSpeed.setText(String.valueOf(daylist.get(position).getSpeed())+"meter/sec");

        ((ViewPager) container).addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ((ViewPager) container).removeView((LinearLayout) object);
    }


}
