package com.pathway.geokrishi.Openweather;

import android.app.Activity;
import android.nfc.tech.MifareUltralight;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.pathway.geokrishi.BaseActivity;
import com.pathway.geokrishi.Openweather.Fragment.WeatherHourFragment;
import com.pathway.geokrishi.Openweather.Fragment.WeatherdayFragment;
import com.pathway.geokrishi.R;
import com.pathway.geokrishi.utils.AppUtils;


public class WeatherActivity extends BaseActivity {
    TabLayout tablayoutaccuwheather;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if(AppUtils.locatiosettingname(getApplicationContext()).equals("")){

        }
        else{
            getSupportActionBar().setTitle("weatther in "+AppUtils.locatiosettingname(getApplicationContext())) ;
        }

        init();


    }

    private void init(){
        tablayoutaccuwheather = (TabLayout) findViewById(R.id.tablayoutaccuwheather);
        TabLayout.Tab firstTab = tablayoutaccuwheather.newTab();
        firstTab.setText("Daily");
        tablayoutaccuwheather.addTab(firstTab);
        TabLayout.Tab secondTab = tablayoutaccuwheather.newTab();
        secondTab.setText("Hourly");
        tablayoutaccuwheather.addTab(secondTab);


        setfragement(new WeatherdayFragment());
        tablayoutaccuwheather.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                Fragment fragment = null;
                switch (tab.getPosition()) {
                    case 0:
                        setfragement(new WeatherdayFragment());
                        break;
                    case 1:

                        setfragement(new WeatherHourFragment());
                        break;

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }

    @Override
    protected int getActivityID() {
        return 0;
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.Weather_surkhet;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.activity_accu_weather;
    }

    @Override
    protected int getDrawerLayout() {
        return 0;
    }

    @Override
    protected int getNavigationView() {
        return 0;
    }

    @Override
    protected Activity getInstance() {
        return null;
    }

    public void setfragement(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.simpleFrameLayout, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
