package com.pathway.geokrishi.ApiController.ResponseDTO;

import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dell on 5/18/2017.
 */

public class CropCategoryResponseDto {


    @SerializedName("Status")
    public int Status;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public ArrayList<CropCategoryResponseDto.Data> getData() {
        return Data;
    }

    public void setData(ArrayList<CropCategoryResponseDto.Data> data) {
        Data = data;
    }

    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public ArrayList<Data> Data;

    public static class Data {


        public int getCropCategoryID() {
            return CropCategoryID;
        }

        public void setCropCategoryID(int cropCategoryID) {
            CropCategoryID = cropCategoryID;
        }

        public String getCropCategory1() {
            return CropCategory1;
        }

        public void setCropCategory1(String cropCategory1) {
            CropCategory1 = cropCategory1;
        }

        public String getCropCategory2() {
            return CropCategory2;
        }

        public void setCropCategory2(String cropCategory2) {
            CropCategory2 = cropCategory2;
        }

        public String getCropCategoryImage() {
            return CropCategoryImage;
        }

        public void setCropCategoryImage(String cropCategoryImage) {
            CropCategoryImage = cropCategoryImage;
        }


        public boolean isCheckedStatus() {
            return CheckedStatus;
        }

        public void setCheckedStatus(boolean checkedStatus) {
            CheckedStatus = checkedStatus;
        }

        @SerializedName("CropCategoryID")
        public int CropCategoryID;
        @SerializedName("CropCategory1")
        public String CropCategory1;
        @SerializedName("CropCategory2")
        public String CropCategory2;
        @SerializedName("CropCategoryImage")
        public String CropCategoryImage;
        @SerializedName("CheckedStatus")
        public boolean CheckedStatus;

        public Data(int cropCategoryID,String cropCategory2){
            this.CropCategoryID = cropCategoryID;
            this.CropCategory2 = cropCategory2;

        }


        @Override
        public String toString()
        {
            return this.CropCategory2;

        }
    }


}
