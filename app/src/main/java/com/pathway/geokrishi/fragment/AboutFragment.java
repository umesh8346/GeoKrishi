package com.pathway.geokrishi.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pathway.geokrishi.R;

import static com.pathway.geokrishi.MainActivity.fl_floating;
import static com.pathway.geokrishi.MainActivity.relativelayot_ll;

/**
 * Created by kishor on 15/05/17.
 */

public class AboutFragment extends Fragment implements View.OnClickListener{

    public AboutFragment(){
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.comming_soon_layout, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }



    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

        }
    }

}
