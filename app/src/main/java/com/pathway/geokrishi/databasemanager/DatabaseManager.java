package com.pathway.geokrishi.databasemanager;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.pathway.geokrishi.dtos.DistrictInfo;
import com.pathway.geokrishi.dtos.VDCInfo;
import com.pathway.geokrishi.utils.AppConstant;

import java.util.ArrayList;

public class DatabaseManager {


    SQLiteDatabase db;
    DatabaseManager databasemanager;
    DatabaseHelper helper;

    public DatabaseManager(Context context)

    {
        helper = new DatabaseHelper(context);

        db = helper.getWritableDatabase();

    }


    public int getdistrictcount() {
        String countQuery = "SELECT " + AppConstant.ID + " FROM " + AppConstant.DISTRICT_INFO;
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }

    public int getvdccount() {
        String countQuery = "SELECT " + AppConstant.ID + " FROM " + AppConstant.VDC_INFO;
        Cursor cursor = db.rawQuery(countQuery, null);
        int cnt = cursor.getCount();
        cursor.close();
        return cnt;
    }


    public void insertdistrict(String line) {


        String[] separated = line.split(",");
        String districtid = separated[0];
        String districtname = separated[1];
        System.out.println("districtname===districtid");
        ContentValues cv = new ContentValues();
        cv.put(AppConstant.DISTRICT_ID, districtid);
        cv.put(AppConstant.DISTRICT_NAME, districtname);
        db.insert(AppConstant.DISTRICT_INFO, null, cv);
    }


    public void insertvdc(String line) {


        String[] separated = line.split(",");
        String vdcid = separated[0]; // this will contain "Fruit"
        String vcdname = separated[1];
        String districtid = separated[2];
        System.out.println("vdcid===" + vdcid);
        ContentValues cv = new ContentValues();
        cv.put(AppConstant.VDC_ID, vdcid);
        cv.put(AppConstant.DISTRICT_ID, districtid);
        cv.put(AppConstant.VDC_NAME, vcdname);
        db.insert(AppConstant.VDC_INFO, null, cv);

        //  db.execSQL(sb.toString());

    }

    public ArrayList<DistrictInfo> getalldistrict() {
        ArrayList<DistrictInfo> userlist = new ArrayList<DistrictInfo>();
        String sql = "SELECT DistrictID,DistrictName FROM " + AppConstant.DISTRICT_INFO;
        //System.out.println("sql==="+sql);

        Cursor cursor = db.rawQuery(sql, null);
        DistrictInfo info;
        try {
            while (cursor.moveToNext()) {
                String id = cursor.getString(cursor
                        .getColumnIndex(AppConstant.DISTRICT_ID));
                String districtname = cursor.getString(cursor
                        .getColumnIndex(AppConstant.DISTRICT_NAME));

                info = new DistrictInfo();
                info.setId(id);
                info.setDistrictname(districtname);
                userlist.add(info);

            }

            return userlist;
        } finally {
            cursor.close();
        }

    }


    public ArrayList<VDCInfo> getvdc(String id) {
        ArrayList<VDCInfo> vdclist = new ArrayList<>();
        String sql = "SELECT * FROM " + AppConstant.VDC_INFO
                + " where DistrictID =" + id;
        //System.out.println("sql==="+sql);

        Cursor cursor = db.rawQuery(sql, null);
        VDCInfo info;
        try {
            while (cursor.moveToNext()) {
                String districtid = cursor.getString(cursor
                        .getColumnIndex(AppConstant.DISTRICT_ID));
                String vdcname = cursor.getString(cursor
                        .getColumnIndex(AppConstant.VDC_NAME));
                String vdcid = cursor.getString(cursor
                        .getColumnIndex(AppConstant.VDC_ID));
                info = new VDCInfo();
                info.setDistrictid(districtid);
                info.setVdcname(vdcname);
                info.setVdcid(vdcid);
                vdclist.add(info);

            }

            return vdclist;
        } finally {
            cursor.close();
        }

    }


}
