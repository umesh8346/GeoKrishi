package com.pathway.geokrishi.ApiController.ResponseDTO;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dell on 5/23/2017.
 */

public class CommentResponseDto {

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public ArrayList<CommentResponseDto.Data> getData() {
        return Data;
    }

    public void setData(ArrayList<CommentResponseDto.Data> data) {
        Data = data;
    }

    @SerializedName("Status")
    public int Status;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public ArrayList<Data> Data;

    public static class Data {
        @SerializedName("UserID")
        public int UserID;

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int userID) {
            UserID = userID;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String userName) {
            UserName = userName;
        }

        public String getAvatarImage() {
            return AvatarImage;
        }

        public void setAvatarImage(String avatarImage) {
            AvatarImage = avatarImage;
        }

        public String getCommentDate() {
            return CommentDate;
        }

        public void setCommentDate(String commentDate) {
            CommentDate = commentDate;
        }

        public String getCommentText() {
            return CommentText;
        }

        public void setCommentText(String commentText) {
            CommentText = commentText;
        }

        public String getRating() {
            return Rating;
        }

        public void setRating(String rating) {
            Rating = rating;
        }

        @SerializedName("UserName")
        public String UserName;
        @SerializedName("AvatarImage")
        public String AvatarImage;
        @SerializedName("CommentDate")
        public String CommentDate;
        @SerializedName("CommentText")
        public String CommentText;
        @SerializedName("Rating")
        public String Rating;
    }
}
