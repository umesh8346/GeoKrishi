package com.pathway.geokrishi.ApiController.ResponseDTO;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Ashish on 8/11/2017.
 * Pathway Technologies
 * pathwayasis@gmail.com
 */

public class CropExcelDto {
    @SerializedName("Status")
    public int Status;
    @SerializedName("Message")
    public String Message;

    public String getData() {
        return Data;
    }

    public void setData(String data) {
        Data = data;
    }

    @SerializedName("Data")
    public String Data;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }


}
