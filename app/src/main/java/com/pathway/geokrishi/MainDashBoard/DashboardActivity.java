package com.pathway.geokrishi.MainDashBoard;

import android.app.Activity;
import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.pathway.geokrishi.ApiController.ApiConfig;
import com.pathway.geokrishi.ApiController.ApiManager;
import com.pathway.geokrishi.BaseActivity;
import com.pathway.geokrishi.CropCalender.CropCalendarChooseActivity;
import com.pathway.geokrishi.CropCalender.CropCalenderActivity;
import com.pathway.geokrishi.CropToolActivity;
import com.pathway.geokrishi.FarmCalcualtion.FarmCalculationActivity;
import com.pathway.geokrishi.Interface.Sidemenu;

import com.pathway.geokrishi.MainActivity;
import com.pathway.geokrishi.MonitoringActivity;
import com.pathway.geokrishi.Notification.DTO.Datum;
import com.pathway.geokrishi.Notification.DTO.NotificationDTO;
import com.pathway.geokrishi.Notification.NotificationActivity;
import com.pathway.geokrishi.Openweather.WeatherActivity;
import com.pathway.geokrishi.Openweather.WeatherCurrentDto.Weather;
import com.pathway.geokrishi.Openweather.WeatherCurrentDto.WeatherCurrentDto;
import com.pathway.geokrishi.Openweather.WeatherHourDTO.WeatherHourDto;
import com.pathway.geokrishi.ProfileActivity;
import com.pathway.geokrishi.R;
import com.pathway.geokrishi.utils.AppConstant;
import com.pathway.geokrishi.utils.AppUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DashboardActivity extends BaseActivity implements DashboardView, Sidemenu,View.OnClickListener{
    boolean shouldExecuteOnResume;
ImageButton imagebtnSetting,imagebtnnotification;
    RelativeLayout relativelayoutSutitable,relativelayoutPlant,relativelayoutweather,relativelayouCropCalender,
            relativelayoutCropGrow,relativelayoutFarmCalculation,relativelayoutMonitoring;
    private Callback<WeatherCurrentDto> CurrentWeatherDTOCallback;
    private  Callback<NotificationDTO> NotificationDTOCallback;
    ArrayList<Datum> notificationlist=new ArrayList<>();
    Animation slideUp;
    String latitude,longitude;
    ArrayList<Weather> wheatherlist= new ArrayList<>();
    TextView textviewWeatherCondition,textviewTemperature,textnotification;
ImageView imageviewWether;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        shouldExecuteOnResume = false;
        imagebtnSetting=(ImageButton)findViewById(R.id.imagebtnSetting);
        imagebtnnotification=(ImageButton)findViewById(R.id.imagebtnnotification);
        relativelayoutSutitable=(RelativeLayout)findViewById(R.id.relativelayoutSutitable);
        relativelayoutPlant=(RelativeLayout)findViewById(R.id.relativelayoutPlant);
        relativelayoutweather=(RelativeLayout)findViewById(R.id.relativelayoutweather);
        relativelayouCropCalender=(RelativeLayout)findViewById(R.id.relativelayouCropCalender);
        relativelayoutMonitoring=(RelativeLayout)findViewById(R.id.relativelayoutMonitoring);
        relativelayoutCropGrow=(RelativeLayout)findViewById(R.id.relativelayoutCropGrow);
        relativelayoutFarmCalculation=(RelativeLayout)findViewById(R.id.relativelayoutFarmCalculation);
        textviewTemperature=(TextView)findViewById(R.id.textviewTemperature);
        textviewWeatherCondition=(TextView)findViewById(R.id.textviewWeatherCondition);
        textnotification=(TextView)findViewById(R.id.textnotification);
        imageviewWether=(ImageView) findViewById(R.id.imageviewWether);
        imagebtnSetting.setOnClickListener(this);
        relativelayoutSutitable.setOnClickListener(this);
        relativelayoutPlant.setOnClickListener(this);
        relativelayoutweather.setOnClickListener(this);
        imagebtnnotification.setOnClickListener(this);
        relativelayouCropCalender.setOnClickListener(this);
        relativelayoutCropGrow.setOnClickListener(this);
        relativelayoutFarmCalculation.setOnClickListener(this);
        relativelayoutMonitoring.setOnClickListener(this);
        latitude=String.valueOf(AppUtils.latitude(getApplicationContext()));
        longitude=String.valueOf(AppUtils.longitude(getApplicationContext()));
        slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.zoomin);
        CurrentWeatherDTOCallback = new Callback<WeatherCurrentDto>() {
            @Override
            public void onResponse(Call<WeatherCurrentDto> call, Response<WeatherCurrentDto> response) {
                System.out.println("weatherResponse==="+response.body());
                if (response.isSuccessful()) {

                    WeatherCurrentDto weatherResponse = response.body();
                    wheatherlist=weatherResponse.getWeather();
                    GlideDrawableImageViewTarget imageViewTarget2 = new GlideDrawableImageViewTarget(imageviewWether);
                    Glide.with(getApplicationContext()).load("http://openweathermap.org/img/w/" + wheatherlist.get(0).getIcon()+".png").
                            into(imageViewTarget2);
                    System.out.println("descripiton=="+wheatherlist.get(0).getDescription());
                    textviewTemperature.setText(Html.fromHtml(AppUtils.gettemp(weatherResponse.getMain().getTemp())+"<sup>o</sup>C"));

                  //  textviewTemperature.setText(AppUtils.gettemp(weatherResponse.getMain().getTemp()));
                    textviewWeatherCondition.setText(wheatherlist.get(0).getDescription());
                }

            }

            @Override
            public void onFailure(Call<WeatherCurrentDto> call, Throwable t) {
                System.out.println("error=="+t);

            }
        };


        NotificationDTOCallback = new Callback<NotificationDTO>() {
            @Override
            public void onResponse(Call<NotificationDTO> call, Response<NotificationDTO> response) {
                System.out.println("weatherResponse==="+response.body());
                if (response.isSuccessful()) {

                    NotificationDTO notificationresponse = response.body();
                    notificationlist=notificationresponse.getData();
                    if(notificationlist.size()>0){
                        textnotification.setVisibility(View.VISIBLE);
                        textnotification.setText(String.valueOf(notificationlist.size()));
                    }
                    else{
                        textnotification.setVisibility(View.GONE);
                    }

                }

            }

            @Override
            public void onFailure(Call<NotificationDTO> call, Throwable t) {
                System.out.println("error=="+t);

            }
        };




        if (AppUtils.checkInternetConnection(DashboardActivity.this)) {
            String url= ApiConfig.openwheatherCurrentUrl.replace("{lat}","lat="+latitude+"&lon="+longitude+"&appid=6023985399897109c893543cfd4ce0bb");
            ApiManager.CurrentopenWheather(CurrentWeatherDTOCallback,url);

            ApiManager.getNotification(NotificationDTOCallback, AppUtils.userId(DashboardActivity.this));



        } else {
            //AppUtils.errordialog(getActivity(), AppConstant.no_network);
        }




    }
    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }

    @Override
    protected int getActivityID() {
        return 0;
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.framinig_life_cycle;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.main_dashboard_layout;
    }

    @Override
    protected int getDrawerLayout() {
     return R.id.drawer_layout;
    }

    @Override
    protected int getNavigationView() {
   return R.id.nav_view;
    }

    @Override
    protected Activity getInstance() {
        return DashboardActivity.this;
    }



    @Override
    public void onClick(View v) {
if(v==imagebtnSetting){
    if(drawer.isDrawerOpen(navigationView)){
        drawer.closeDrawer(navigationView);
    }
    else{
        //sendDataToActivity("abc");
        drawer.openDrawer(navigationView);
    }


}
        if(v==relativelayoutSutitable){


                    Intent i=new Intent(DashboardActivity.this,MainActivity.class);
                    startActivity(i);

        }
        if(v==relativelayoutPlant){


                    Intent i=new Intent(DashboardActivity.this,CropToolActivity.class);
                    startActivity(i);


        }
        if(v==relativelayoutweather){


                    Intent i=new Intent(DashboardActivity.this,WeatherActivity.class);
                    startActivity(i);

        }

        if(v==relativelayoutCropGrow){


                    Intent i=new Intent(DashboardActivity.this,CropCalenderActivity.class);
                    startActivity(i);

        }

        if(v==relativelayouCropCalender){


                    Intent i=new Intent(DashboardActivity.this,CropCalendarChooseActivity.class);
                    startActivity(i);


        }

        if(v==relativelayoutFarmCalculation){




                    Intent i=new Intent(DashboardActivity.this,FarmCalculationActivity.class);
                    startActivity(i);

//
//
//            startActivity(new Intent(this,
//                    FarmCalculationActivity.class));
        }
        if(v==relativelayoutMonitoring){


                    Intent i=new Intent(DashboardActivity.this,MonitoringActivity.class);
                    startActivity(i);


        }

        if(v==imagebtnnotification){
            if(notificationlist.size()>0){
                Intent i = new Intent(this, NotificationActivity.class);
                i.putExtra("mylist", notificationlist);
                startActivity(i);

            }
            else{

            }
        }
    }


    @Override
    public void sendDataToActivity(String data) {
        if (data.equals("abc")) {
            drawer.closeDrawer(navigationView);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(shouldExecuteOnResume){
            Intent intent = getIntent();
            finish();
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
        } else{
            shouldExecuteOnResume = true;
        }

    }
}
