package com.pathway.geokrishi.ApiController;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.pathway.geokrishi.ApiController.ResponseDTO.CommentResponseDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.CropCalenderDTO;
import com.pathway.geokrishi.ApiController.ResponseDTO.CropCategoryResponseDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.CropExcelDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.CropListDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.CropVaritiesDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.LoginResponseDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.ProfileDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.ProfileResponseDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.RegisterResponseDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.SuitableCropsDetail;
import com.pathway.geokrishi.ApiController.ResponseDTO.SuitableListResponseDto;
import com.pathway.geokrishi.Notification.DTO.NotificationDTO;
import com.pathway.geokrishi.Openweather.WeatherCurrentDto.WeatherCurrentDto;
import com.pathway.geokrishi.Openweather.WeatherDTO.WeatherDayDto;
import com.pathway.geokrishi.Openweather.WeatherHourDTO.WeatherHourDto;


import org.json.JSONArray;

import java.util.Map;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class ApiManager {
    public static final String TAG = "ApiManager";

    //get crop category
    public static void getCropCategory(Callback<CropCategoryResponseDto> callback, String language) {
        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);
        Call<CropCategoryResponseDto> call = apiInterface.getCropCatergory(language);
        call.enqueue(callback);
    }

    public static void FacebookRegistration(Callback<RegisterResponseDto> callback, String username, String firstname, String lastname, String fbid, String fbtoken,String email, String userimage,String RegisterID,String AndroidID,String Longitude,String Latitude) {
        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);
        Call<RegisterResponseDto> call = apiInterface.FacebookRegistration(username,firstname,lastname,fbid,fbtoken,email,userimage,RegisterID,AndroidID,Longitude,Latitude);
        call.enqueue(callback);
    }

    public static void UserRegistration(Callback<RegisterResponseDto> callback, String username, String email,String Password,String RegisterID,String AndroidID,String Longitude,String Latitude) {
        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);
        Call<RegisterResponseDto> call = apiInterface.UserRegistration(username,email,Password,RegisterID,AndroidID,Longitude,Latitude);
        call.enqueue(callback);
    }

    public static void UserSignIn(Callback<LoginResponseDto> callback, String username, String password) {
        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);
        Call<LoginResponseDto> call = apiInterface.UserSign(username,password);
        call.enqueue(callback);
    }

    public static void SuitableCrops(Callback<SuitableListResponseDto> callback, String  cropidArray, String latitude, String longitude, String month,String language) {
        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);
        Call<SuitableListResponseDto> call = apiInterface.getSuitableCrops(cropidArray,latitude,longitude,month, language);
        call.enqueue(callback);
    }
    public static void SuitableCropsDetail(Callback<SuitableCropsDetail> callback, String  cropdetailId,String language) {
        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);
        Call<SuitableCropsDetail> call = apiInterface.getSuitableCropsDetail(cropdetailId,language);
        call.enqueue(callback);
    }


    public static void UserComment(Callback<CommentResponseDto> callback, String UserID,String  CropdetailId,String Comment,String Rating) {
        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);
        Call<CommentResponseDto> call = apiInterface.postComment(UserID,CropdetailId,Comment,Rating);
        call.enqueue(callback);
    }


    public static void UserRemark(Callback<CommentResponseDto> callback, String UserID,String  CropdetailId,String RemarkType,String UserRemark) {
        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);
        Call<CommentResponseDto> call = apiInterface.postRemark(UserID,CropdetailId,RemarkType,UserRemark);
        call.enqueue(callback);
    }

    public static void UserProfile(Callback<ProfileDto> callback, String UserID){

        ApiInterface apiInterface =RestServiceGenerator.createService(ApiInterface.class);
        Call<ProfileDto> call=apiInterface.getUserProfile(UserID);
        call.enqueue(callback);
    }

    public static void UserProfileResponse(Callback<ProfileResponseDto> callback, String Type,JsonObject json){
        ApiInterface apiInterface =RestServiceGenerator.createService(ApiInterface.class);
        Call<ProfileResponseDto> call=apiInterface.getUserProfileResponse(Type,json);
        call.enqueue(callback);
    }

    public static void UserProfileImageResponse(Callback<ProfileResponseDto> callback,Map<String, RequestBody> map, MultipartBody.Part file){
        ApiInterface apiInterface =RestServiceGenerator.createService(ApiInterface.class);
        Call<ProfileResponseDto> call=apiInterface.getuserimageresponse(map,file);
        call.enqueue(callback);
    }

    public static void UserChangepasswordResponse(Callback<ProfileResponseDto> callback, String oldpassword,String newpassword,String Userid ){
        ApiInterface apiInterface =RestServiceGenerator.createService(ApiInterface.class);
        Call<ProfileResponseDto> call=apiInterface.getChangepasswordResponse(oldpassword,newpassword,Userid);
        call.enqueue(callback);
    }

    public static void SuitableCropsTool(Callback<SuitableListResponseDto> callback, String  cropid,String language) {
        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);
        Call<SuitableListResponseDto> call = apiInterface.getSuitableCropsTool(cropid, language);
        call.enqueue(callback);
    }
//    public static void DayWeather(Callback<WeatherDTO> callback) {
//        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);
//        Call<WeatherDTO> call =  apiInterface.getWeather5day();
//        call.enqueue(callback);
//    }
//    public static void HourWeather(Callback<ArrayList<WeatherHourDto>> callback) {
//        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);
//        Call<ArrayList<WeatherHourDto>> call =  apiInterface.getWeatherHour();
//        call.enqueue(callback);
 //   }

    public static void DayOpenWeather(Callback<WeatherDayDto> callback,String url) {
        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);
        Call<WeatherDayDto> call =  apiInterface.getdatweather(url);
        call.enqueue(callback);
    }
    public static void HourOpenWeather(Callback<WeatherHourDto> callback,String url) {
        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);
        Call<WeatherHourDto> call =  apiInterface.getWeatherHour(url);
        call.enqueue(callback);
    }

    public static void Userlatlong(Callback<ProfileResponseDto> callback,String userid, String longitude,String latitude) {
        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);
        Call<ProfileResponseDto> call =  apiInterface.getuserlatlng(userid,latitude,longitude);
        call.enqueue(callback);
    }

    public static void CurrentopenWheather(Callback<WeatherCurrentDto> callback, String url) {
        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);
        Call<WeatherCurrentDto> call =  apiInterface.getCurrentWeather(url);
        call.enqueue(callback);
    }

    public static void getCropList(Callback<CropListDto> callback, String CategoryId,String language) {
        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);
        Call<CropListDto> call = apiInterface.getCropCatergory(language,CategoryId);
        call.enqueue(callback);
    }
    public static void SuitableCropsList(Callback<SuitableListResponseDto> callback, String  CropId, String latitude, String longitude,String language) {
        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);
        Call<SuitableListResponseDto> call = apiInterface.getSutaibleCropList(CropId,latitude,longitude, language);
        call.enqueue(callback);
    }


    public static void getCalendarList(Callback<CropCalenderDTO> callback, String latlng, String type,String language) {
        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);
        Call<CropCalenderDTO> call = apiInterface.getcalendar(latlng,type,language);
        call.enqueue(callback);
    }


    public static void getCropVarities(Callback<CropVaritiesDto> callback, String Cropid, String language) {
        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);
        Call<CropVaritiesDto> call = apiInterface.getCropvarties(Cropid,language);
        call.enqueue(callback);
    }

    public static void getNotification(Callback<NotificationDTO> callback, String UserID) {
        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);
        Call<NotificationDTO> call =  apiInterface.getnotification(UserID);
        call.enqueue(callback);
    }
    public static void getNotificationdetail(Callback<NotificationDTO> callback, String UseridString,String NotificationID) {
        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);
        Call<NotificationDTO> call =  apiInterface.getnotificationDetail(UseridString,NotificationID);
        call.enqueue(callback);
    }


    public static void CropFeedback(Callback<ProfileResponseDto> callback, String latitude, String longitude, String UserID,String UserInputString) {
        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);
        Call<ProfileResponseDto> call = apiInterface.getcropFeedback(latitude,longitude, UserID,UserInputString);
        call.enqueue(callback);
    }

    public static void CropExcel(Callback<CropExcelDto> callback, String latlng,String type,String language) {
        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);
        Call<CropExcelDto> call = apiInterface.getCropExcel(latlng, type,language);
        call.enqueue(callback);
    }


    public static void Userforgetpassword(Callback<ProfileResponseDto> callback, String email,String phone ){
        ApiInterface apiInterface =RestServiceGenerator.createService(ApiInterface.class);
        Call<ProfileResponseDto> call=apiInterface.getforgetpassword(email,phone);
        call.enqueue(callback);
    }


}
