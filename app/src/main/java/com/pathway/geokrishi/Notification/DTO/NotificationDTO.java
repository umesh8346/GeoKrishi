
package com.pathway.geokrishi.Notification.DTO;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationDTO implements Parcelable
{

    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Data")
    @Expose
    private ArrayList<Datum> data = null;
    public final static Creator<NotificationDTO> CREATOR = new Creator<NotificationDTO>() {


        @SuppressWarnings({
            "unchecked"
        })
        public NotificationDTO createFromParcel(Parcel in) {
            NotificationDTO instance = new NotificationDTO();
            instance.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.message = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(instance.data, (Datum.class.getClassLoader()));
            return instance;
        }

        public NotificationDTO[] newArray(int size) {
            return (new NotificationDTO[size]);
        }

    }
    ;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Datum> getData() {
        return data;
    }

    public void setData(ArrayList<Datum> data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeList(data);
    }

    public int describeContents() {
        return  0;
    }

}
