package com.pathway.geokrishi.ApiController.ResponseDTO;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ashish on 7/12/2017.
 * Pathway Technologies
 * pathwayasis@gmail.com
 */

public class CropListDto {
    @SerializedName("Status")
    public int Status;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public ArrayList<Data> Data;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public ArrayList<CropListDto.Data> getData() {
        return Data;
    }

    public void setData(ArrayList<CropListDto.Data> data) {
        Data = data;
    }

    public static class Data {
        @SerializedName("CropID")
        public int CropID;
        @SerializedName("CropName")
        public String CropName;
        @SerializedName("Language")
        public String Language;

        public int getCropID() {
            return CropID;
        }

        public void setCropID(int cropID) {
            CropID = cropID;
        }

        public String getCropName() {
            return CropName;
        }

        public void setCropName(String cropName) {
            CropName = cropName;
        }

        public String getLanguage() {
            return Language;
        }

        public void setLanguage(String language) {
            Language = language;
        }

        @Override
        public String toString()
        {
            return this.CropName;

        }

    }
}
