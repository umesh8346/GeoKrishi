package com.pathway.geokrishi.ApiController;

import android.content.Context;

import com.pathway.geokrishi.utils.AppUtils;



public interface ApiConfig {;


    int CONNECT_TIMEOUT = 5000;
    int READ_TIMEOUT = 10000;
    String baseUrl = "http://103.233.58.90/GeoKrishi/API/";
  //  String baseUrl = "http://103.233.58.223/GeoKrishi/API/";
    String ImageUrl="http://103.233.58.223/GeoKrishi/CropImages/";
    String loginUrl = baseUrl+"UserSignIn/";
    String UserRegesterationMobile = baseUrl+"UserRegistrationMobile/";
    String CropCategory = baseUrl+"CropCategory";
    String CropListUrl = baseUrl+"Crops";
    String FacebookRegistration = baseUrl+"FacebookRegistration/";
    String UserSignIn = baseUrl+"UserSignIn/";
    String SuitableCrops = baseUrl+"SuitableCrops/";
  String CropExcelUrl = baseUrl+"CropCalenderExcel/";
    String DetailSuitableCrops = baseUrl+"SuitableCropDetail/";
    String Comment = baseUrl+"PostComment/";
    String Remark = baseUrl+"UserRemark/";
    String Userprofile=baseUrl+"GetUserProfile/";
    String ProfileUrl=baseUrl+"RegisterProfile/";
    String ListOfCrops = baseUrl+"ListOfCrops/";
    String ChangePAsswordUrl = baseUrl+"ChangePassword/";
    String latlngUrl = baseUrl+"SetDefaultLocation/";
    String UserImage =baseUrl+"Upload/PostUserImage/";
    String SuitableCropsListUrl = baseUrl+"CanIGrow/";
    String CalenderListUrl = baseUrl+"CropCalendar/";
  String CropvaritiesUrl = baseUrl+"GetCropVaritiesbyCropID/";
  String NotificationUrl = baseUrl+"NotificationsView/";
  String NotificationDetailUrl = baseUrl+"NotificationViewDetail/";
  String UserInputUrl=baseUrl+"UserInput/";
  String ForgetPasswordUrl=baseUrl+"ForgotPassword/";

    String WeatherUrl="http://dataservice.accuweather.com/forecasts/v1/daily/5day/243170?apikey=S5RRCeswPycIc1wT3v5UnYm28o3EydUJ&language=en&details=true&metric=true";
   // String UserImage = "http://192.168.13.56:64109/api/DocumentUpload/MediaUpload";

    String WeatherHrly="http://dataservice.accuweather.com/forecasts/v1/hourly/12hour/243170?apikey=S5RRCeswPycIc1wT3v5UnYm28o3EydUJ&language=en&details=true&metric=true";


    String OpenWeatherDayUrl="http://api.openweathermap.org/data/2.5/forecast/daily?{lat}";
String openWeatherHourUrl="http://api.openweathermap.org/data/2.5/forecast?{lat}&appid=6023985399897109c893543cfd4ce0bb";
String openwheatherCurrentUrl="http://api.openweathermap.org/data/2.5/weather?{lat}";


}
