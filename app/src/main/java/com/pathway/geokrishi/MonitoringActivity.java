package com.pathway.geokrishi;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.pathway.geokrishi.utils.AppConstant;


public class MonitoringActivity extends  BaseActivity implements View.OnClickListener {
TextView textmontoring,textviewmobile,textviewphone;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        textmontoring=(TextView)findViewById(R.id.textmontoring);
        textviewmobile=(TextView)findViewById(R.id.textviewmobile);
                textviewphone =(TextView)findViewById(R.id.textviewphone);
        textviewmobile.setOnClickListener(this);
        textviewphone.setOnClickListener(this);
        textmontoring.setText(Html.fromHtml(String.valueOf(getText(R.string.monotoring))));
//        textmontoring.setText(Html.fromHtml(String.valueOf(getText(R.string.about_us))));
    }
    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }

    @Override
    protected int getActivityID() {
        return 0;
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.crop_monitioring;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.monitoring_layout;
    }

    @Override
    protected int getDrawerLayout() {
        return 0;
    }

    @Override
    protected int getNavigationView() {
        return 0;
    }

    @Override
    protected Activity getInstance() {
        return null;
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onClick(View v) {
        if(v==textviewmobile){
            callPhone(AppConstant.mobile_no);
        }
        if(v==textviewphone){
            callPhone(AppConstant.phone_no);
        }
    }


    public void callPhone(String phone) {
        Intent callIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"
                + phone));
        startActivity(callIntent);
    }
}
