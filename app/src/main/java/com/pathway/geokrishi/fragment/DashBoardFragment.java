package com.pathway.geokrishi.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.GridLayoutAnimationController;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.pathway.geokrishi.ApiController.ApiManager;
import com.pathway.geokrishi.ApiController.CustomDialogs;
import com.pathway.geokrishi.ApiController.Networkcontroller;
import com.pathway.geokrishi.ApiController.ResponseDTO.CropCategoryResponseDto;
import com.pathway.geokrishi.ChangepasswordActivity;
import com.pathway.geokrishi.Interface.NetworkInterFace;
import com.pathway.geokrishi.Interface.ProgressbarInterface;
import com.pathway.geokrishi.MainActivity;
import com.pathway.geokrishi.R;
import com.pathway.geokrishi.SignupActivity;
import com.pathway.geokrishi.adapter.GridViewAdapter;
import com.pathway.geokrishi.dtos.CategoryDto;
import com.pathway.geokrishi.utils.AppConstant;
import com.pathway.geokrishi.utils.AppUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

import static com.pathway.geokrishi.MainActivity.selections1;

public class DashBoardFragment extends Fragment implements View.OnClickListener,NetworkInterFace,ProgressbarInterface{

    ArrayList<CropCategoryResponseDto.Data> categoryList = new ArrayList<>();
    CategoryDto categoryDto;
    public static CheckBox checkBox_all;
    private Callback<CropCategoryResponseDto> cropCategoryeResponseDtoCallback;
    Dialog pDialog;
    NetworkInterFace networkInterFace;
    ProgressbarInterface progressbarInterface;
    ProgressBar progressBar;
    RelativeLayout relativelayout;
    SwipeRefreshLayout swipeContainer;
    GridView gridView;
    GridLayoutAnimationController controller;
    Animation animation ;
    public DashBoardFragment(){
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
       final View view = inflater.inflate(R.layout.dashboard_layout, container, false);
        networkInterFace=this;
        progressbarInterface=this;
        checkBox_all = (CheckBox) view.findViewById(R.id.ch_all);
        progressBar=(ProgressBar)view.findViewById(R.id.progressBar) ;
        AppUtils.showprogressbar(getActivity(),progressBar,progressbarInterface);
        relativelayout=(RelativeLayout)view.findViewById(R.id.relativelayout);
        gridView = (GridView) view.findViewById(R.id.gv_category);
        relativelayout.setVisibility(View.INVISIBLE);
    animation = AnimationUtils.loadAnimation(getContext(),R.anim.grid_item_anim);
 controller = new GridLayoutAnimationController(animation, .2f, .2f);
        gridView.setLayoutAnimation(controller);
        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                gridView.setLayoutAnimation(controller);
                ApiManager.getCropCategory(cropCategoryeResponseDtoCallback,AppUtils.apilanguage(getActivity()));
            }
        });

        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        cropCategoryeResponseDtoCallback = new Callback<CropCategoryResponseDto>() {
            @Override
            public void onResponse(Call<CropCategoryResponseDto> call, retrofit2.Response<CropCategoryResponseDto> response) {

                pDialog.dismiss();
                if (response.isSuccessful()) {
                    swipeContainer.setRefreshing(false);
                    relativelayout.setVisibility(View.VISIBLE);
                    AppUtils.hideprogressbar(getActivity(),progressBar,progressbarInterface);
                    pDialog.dismiss();
                    CropCategoryResponseDto cropCategoryResponse = response.body();
                    if (cropCategoryResponse.getStatus()==0) {
                        categoryList = cropCategoryResponse.getData();

                        GridViewAdapter gridViewAdapter = new GridViewAdapter(categoryList,getActivity(),DashBoardFragment.this);
                        gridView.setAdapter(gridViewAdapter);

                    } else if(cropCategoryResponse.getStatus()==1){
                        swipeContainer.setRefreshing(false);
                        AppUtils.errordialog(getActivity(), AppConstant.Server_Error);;
                    }

                } else {
                    swipeContainer.setRefreshing(false);
                    AppUtils.hideprogressbar(getActivity(),progressBar,progressbarInterface);
                    AppUtils.errordialog(getActivity(), AppConstant.Server_Error);
                }
            }
            @Override
            public void onFailure(Call<CropCategoryResponseDto> call, Throwable t) {
                swipeContainer.setRefreshing(false);
                AppUtils.hideprogressbar(getActivity(),progressBar,progressbarInterface);
                pDialog.dismiss();
                AppUtils.errordialog(getActivity(),  t.getMessage());
            }
        };
        Networkcontroller.checkInternetConnection(getActivity(),this,"Dashboard");
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

        }
    }

    public void sendData(ArrayList<String> selections,ArrayList<String> selectionInteger) {
        MainActivity.selections1 = selections;


        MainActivity.selectionsInterger = selectionInteger;
      //  Toast.makeText(getActivity(),selections1.toString(),Toast.LENGTH_SHORT).show();

    }

    @Override
    public void network(Boolean aBoolean,String s) {
        if(aBoolean){
            pDialog = CustomDialogs.progressDialog(getActivity(),"Loading");

//            System.out.println("userlanguage=="+AppUtils.apilanguage(getActivity()));
            ApiManager.getCropCategory(cropCategoryeResponseDtoCallback,AppUtils.apilanguage(getActivity()));
        }
        else{
          //  AppUtils.errordialog(getActivity(), AppConstant.no_network);
        }
    }

    @Override
    public void showprogressbar(ProgressBar progressBar) {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideprogressbar(ProgressBar progressBar) {
progressBar.setVisibility(View.INVISIBLE);
    }
}
