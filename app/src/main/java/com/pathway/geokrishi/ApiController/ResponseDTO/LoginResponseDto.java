package com.pathway.geokrishi.ApiController.ResponseDTO;


import com.google.gson.annotations.SerializedName;

public class LoginResponseDto {

    @SerializedName("Status")
    public int Status;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public LoginResponseDto.Data getData() {
        return Data;
    }

    public void setData(LoginResponseDto.Data data) {
        Data = data;
    }

    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class Data {
        public int getUserID() {
            return UserID;
        }

        public void setUserID(int userID) {
            UserID = userID;
        }

        public String getMobileNumber() {
            return MobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            MobileNumber = mobileNumber;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String userName) {
            UserName = userName;
        }

        public String getPassword() {
            return Password;
        }

        public void setPassword(String password) {
            Password = password;
        }

        public String getFirstName() {
            return FirstName;
        }

        public void setFirstName(String firstName) {
            FirstName = firstName;
        }

        public String getLastName() {
            return LastName;
        }

        public void setLastName(String lastName) {
            LastName = lastName;
        }

        public String getEmail() {
            return Email;
        }

        public void setEmail(String email) {
            Email = email;
        }

        public String getOrganization() {
            return Organization;
        }

        public void setOrganization(String organization) {
            Organization = organization;
        }

        public String getProfession() {
            return Profession;
        }

        public void setProfession(String profession) {
            Profession = profession;
        }

        public String getFacebookID() {
            return FacebookID;
        }

        public void setFacebookID(String facebookID) {
            FacebookID = facebookID;
        }

        public String getFacebookToken() {
            return FacebookToken;
        }

        public void setFacebookToken(String facebookToken) {
            FacebookToken = facebookToken;
        }

        public String getAvatarImage() {
            return AvatarImage;
        }

        public void setAvatarImage(String avatarImage) {
            AvatarImage = avatarImage;
        }

        public double getLatitude() {
            return Latitude;
        }

        public void setLatitude(double latitude) {
            Latitude = latitude;
        }

        public double getLongitude() {
            return Longitude;
        }

        public void setLongitude(double longitude) {
            Longitude = longitude;
        }

        @SerializedName("UserID")
        public int UserID;
        @SerializedName("MobileNumber")
        public String MobileNumber;
        @SerializedName("UserName")
        public String UserName;
        @SerializedName("Password")
        public String Password;
        @SerializedName("FirstName")
        public String FirstName;
        @SerializedName("LastName")
        public String LastName;
        @SerializedName("Email")
        public String Email;
        @SerializedName("Organization")
        public String Organization;
        @SerializedName("Profession")
        public String Profession;
        @SerializedName("FacebookID")
        public String FacebookID;
        @SerializedName("FacebookToken")
        public String FacebookToken;
        @SerializedName("AvatarImage")
        public String AvatarImage;
        @SerializedName("Latitude")
        public double Latitude;
        @SerializedName("Longitude")
        public double Longitude;
    }
}
