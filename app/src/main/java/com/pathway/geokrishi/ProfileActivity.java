package com.pathway.geokrishi;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import com.google.gson.JsonObject;
import com.pathway.geokrishi.ApiController.ApiManager;
import com.pathway.geokrishi.ApiController.CustomDialogs;
import com.pathway.geokrishi.ApiController.ResponseDTO.CommentResponseDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.CropCategoryResponseDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.ProfileDto;
import com.pathway.geokrishi.Boardcast.NetworkChangeReceiver;
import com.pathway.geokrishi.adapter.GridViewAdapter;
import com.pathway.geokrishi.fragment.DashBoardFragment;
import com.pathway.geokrishi.utils.AppConstant;
import com.pathway.geokrishi.utils.AppUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;


public class ProfileActivity extends BaseActivity implements View.OnClickListener{
    Dialog pDialog;
    Button btncancel,btnSignIn;

    SharedPreferences sharedPref;
    private Callback<ProfileDto> profileDtoCallback;
     public static ProfileDto.Data profiledata;
    Dialog  profiledailog;
    Spinner profilespinner;
    String profiletypestring;
    int userposition;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();
        profiledata=new ProfileDto.Data();
        profileDtoCallback = new Callback<ProfileDto>() {
            @Override
            public void onResponse(Call<ProfileDto> call, retrofit2.Response<ProfileDto> response) {
                pDialog.dismiss();
                if (response.isSuccessful()) {

                    ProfileDto ProfileResponse = response.body();
                    if (ProfileResponse.getStatus() == 0) {
                    } else if (ProfileResponse.getStatus() == 1) {
                        profiledata = ProfileResponse.getData();
                        startActivity(new Intent(ProfileActivity.this, FarmerProfileActivity.class));

                    } else if (ProfileResponse.getStatus() == 2) {
                        profiledata = ProfileResponse.getData();

                        startActivity(new Intent(ProfileActivity.this, ExtensionProfileActivity.class));
                    } else if (ProfileResponse.getStatus() == 3) {

                        profiledata = ProfileResponse.getData();
                            startActivity(new Intent(ProfileActivity.this, CommunityProfileActivity.class));

                    }

                } else {
                    AppUtils.errordialog(ProfileActivity.this, AppConstant.Server_Error);

                }
            }

            @Override
            public void onFailure(Call<ProfileDto> call, Throwable t) {
                pDialog.dismiss();
                AppUtils.errordialog(ProfileActivity.this, t.getMessage());;

            }
        };
        if (AppUtils.checkInternetConnection(getApplicationContext())) {
            pDialog = CustomDialogs.progressDialog(ProfileActivity.this, "Loading");
            pDialog.show();
          //
             ApiManager.UserProfile(profileDtoCallback, AppUtils.userId(ProfileActivity.this));
           // ApiManager.UserProfile(profileDtoCallback, "13");
        } else {
            pDialog = CustomDialogs.progressDialog(ProfileActivity.this, "Loading");
            pDialog.dismiss();
        }


    }

    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }

    @Override
    protected int getActivityID() {
        return 0;
    }

    @Override
    protected int getToolbarTitle() {

        return R.string.userorofile;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.choose_profile_layout;
    }

    @Override
    protected int getDrawerLayout() {
        return 0;
    }

    @Override
    protected int getNavigationView() {
        return 0;
    }

    @Override
    protected Activity getInstance() {
        return null;
    }

    public void init() {
        profilespinner=(Spinner) findViewById(R.id.profilespinner);
        btncancel=(Button) findViewById(R.id.btncancel);
        btnSignIn=(Button) findViewById(R.id.btnsubmit);
        profiletypeadpater();
        btnSignIn.setOnClickListener(this);
        btncancel.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    public void profiletypeadpater() {
        List<String> relationgroup = Arrays.asList(getResources().getStringArray(R.array.profiletype_array));
        ArrayAdapter<String> relationadapter = new ArrayAdapter<>(getApplicationContext(),
                R.layout.spinner_list_item, relationgroup);
        profilespinner.setAdapter(relationadapter);
        profilespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub
                profiletypestring = String.valueOf(profilespinner
                        .getSelectedItem());
                userposition=arg2;
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v==btnSignIn){
            if(userposition==0){
                startActivity(new Intent(ProfileActivity.this, FarmerAddActivity.class));
                finish();
            }
            else if (userposition==1){
                startActivity(new Intent(ProfileActivity.this, ExtensionWorkerAddActivity.class));
                finish();
            }
            else{
                startActivity(new Intent(ProfileActivity.this, CommunityAddActivity.class));
                finish();
            }

        }
        if(v==btncancel){
            finish();
        }
    }

@Override
public void onResume(){
    super.onResume();
}

}
