package com.pathway.geokrishi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.pathway.geokrishi.Boardcast.NetworkChangeReceiver;
import com.pathway.geokrishi.Interface.Sidemenu;
import com.pathway.geokrishi.MainDashBoard.DashboardActivity;
import com.pathway.geokrishi.adapter.MyPageAdapter;
import com.pathway.geokrishi.dtos.MonthInfo;
import com.pathway.geokrishi.fragment.AboutFragment;
import com.pathway.geokrishi.fragment.DashBoardFragment;

import com.pathway.geokrishi.fragment.SearchFragment;
import com.pathway.geokrishi.fragment.SettingFragment;
import com.pathway.geokrishi.utils.AppUtils;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends BaseActivity implements Sidemenu {

    public static ArrayList<String> selections1 = new ArrayList<String>();
    public static ArrayList<String> selectionsInterger = new ArrayList<String>();
    List<MonthInfo> monthList;
    private Calendar _calendar;
    int month;
    static Context context;
    List<Fragment> fragments;
    ImageView ivAdd;
    AboutFragment aboutFragment = new AboutFragment();
    DashBoardFragment dashboardFragment = new DashBoardFragment();
    SearchFragment searchFragment = new SearchFragment();

    SettingFragment settingFragment = new SettingFragment();
    public static FrameLayout fl_floating;
    public static RelativeLayout relativelayot_ll;
    ViewPager viewPager;
    MyPageAdapter pagerAdapter;
    View avoutView, dashboardView, searchView, notifyView, settingView;
    TextView textViewAbout, textViewDashboard, textVieww_search, textVieww_notify, textView_setting, textviewNext;
    ImageView iv_search, iv_notify, iv_about, iv_dashboard, iv_setting;
    TabLayout tabLayout;
    int tabPosition;
    private String TAG = "MainActivity";
    AppBarLayout appBarLayout;
    Spinner monthspinner;
    public static String monthString;
    FloatingActionButton fav_ok;
    public static String monthidString;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);;
        _calendar = Calendar.getInstance();
        month = _calendar.get(Calendar.MONTH);
        context = MainActivity.this;
        fav_ok = (FloatingActionButton) findViewById(R.id.fav_ok);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        textviewNext = (TextView) findViewById(R.id.textviewNext);
        monthspinner = (Spinner) findViewById(R.id.monthspinner);
        fl_floating = (FrameLayout) findViewById(R.id.fl_floating);
        relativelayot_ll = (RelativeLayout) findViewById(R.id.relativelayot_ll);
        final Animation animation = new AlphaAnimation(1, 0); // Change alpha from fully visible to invisible
        animation.setDuration(1000); // duration - half a second
        animation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
        animation.setRepeatCount(Animation.INFINITE); // Repeat animation infinitely
        animation.setRepeatMode(Animation.REVERSE);
        fav_ok.startAnimation(animation);

        fav_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selections1.size() != 0) {
                    startActivity(new Intent(MainActivity.this,
                            GeoLocationActivity.class));
                } else {
                    Toast.makeText(MainActivity.this, getText(R.string.please_select_atleast), Toast.LENGTH_SHORT).show();
                }
            }
        });

        ivAdd = (ImageView) findViewById(R.id.iv_add);
        appBarLayout = (AppBarLayout) findViewById(R.id.appBarLayout);

        init();
        monthadapter();
        initViewPagerAndTabs();


    }
    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }
    @Override
    protected int getActivityID() {
        return 0;
    }
    @Override
    protected int getToolbarTitle() {
        return R.string.app_name;
    }
    @Override
    protected int getResourceLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected int getDrawerLayout() {
        return 0;
    }

    @Override
    protected int getNavigationView() {
        return 0;
    }

    @Override
    protected Activity getInstance() {
        return MainActivity.this;
    }
    private void init() {
        fragments = new ArrayList<>();
       // fragments.add(aboutFragment);
        fragments.add(dashboardFragment);
       // fragments.add(searchFragment);
        //fragments.add(notifyFragment);
        //fragments.add(settingFragment);
        pagerAdapter = new MyPageAdapter(this.getSupportFragmentManager(), fragments);
        viewPager.setAdapter(pagerAdapter);
      //  viewPager.setOffscreenPageLimit(5);
        viewPager.setCurrentItem(1);
        tabLayout.setupWithViewPager(viewPager);
    }
    private void initViewPagerAndTabs() {
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabPosition = tab.getPosition();

                if (tab.getPosition() == 1) {
                    textViewDashboard.setTextColor(ContextCompat.getColor(context, android.R.color.white));
                    fl_floating.setVisibility(View.VISIBLE);
                    relativelayot_ll.setVisibility(View.VISIBLE);
                    viewPager.setCurrentItem(1);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager.addOnPageChangeListener(new PageListener());
    }
    @Override
    public void sendDataToActivity(String data) {
        if (data.equals("abc")) {
            drawer.closeDrawer(navigationView);
        }
    }
    private class PageListener extends ViewPager.SimpleOnPageChangeListener {
        public void onPageSelected(int position) {

        }
    }

    public List getmonth() {
        monthList = new ArrayList<>();
        monthList.add(new MonthInfo("All", "0"));
        monthList.add(new MonthInfo("Jan", "1"));
        monthList.add(new MonthInfo("Feb ", "2"));
        monthList.add(new MonthInfo("Mar", "3"));
        monthList.add(new MonthInfo("Apr", "4"));
        monthList.add(new MonthInfo("May", "5"));
        monthList.add(new MonthInfo("Jun", "6"));
        monthList.add(new MonthInfo("Jul", "7"));
        monthList.add(new MonthInfo("Aug", "8"));
        monthList.add(new MonthInfo("Sep", "9"));
        monthList.add(new MonthInfo("Oct", "10"));
        monthList.add(new MonthInfo("Nov", "11"));
        monthList.add(new MonthInfo("Dec", "12"));
        return monthList;
    }
    public List getmonthnepali() {
        monthList = new ArrayList<>();
        monthList.add(new MonthInfo("सबै", "0"));
        monthList.add(new MonthInfo("बैशाख-जेठ", "5"));
        monthList.add(new MonthInfo("जेठ-असार ", "6"));
        monthList.add(new MonthInfo("असार-श्रावण", "7"));
        monthList.add(new MonthInfo("श्रावण-भदौ", "8"));
        monthList.add(new MonthInfo("भदौ-आश्विन", "9"));
        monthList.add(new MonthInfo("आश्विन-कार्तिक", "10"));
        monthList.add(new MonthInfo("कार्तिक-मंसिर", "11"));
        monthList.add(new MonthInfo("पमंसिर-पुष", "12"));
        monthList.add(new MonthInfo("पुष-माघ", "1"));
        monthList.add(new MonthInfo("माघ-फाल्गुन", "2"));
        monthList.add(new MonthInfo("फाल्गुन-चैत्र", "3"));
        monthList.add(new MonthInfo("चैत्र-बैशाख", "4"));
        return monthList;
    }
    public void monthadapter() {

if(AppUtils.language(MainActivity.this).equals("en")){
    ArrayAdapter<MonthInfo> monthadapter = new ArrayAdapter<>(
            getApplicationContext(), R.layout.spinner_list_item,
            getmonth());
    monthspinner.setAdapter(monthadapter);
}
else{
    ArrayAdapter<MonthInfo> monthadapter = new ArrayAdapter<>(
            getApplicationContext(), R.layout.spinner_list_item,
            getmonthnepali());
    monthspinner.setAdapter(monthadapter);
}

        monthspinner.setOnItemSelectedListener(new spinnerselector());
    }
    public class spinnerselector implements AdapterView.OnItemSelectedListener {

        public spinnerselector() {

        }

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            monthString = parent.getItemAtPosition(pos).toString();
            monthidString=monthList.get(pos).getMth();
        }

        public void onNothingSelected(AdapterView parent) {
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        try {
        } catch (Exception ex) {

        }

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(MainActivity.this, DashboardActivity.class));
        finish();
    }


}
