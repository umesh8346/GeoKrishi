package com.pathway.geokrishi.Openweather;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.text.style.SuperscriptSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.pathway.geokrishi.Openweather.WeatherHourDTO.List;
import com.pathway.geokrishi.R;
import com.pathway.geokrishi.utils.AppUtils;
import com.pathway.geokrishi.utils.DonutProgress;

import java.util.ArrayList;


public class HourWeatherAdapter extends RecyclerView.Adapter<HourWeatherAdapter.ViewHolder> {
    private Context mContext;
    ArrayList<List> hourlist = new ArrayList<>();
    SpannableStringBuilder mSSBuilder;
    String temp;

    public HourWeatherAdapter(Context c, ArrayList<List> p) {
        this.mContext = c;
        this.hourlist = p;


    }


    @Override
    public int getItemCount() {
        return hourlist.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView textviewtime, texviewtphaseicon, textviewtemp, textviewRain, textviewsnow, textviewPressure,textinfo;
        public View mTextView;
        ImageView imageweather;
        DonutProgress tempprofressbar, maxtempprofressbar, mintempprofressbar, humdityprofressbar;

        public ViewHolder(View v) {
            super(v);
            mTextView = v;

        }

    }


    @Override
    public HourWeatherAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {
        // create a new view
        View vi = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.hourweather_item_layout, parent, false);
        ViewHolder vh = new ViewHolder(vi);

        vh.textviewtime = (TextView) vi.findViewById(R.id.textviewtime);
        vh.texviewtphaseicon = (TextView) vi.findViewById(R.id.texviewtphaseicon);
        vh.textinfo = (TextView) vi.findViewById(R.id.textinfo);
        // vh.textviewtemp = (TextView) vi.findViewById(R.id.textviewtemp);
        vh.imageweather = (ImageView) vi.findViewById(R.id.imageweather);
        vh.textviewRain = (TextView) vi.findViewById(R.id.textviewRain);
        vh.textviewsnow = (TextView) vi.findViewById(R.id.textviewsnow);
        vh.textviewPressure = (TextView) vi.findViewById(R.id.textviewPressure);
        vh.tempprofressbar = (DonutProgress) vi.findViewById(R.id.tempprofressbar);
        vh.maxtempprofressbar = (DonutProgress) vi.findViewById(R.id.maxtempprofressbar);
        vh.mintempprofressbar = (DonutProgress) vi.findViewById(R.id.mintempprofressbar);
        vh.humdityprofressbar = (DonutProgress) vi.findViewById(R.id.humdityprofressbar);


        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String text = null;


        SuperscriptSpan superscriptSpan = new SuperscriptSpan();
        holder.textviewtime.setText((hourlist.get(position).getDtTxt()));
        holder.texviewtphaseicon.setText(hourlist.get(position).getWeather().get(0).getDescription());
        holder.textviewRain.setText(String.valueOf(hourlist.get(position).getWind().getSpeed())+"mm");

        holder.textviewPressure.setText(String.valueOf(hourlist.get(position).getMain().getPressure())+"hPa");
        try {
            holder.textviewsnow.setText(String.valueOf(hourlist.get(position).getSnow().get3h()));
        }
        catch(Exception ex){
            holder.textviewsnow.setText("0.00");
        }
        GlideDrawableImageViewTarget imageViewTarget2 = new GlideDrawableImageViewTarget(holder.imageweather);


        Glide.with(mContext).load("http://openweathermap.org/img/w/" + hourlist.get(position).getWeather().get(0).getIcon() + ".png").
                into(imageViewTarget2);

        holder.tempprofressbar.setProgress(hourlist.get(position).getMain().getTemp().intValue() - 272);
        holder.maxtempprofressbar.setProgress(hourlist.get(position).getMain().getTempMax().intValue() - 272);
        holder.mintempprofressbar.setProgress(hourlist.get(position).getMain().getTempMin().intValue() - 272);
        temp = String.valueOf(hourlist.get(position).getMain().getTempMin().intValue() - 272) + "oC";
        mSSBuilder = new SpannableStringBuilder(temp);
//        holder.mintempprofressbar.setText(Html.fromHtml("hello"));
        holder.humdityprofressbar.setSuffix("%");

        holder.humdityprofressbar.setProgressWithAnimation(hourlist.get(position).getMain().getHumidity(), 1);
        holder.textinfo.setText(Html.fromHtml("Temperature unit <sup>o</sup>C"));




    }
}