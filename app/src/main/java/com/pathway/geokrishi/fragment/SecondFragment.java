package com.pathway.geokrishi.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.pathway.geokrishi.R;


public class SecondFragment extends Fragment {
    Context mContext;
    static String type;

    // if value needed
    public static SecondFragment getInstanceFragment() {
        SecondFragment fragment = new SecondFragment();

        return fragment;

    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        mContext = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;

        view = inflater.inflate(R.layout.first_layout, container,
                false);
        ImageView imageview=(ImageView)view.findViewById(R.id.imageview);
        imageview.setImageResource(R.drawable.slide2);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);

    }
}
