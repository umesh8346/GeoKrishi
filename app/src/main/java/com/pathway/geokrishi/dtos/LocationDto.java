package com.pathway.geokrishi.dtos;


public class LocationDto {
    String location;
    String latitude;
    String longitude;

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
    public LocationDto(String location , String  latitude,String longitude) {
        super();
       this.location = location;
        this.latitude=latitude;
        this.longitude=longitude;

    }

    @Override
    public String toString()
    {
        return this.location;

    }
}
