package com.pathway.geokrishi.CropCalender;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.pathway.geokrishi.ApiController.ApiManager;
import com.pathway.geokrishi.ApiController.ResponseDTO.CropCalenderDTO;
import com.pathway.geokrishi.ApiController.ResponseDTO.CropExcelDto;
import com.pathway.geokrishi.BaseActivity;
import com.pathway.geokrishi.Interface.ProgressbarInterface;
import com.pathway.geokrishi.R;
import com.pathway.geokrishi.adapter.CalendarListAdapter;
import com.pathway.geokrishi.utils.AppConstant;
import com.pathway.geokrishi.utils.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;

public class CropCalendarDetailActivity extends BaseActivity implements ProgressbarInterface {

    ViewPager viewPager;
    TabLayout viewPagerTab;
    FragmentManager fragManager;
    ProgressBar progressBar;
    ProgressbarInterface progressbarInterface;
    String lat,lng,jsonlatlong;
    private Callback<CropCalenderDTO> getCropCalenderDTOCallback;
    private Callback<CropExcelDto> getcropExcel;
    ArrayList<CropCalenderDTO.Data> calenderlist;
    CalendarListAdapter calendarListAdapter;
   JSONArray array;
    ImageButton Imagebuttondownloadimage;
    String cropcalendarexcalurl;
    String latlngarray;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        lat = getIntent().getExtras().getString("latitude");
        jsonlatlong = getIntent().getExtras().getString("json");
        latlngarray= (jsonlatlong.substring(1, jsonlatlong.length()-1));
        System.out.println("latlngarray==="+ latlngarray);

        try {
             array = new JSONArray(jsonlatlong);
            System.out.println("array=="+array.toString(2));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        lng = getIntent().getExtras().getString("longitude");
        init();
    }

    public void init() {
        progressbarInterface=this;
        fragManager = getSupportFragmentManager();
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        progressBar= (ProgressBar) findViewById(R.id.progressBar);
        AppUtils.showprogressbar(getApplicationContext(),progressBar,this);

        Imagebuttondownloadimage=(ImageButton)findViewById(R.id.Imagebuttondownloadimage);
        Imagebuttondownloadimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File d = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                String download = cropcalendarexcalurl;

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri
                        .parse(download));
                startActivity(intent);

            }
        });
        viewPagerTab = (TabLayout) findViewById(R.id.viewpagertab);

        getCropCalenderDTOCallback = new Callback<CropCalenderDTO>() {
            @Override
            public void onResponse(Call<CropCalenderDTO> call, retrofit2.Response<CropCalenderDTO> response) {
                //AppLog.d(TAG, response.body().toString());

                if (response.isSuccessful()) {
                    AppUtils.hideprogressbar(getApplicationContext(),progressBar,progressbarInterface);
                    CropCalenderDTO cropCategoryResponse = response.body();
                    if (cropCategoryResponse.getStatus() == 0) {

                        calenderlist = cropCategoryResponse.getData();

                        for (int i = 0; i < calenderlist.size(); i++) {
                            viewPagerTab.addTab(viewPagerTab.newTab().setText(calenderlist.get(i).getMonth()));

                        }
                        calendarListAdapter = new CalendarListAdapter(CropCalendarDetailActivity.this,calenderlist, viewPagerTab.getTabCount(),cropcalendarexcalurl);
                        viewPager.setAdapter(calendarListAdapter);
                        viewPager.addOnPageChangeListener(new
                                TabLayout.TabLayoutOnPageChangeListener(viewPagerTab));
                        viewPager.setOffscreenPageLimit(2);

                    } else if (cropCategoryResponse.getStatus() == 1) {
                        AppUtils.hideprogressbar(getApplicationContext(),progressBar,progressbarInterface);
                        AppUtils.errordialog(CropCalendarDetailActivity.this, AppConstant.Server_Error);
                    }


                } else {
                    AppUtils.hideprogressbar(getApplicationContext(),progressBar,progressbarInterface);
                    AppUtils.errordialog(CropCalendarDetailActivity.this, AppConstant.Server_Error);

                }
            }

            @Override
            public void onFailure(Call<CropCalenderDTO> call, Throwable t) {

                AppUtils.errordialog(CropCalendarDetailActivity.this, t.getMessage());
            }
        };

        getcropExcel = new Callback<CropExcelDto>() {
            @Override
            public void onResponse(Call<CropExcelDto> call, retrofit2.Response<CropExcelDto> response) {
                //AppLog.d(TAG, response.body().toString());

                if (response.isSuccessful()) {
                    AppUtils.hideprogressbar(getApplicationContext(),progressBar,progressbarInterface);
                    CropExcelDto cropExcelResponse = response.body();
                    if (cropExcelResponse.getStatus() == 0) {
                        cropcalendarexcalurl=cropExcelResponse.getData();

                    } else if (cropExcelResponse.getStatus() == 1) {
                        AppUtils.hideprogressbar(getApplicationContext(),progressBar,progressbarInterface);
                        AppUtils.errordialog(CropCalendarDetailActivity.this, AppConstant.Server_Error);
                    }


                } else {
                    AppUtils.hideprogressbar(getApplicationContext(),progressBar,progressbarInterface);
                    AppUtils.errordialog(CropCalendarDetailActivity.this, AppConstant.Server_Error);

                }
            }

            @Override
            public void onFailure(Call<CropExcelDto> call, Throwable t) {

                AppUtils.errordialog(CropCalendarDetailActivity.this, t.getMessage());
            }
        };





        if (AppUtils.checkInternetConnection(CropCalendarDetailActivity.this)) {

            ApiManager.getCalendarList(getCropCalenderDTOCallback,latlngarray,"Polygon",AppUtils.apilanguage(getApplicationContext()));
            ApiManager.CropExcel(getcropExcel,latlngarray,"Polygon",AppUtils.apilanguage(getApplicationContext()));
        }else{
        }


        viewPagerTab.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }

        });


    }


    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }

    @Override
    protected int getActivityID() {
        return 0;
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.crop_calendar;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.cropcalender_detail_layout;
    }

    @Override
    protected int getDrawerLayout() {
        return 0;
    }

    @Override
    protected int getNavigationView() {
        return 0;
    }

    @Override
    protected Activity getInstance() {
        return null;
    }


    @Override
    public void showprogressbar(ProgressBar progressBar) {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideprogressbar(ProgressBar progressBar) {
        progressBar.setVisibility(View.INVISIBLE);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //startActivity(new Intent(C.this, MainActivity.class));
        finish();
    }


    private class DownloadTask extends AsyncTask<String, Integer, String> {

        private Context context;
        private PowerManager.WakeLock mWakeLock;

        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();


                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }

                // this will be useful to display download percentage
                // might be -1: server did not report the length
                int fileLength = connection.getContentLength();

                // download the file
                input = connection.getInputStream();
                String storageDir =Environment.getExternalStorageDirectory()
                        + "/excel";
                output = new FileOutputStream(storageDir);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }

    }
}
