package com.pathway.geokrishi.FarmCalcualtion;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.widget.ProgressBar;

import com.pathway.geokrishi.BaseActivity;
import com.pathway.geokrishi.Interface.ProgressbarInterface;
import com.pathway.geokrishi.R;

import com.pathway.geokrishi.adapter.FarmListAdapter;
import com.pathway.geokrishi.utils.AppUtils;


public class FarmCalculationActivity extends BaseActivity  implements ProgressbarInterface {
    ViewPager viewPager;
    TabLayout viewPagerTab;
    FragmentManager fragManager;
    ProgressBar progressBar;
    ProgressbarInterface progressbarInterface;
    FarmListAdapter farmListAdapter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    private void init(){
        progressbarInterface=this;
        fragManager = getSupportFragmentManager();
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        progressBar= (ProgressBar) findViewById(R.id.progressBar);
        AppUtils.showprogressbar(getApplicationContext(),progressBar,this);
        viewPagerTab = (TabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.addTab(viewPagerTab.newTab().setText("NPK Calculation"));
     //   viewPagerTab.addTab(viewPagerTab.newTab().setText("pH Calculation"));
        farmListAdapter = new FarmListAdapter(fragManager, viewPagerTab.getTabCount());

        System.out.print("spkcalculation===hit");
        viewPager.addOnPageChangeListener(new
                TabLayout.TabLayoutOnPageChangeListener(viewPagerTab));
        viewPager.setAdapter(farmListAdapter);
        viewPager.setOffscreenPageLimit(2);
    }

    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }

    @Override
    protected int getActivityID() {
        return 0;
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.fram_calculation;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.crop_viewpager_layout;
    }

    @Override
    protected int getDrawerLayout() {
        return 0;
    }

    @Override
    protected int getNavigationView() {
        return 0;
    }

    @Override
    protected Activity getInstance() {
        return null;
    }

    @Override
    public void showprogressbar(ProgressBar progressBar) {

    }

    @Override
    public void hideprogressbar(ProgressBar progressBar) {

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
