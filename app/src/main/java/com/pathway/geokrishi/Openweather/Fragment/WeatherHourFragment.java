package com.pathway.geokrishi.Openweather.Fragment;




import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.pathway.geokrishi.ApiController.ApiConfig;
import com.pathway.geokrishi.ApiController.ApiManager;
import com.pathway.geokrishi.Openweather.HourWeatherAdapter;
import com.pathway.geokrishi.Openweather.WeatherHourDTO.List;
import com.pathway.geokrishi.Openweather.WeatherHourDTO.WeatherHourDto;
import com.pathway.geokrishi.R;
import com.pathway.geokrishi.utils.AppConstant;
import com.pathway.geokrishi.utils.AppUtils;
import com.pathway.geokrishi.utils.ItemClickSupport;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class WeatherHourFragment extends Fragment {

     RecyclerView recycleviewHourWeather;
    private Callback<WeatherHourDto> hourWeatherDTOCallback;
  ArrayList<List> daylist = new ArrayList<>();
     HourWeatherAdapter houradpter;
String locationId;
    LinearLayoutManager mLayoutManager;
    String latitude,longitude;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.hourweather_layout, container, false);

        recycleviewHourWeather=(RecyclerView)view.findViewById(R.id.recycleviewHourWeather);
        hourWeatherDTOCallback = new Callback<WeatherHourDto>() {
            @Override
            public void onResponse(Call<WeatherHourDto> call, Response<WeatherHourDto> response) {
                System.out.println("weatherResponse==="+response.body());
                if (response.isSuccessful()) {

                    WeatherHourDto weatherResponse = response.body();
                    daylist=weatherResponse.getList();
                    hrweatheradapter();
                }

            }

            @Override
            public void onFailure(Call<WeatherHourDto> call, Throwable t) {
                System.out.println("error=="+t);

                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        };

        if (AppUtils.checkInternetConnection(getActivity())) {

            locationId=AppUtils.locationid(getActivity());
            System.out.println("url=="+locationId);
            if(locationId.equals("")){
                locationId="7649119";
            }

            latitude=String.valueOf(AppUtils.latitude(getActivity()));
            longitude=String.valueOf(AppUtils.longitude(getActivity()));

            String url= ApiConfig.openWeatherHourUrl.replace("{lat}","lat="+latitude+"&lon="+longitude);
            ApiManager.HourOpenWeather(hourWeatherDTOCallback,url);

        } else {
            AppUtils.errordialog(getActivity(), AppConstant.no_network);
        }

return view;
    }

    private void hrweatheradapter( ){
        mLayoutManager = new LinearLayoutManager(getActivity());
        HourWeatherAdapter houradapter= new HourWeatherAdapter(getActivity(),daylist);
        recycleviewHourWeather.setLayoutManager(mLayoutManager);
        recycleviewHourWeather.setAdapter(houradapter);

        ItemClickSupport.addTo(recycleviewHourWeather).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {



                }


            }
        );
    }


}
