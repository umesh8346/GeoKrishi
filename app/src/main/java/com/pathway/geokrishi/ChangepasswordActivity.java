package com.pathway.geokrishi;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.pathway.geokrishi.ApiController.ApiManager;
import com.pathway.geokrishi.ApiController.Networkcontroller;
import com.pathway.geokrishi.ApiController.ResponseDTO.ProfileResponseDto;
import com.pathway.geokrishi.Interface.NetworkInterFace;
import com.pathway.geokrishi.utils.AppConstant;
import com.pathway.geokrishi.utils.AppUtils;
import retrofit2.Call;
import retrofit2.Callback;


public class ChangepasswordActivity extends BaseActivity implements View.OnClickListener,NetworkInterFace{
    EditText edittextconfirmpassword, edittextnewpassword, edittextoldpassword;
String oldpasswordString,newpasswordString,confirmpasswordString;
    Button btnsubmit,btncancel;
    private Callback<ProfileResponseDto> profileRespnseDtoCallback;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();

    }
    private void init() {

        edittextconfirmpassword = (EditText) findViewById(R.id.edittextconfirmpassword);
        edittextnewpassword = (EditText) findViewById(R.id.edittextnewpassword);
        edittextoldpassword = (EditText) findViewById(R.id.edittextoldpassword);
        btnsubmit= (Button) findViewById(R.id.btnsubmit);
                btncancel= (Button) findViewById(R.id.btncancel);
        btnsubmit.setOnClickListener(this);
        btncancel.setOnClickListener(this);
        profileRespnseDtoCallback = new Callback<ProfileResponseDto>() {
            @Override
            public void onResponse(Call<ProfileResponseDto> call, retrofit2.Response<ProfileResponseDto> response) {
                //AppLog.d(TAG, response.body().toString());
                // pDialog.dismiss();
                if (response.isSuccessful()) {

                    ProfileResponseDto ProfileResponse = response.body();
                    if (ProfileResponse.getStatus() == 0) {

                        AppUtils.showdailog("Password change successfully", ChangepasswordActivity.this);
                    } else if (ProfileResponse.getStatus() == 1) {

                        AppUtils.errordialog(ChangepasswordActivity.this, ProfileResponse.getMessage());
                    }

                } else {
                    //  AppUtils.errordialog(ProfileActivity.this, AppConstant.Server_Error);

                }
            }

            @Override
            public void onFailure(Call<ProfileResponseDto> call, Throwable t) {
                // pDialog.dismiss();
                AppUtils.errordialog(ChangepasswordActivity.this, t.getMessage());
                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        };

    }

    private void getstring(){
        oldpasswordString= AppUtils.gettextstring(edittextoldpassword);
        newpasswordString= AppUtils.gettextstring(edittextnewpassword);
        confirmpasswordString= AppUtils.gettextstring(edittextconfirmpassword);
    }

    private  Boolean isvalid(){
        getstring();
        if(!AppUtils.isvalid(oldpasswordString)){
            edittextoldpassword.setError(getText(R.string.reduired_field));
            return false;
        }
        if(newpasswordString.length()<6){
            edittextnewpassword.setError(getText(R.string.reduired_field));
            return false;
        }
        if(!newpasswordString.equals(confirmpasswordString)){
            edittextconfirmpassword.setError(getText(R.string.reduired_field));
            return false;
        }
        return true;

    }

    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }

    @Override
    protected int getActivityID() {
        return 0;
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.change_password;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.changepassword_layout;
    }

    @Override
    protected int getDrawerLayout() {
        return 0;
    }

    @Override
    protected int getNavigationView() {
        return 0;
    }

    @Override
    protected Activity getInstance() {
        return null;
    }

    @Override
    public void onClick(View v) {
        if(v==btnsubmit){
            if(isvalid()) {
                Networkcontroller.checkInternetConnection(ChangepasswordActivity.this,this,"changepassword");
                        }
        }
         if(v==btncancel){
             finish();
         }
    }

    @Override
    public void network(Boolean aBoolean,String s) {
        if(aBoolean){
            ApiManager.UserChangepasswordResponse(profileRespnseDtoCallback, oldpasswordString, newpasswordString, AppUtils.userId(ChangepasswordActivity.this));
        }
        else{
            AppUtils.errordialog(ChangepasswordActivity.this, AppConstant.no_network);
        }

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
