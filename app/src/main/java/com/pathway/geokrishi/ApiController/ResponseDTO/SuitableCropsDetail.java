package com.pathway.geokrishi.ApiController.ResponseDTO;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dell on 5/22/2017.
 */

public class SuitableCropsDetail {

    @SerializedName("Status")
    public int Status;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public SuitableCropsDetail.Data getData() {
        return Data;
    }

    public void setData(SuitableCropsDetail.Data data) {
        Data = data;
    }

    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public static class Containers {
        public String getContainerName() {
            return ContainerName;
        }

        public void setContainerName(String containerName) {
            ContainerName = containerName;
        }

        public String getContainerValue() {
            return ContainerValue;
        }

        public void setContainerValue(String containerValue) {
            ContainerValue = containerValue;
        }

        @SerializedName("ContainerName")
        public String ContainerName;
        @SerializedName("ContainerValue")
        public String ContainerValue;
    }

    public static class Comment {
        @SerializedName("UserID")
        public int UserID;

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int userID) {
            UserID = userID;
        }

        public String getUserName() {
            return UserName;
        }

        public void setUserName(String userName) {
            UserName = userName;
        }

        public String getAvatarImage() {
            return AvatarImage;
        }

        public void setAvatarImage(String avatarImage) {
            AvatarImage = avatarImage;
        }

        public String getCommentDate() {
            return CommentDate;
        }

        public void setCommentDate(String commentDate) {
            CommentDate = commentDate;
        }

        public String getCommentText() {
            return CommentText;
        }

        public void setCommentText(String commentText) {
            CommentText = commentText;
        }

        public String getRating() {
            return Rating;
        }

        public void setRating(String rating) {
            Rating = rating;
        }

        @SerializedName("UserName")
        public String UserName;
        @SerializedName("AvatarImage")
        public String AvatarImage;
        @SerializedName("CommentDate")
        public String CommentDate;
        @SerializedName("CommentText")
        public String CommentText;
        @SerializedName("Rating")
        public String Rating;
    }

    public static class Data {
        @SerializedName("CropName")
        public String CropName;

        public String getCropName() {
            return CropName;
        }

        public void setCropName(String cropName) {
            CropName = cropName;
        }

        public String getCropValue() {
            return CropValue;
        }

        public void setCropValue(String cropValue) {
            CropValue = cropValue;
        }

        public String getSuitableVarieties() {
            return SuitableVarieties;
        }

        public void setSuitableVarieties(String suitableVarieties) {
            SuitableVarieties = suitableVarieties;
        }

        public String getCropImage() {
            return CropImage;
        }

        public void setCropImage(String cropImage) {
            CropImage = cropImage;
        }

        public ArrayList<SuitableCropsDetail.Containers> getContainers() {
            return Containers;
        }

        public void setContainers(ArrayList<SuitableCropsDetail.Containers> containers) {
            Containers = containers;
        }

        public ArrayList<SuitableCropsDetail.Comment> getComment() {
            return Comment;
        }

        public void setComment(ArrayList<SuitableCropsDetail.Comment> comment) {
            Comment = comment;
        }

        @SerializedName("CropValue")
        public String CropValue;
        @SerializedName("SuitableVarieties")
        public String SuitableVarieties;
        @SerializedName("CropImage")
        public String CropImage;
        @SerializedName("Containers")
        public ArrayList<Containers> Containers;
        @SerializedName("Comment")
        public ArrayList<Comment> Comment;
    }
}
