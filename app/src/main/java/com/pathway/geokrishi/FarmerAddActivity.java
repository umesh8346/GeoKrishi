package com.pathway.geokrishi;


import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.pathway.geokrishi.ApiController.ApiManager;
import com.pathway.geokrishi.ApiController.ResponseDTO.ProfileResponseDto;
import com.pathway.geokrishi.utils.AppUtils;
import com.pathway.geokrishi.utils.CircularProgressButton;
import java.util.Arrays;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
public class FarmerAddActivity extends BaseActivity implements View.OnClickListener {
    EditText edittextName, edittextfamilymember, edittextmobile,
            edittextlandholding;
    Spinner spinnerage,spinnerland;
    String nameString, ageString, genderString, familymemString, mobileString, landingholdingString,landunitString;
    RadioGroup radiogenderGroup;
    RadioButton radiomale, radiofemale;
    CircularProgressButton circularButton2;
    Gson gson;
    String userage,relativeelationship;
    JsonObject userjson, farmerjson;
    int ageint,relationint;
    private Callback<ProfileResponseDto> profileRespnseDtoCallback;
    CheckBox checkbodlocation;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
      //  animateButtons();
        init();
        ageadpater();
        landadpater();
    }

    private void init() {
        gson = new Gson();
        edittextName = (EditText) findViewById(R.id.edittextName);
        spinnerage = (Spinner) findViewById(R.id.spinnerage);
       spinnerland = (Spinner) findViewById(R.id.spinnerland);
        edittextfamilymember = (EditText) findViewById(R.id.edittextfamilymember);
        //edittextRepresentativename = (EditText) findViewById(R.id.edittextRepresentativename);
        edittextmobile = (EditText) findViewById(R.id.edittextmobile);
        edittextlandholding = (EditText) findViewById(R.id.edittextlandholding);
        radiogenderGroup = (RadioGroup) findViewById(R.id.radiogenderGroup);
        radiomale = (RadioButton) findViewById(R.id.radiomale);
        radiofemale = (RadioButton) findViewById(R.id.radiofemale);
        circularButton2 = (CircularProgressButton) findViewById(R.id.circularButton1);
        checkbodlocation= (CheckBox) findViewById(R.id.checkbodlocation);
        circularButton2.setOnClickListener(this);
        edittextName.addTextChangedListener(new addListenerOnTextChange());
        edittextfamilymember.addTextChangedListener(new addListenerOnTextChange());
        //edittextRepresentativename.addTextChangedListener(new addListenerOnTextChange());
        edittextmobile.addTextChangedListener(new addListenerOnTextChange());
        edittextlandholding.addTextChangedListener(new addListenerOnTextChange());


        profileRespnseDtoCallback = new Callback<ProfileResponseDto>() {
            @Override
            public void onResponse(Call<ProfileResponseDto> call, retrofit2.Response<ProfileResponseDto> response) {
                //AppLog.d(TAG, response.body().toString());
               // pDialog.dismiss();
                if (response.isSuccessful()) {

                    ProfileResponseDto ProfileResponse = response.body();
                    if (ProfileResponse.getStatus() == 0) {
                       AppUtils.showdailog("User profile Edit successfully",FarmerAddActivity.this);
                    } else if (ProfileResponse.getStatus() == 1) {
                        AppUtils.errordialog(FarmerAddActivity.this,ProfileResponse.getMessage());
                    }

                } else {
                  //  AppUtils.errordialog(ProfileActivity.this, AppConstant.Server_Error);

                }
            }

            @Override
            public void onFailure(Call<ProfileResponseDto> call, Throwable t) {
               // pDialog.dismiss();
                AppUtils.errordialog(FarmerAddActivity.this, t.getMessage());
                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        };

     //   getsettextEdittext();

    }

    public void getsettextEdittext(){
        edittextName.setText(ProfileActivity.profiledata.getFarmerName());
        userage=ProfileActivity.profiledata.getAge();

        edittextfamilymember.setText(ProfileActivity.profiledata.getNoOfFamilyMembers()+"");
      //  edittextRepresentativename.setText(ProfileActivity.profiledata.getFarmerRepresentativeName());
        edittextlandholding.setText(ProfileActivity.profiledata.getLandHoldingUnitArea()+"");
        edittextmobile.setText(ProfileActivity.profiledata.getMobile());
        if(ProfileActivity.profiledata.getGender().equals("M")){
            radiomale.setChecked(true);
        }
        else{
            radiofemale.setChecked(true);
        }

        relativeelationship=ProfileActivity.profiledata.getFarmerRepresentativeRelation();
        for(int i=0;i< Arrays.asList(getResources().getStringArray(R.array.age_array)).size();i++){
            if(userage.equals(Arrays.asList(getResources().getStringArray(R.array.age_array)).get(i))){
                ageint=i;
            }

        }

        for (int j=0;j< Arrays.asList(getResources().getStringArray(R.array.relationship_array)).size();j++){

//            System.out.println("relation==="+relativeelationship);
//            System.out.println("relationarray==="+Arrays.asList(getResources().getStringArray(R.array.relationship_array)).get(j));
            if(relativeelationship.equals((Arrays.asList(getResources().getStringArray(R.array.relationship_array)).get(j)))){
                relationint=j;
            }

        }


    }

    public void getstring() {

        nameString = AppUtils.gettextstring(edittextName);

        familymemString = AppUtils.gettextstring(edittextfamilymember);
       // representativenameString = AppUtils.gettextstring(edittextRepresentativename);

        mobileString = AppUtils.gettextstring(edittextmobile);
        landingholdingString = AppUtils.gettextstring(edittextlandholding);
        int selectgendertype = radiogenderGroup.getCheckedRadioButtonId();
        genderString = "";
        if (selectgendertype == R.id.radiomale) {
            genderString = "M";
        } else if (selectgendertype == R.id.radiofemale) {
            genderString = "F";
        }

    }

    public boolean isvalidation() {
        getstring();
        if (!AppUtils.isvalid(nameString)) {
            AppUtils.simulateErrorProgress(circularButton2);
            edittextName.setError(getText(R.string.reduired_field));
            return false;
        }


        if (!AppUtils.isvalid(familymemString)) {
            AppUtils.simulateErrorProgress(circularButton2);
            edittextfamilymember.setError(getText(R.string.reduired_field));
            return false;
        }
        if(mobileString.length()!=10){
            AppUtils.simulateErrorProgress(circularButton2);
            edittextmobile.setError(getText(R.string.reduired_field));
            return false;
        }


        return true;

    }

    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }

    @Override
    protected int getActivityID() {
        return 0;
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.farmer_profile;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.farmer_layout;
    }

    @Override
    protected int getDrawerLayout() {
        return 0;
    }

    @Override
    protected int getNavigationView() {
        return 0;
    }

    @Override
    protected Activity getInstance() {
        return null;
    }

    @Override
    public void onClick(View v) {

        if (v == circularButton2) {

            if (circularButton2.getProgress() == 0) {


            } else {
                circularButton2.setProgress(0);
            }
            if (isvalidation()) {

                farmerjson();

                ApiManager.UserProfileResponse( profileRespnseDtoCallback, "1", farmerjson);
               // callapi();
            }
        }
    }

    public void farmerjson() {

        userjson = new JsonObject();

        farmerjson = new JsonObject();

        try {
            farmerjson.addProperty("UserID", AppUtils.userId(FarmerAddActivity.this));
            farmerjson.addProperty("FarmerName", nameString);
            farmerjson.addProperty("Age", ageString);
            farmerjson.addProperty("Gender", genderString);
            farmerjson.addProperty("No_Of_Family_Members", familymemString);
            farmerjson.addProperty("Latitude", "27.5");
            farmerjson.addProperty("Longitude", "85.5");
//            farmerjson.addProperty("Farmer_Representative_Name", representativenameString);
//            farmerjson.addProperty("Farmer_Representative_Relation", representativerealtionString);
            farmerjson.addProperty("LandUnit", landunitString);
            farmerjson.addProperty("Mobile", mobileString);
            farmerjson.addProperty("LandHoldingUnitArea", landingholdingString);

        } catch (Exception ex) {

        }

    }


    public void ageadpater() {
        List<String> agegroup = Arrays.asList(getResources().getStringArray(R.array.age_array));
        ArrayAdapter<String> ageadapter = new ArrayAdapter<>(getApplicationContext(),
                R.layout.spinner_list_item, agegroup);

        spinnerage.setAdapter(ageadapter);
        spinnerage.setSelection(ageint);
        spinnerage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub

                ageString = String.valueOf(spinnerage
                        .getSelectedItem());
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

    }

    public void landadpater() {
        List<String> relationgroup = Arrays.asList(getResources().getStringArray(R.array.land_array));
        ArrayAdapter<String> landarray = new ArrayAdapter<>(getApplicationContext(),
                R.layout.spinner_list_item, relationgroup);

        spinnerland.setAdapter(landarray);
        spinnerland.setSelection(relationint);
        spinnerland.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub

                landunitString = String.valueOf(spinnerland
                        .getSelectedItem());
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });

    }


//    public void animateButtons() {
//        // int[] imageButtonIds = {R.id.animateButton};
//        int[] textViewIds = {R.id.cardA, R.id.cardB, R.id.cardC, R.id.cardD, R.id.cardE, R.id.cardF, R.id.cardG, R.id.cardH};
//
//        int i = 1;
//
//        for (int viewId : textViewIds) {
//            // Button imageButton = (Button) findViewById(viewId);
//            Animation fadeAnimation = AnimationUtils.loadAnimation(this, R.anim.fading_effect);
//            fadeAnimation.setStartOffset(i * 200);
//            //imageButton.startAnimation(fadeAnimation);
//
//            int textViewId = textViewIds[i - 1];
//            CardView cardview = (CardView) findViewById(textViewId);
//            cardview.startAnimation(fadeAnimation);
//
//            i++;
//        }
//
//
//    }





    private class addListenerOnTextChange implements TextWatcher {
        public addListenerOnTextChange() {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

            circularButton2.setProgress(0);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


}
