package com.pathway.geokrishi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.pathway.geokrishi.ApiController.Networkcontroller;
import com.pathway.geokrishi.Interface.NetworkInterFace;
import com.pathway.geokrishi.Openweather.WeatherActivity;


public class PlaningActivity  extends BaseActivity implements View.OnClickListener,NetworkInterFace{

    RelativeLayout relativelayotGrow;
    ImageButton imagebutton,image2button;
    LinearLayout linearlayoiut;
    TextView textinfo,textretry;
    ImageButton image1button;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // AppUtils.ActivitySetting(getApplicationContext(),"PlaningActivity");
//        mConnReceiver=new NetworkChangeReceiver();
//        registerReceiver(mConnReceiver,
//                new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        relativelayotGrow=(RelativeLayout)findViewById(R.id.relativelayotGrow);
        imagebutton =(ImageButton)findViewById(R.id.imagebutton);
        image2button =(ImageButton)findViewById(R.id.image2button);
        linearlayoiut =(LinearLayout)findViewById(R.id.linearlayoiut);
        imagebutton.setOnClickListener(this);
        image2button.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        if(v==imagebutton){
            Networkcontroller.checkInternetConnection(PlaningActivity.this,this,"planning");
        }
        if(v==image2button){
            Networkcontroller.checkInternetConnection(PlaningActivity.this,this,"wheather");
        }

    }

    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }

    @Override
    protected int getActivityID() {
        return 0;
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.geo;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.planing_layout;
    }

    @Override
    protected int getDrawerLayout() {
        return 0;
    }

    @Override
    protected int getNavigationView() {
        return 0;
    }

    @Override
    protected Activity getInstance() {
        return null;
    }

    @Override
    public void network(Boolean aBoolean, String s) {
if(aBoolean){
    if(s.equals("planning")) {
        startActivity(new Intent(PlaningActivity.this,
                MainActivity.class));
    }
            if(s.equals("wheather")){
                startActivity(new Intent(PlaningActivity.this,
                        WeatherActivity.class));
        }
}
else{
    Snackbar snackbar = Snackbar.make(linearlayoiut, "", Snackbar.LENGTH_LONG);
    Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
    View snackView = View.inflate(PlaningActivity.this,R.layout.snack_layout, null);
    textinfo=(TextView) snackView.findViewById(R.id.textinfo);
            textretry=(TextView) snackView.findViewById(R.id.textretry);
    image1button=(ImageButton) snackView.findViewById(R.id.image1button);
    final Animation animation = new AlphaAnimation(1, 0); // Change alpha from fully visible to invisible
    animation.setDuration(1000); // duration - half a second
    animation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
    animation.setRepeatCount(Animation.INFINITE); // Repeat animation infinitely
    animation.setRepeatMode(Animation.REVERSE);
    image1button.startAnimation(animation);
    layout.addView(snackView, 0);
    snackbar.show();

}
    }
}
