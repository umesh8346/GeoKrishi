package com.pathway.geokrishi.adapter;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.pathway.geokrishi.fragment.FirstFragment;
import com.pathway.geokrishi.fragment.SecondFragment;
import com.pathway.geokrishi.fragment.ThirdFragment;

public class FragmentViewPagerAdapter extends FragmentStatePagerAdapter {
	private int size;
	private String[] groups;
	public FragmentViewPagerAdapter(FragmentManager fragmentManager, String[] groupArray) {
		super(fragmentManager);
		this.size = groupArray.length;
		this.groups = groupArray;
	}

	@Override
	public android.support.v4.app.Fragment getItem(int position) {
		// TODO Auto-generated method stub
		switch (position) {
		case 0:
			return FirstFragment.getInstanceFragment();
		case 1:
			return SecondFragment.getInstanceFragment();
		case 2:
			return ThirdFragment.getInstanceFragment();
		default:
			return FirstFragment.getInstanceFragment();
		}

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		return groups[position % groups.length].toUpperCase();
	}

}
