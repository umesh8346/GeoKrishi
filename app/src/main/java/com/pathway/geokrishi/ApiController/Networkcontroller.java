package com.pathway.geokrishi.ApiController;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.pathway.geokrishi.Interface.NetworkInterFace;
public class Networkcontroller {
    public static void  checkInternetConnection(Context context, NetworkInterFace networkInterFace,String activity) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            NetworkInfo[] netInfo = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo) {
                if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                    if (ni.isConnected())
                        haveConnectedWifi = true;
                if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                    if (ni.isConnected())
                        haveConnectedMobile = true;
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
        networkInterFace.network(haveConnectedWifi ||haveConnectedMobile,activity);
    }

}
