package com.pathway.geokrishi;

import android.app.Activity;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.messaging.FirebaseMessaging;
import com.pathway.geokrishi.ApiController.ApiManager;
import com.pathway.geokrishi.ApiController.ResponseDTO.LoginResponseDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.RegisterResponseDto;
import com.pathway.geokrishi.Fcm.NotificationUtils;
import com.pathway.geokrishi.MainDashBoard.DashboardActivity;
import com.pathway.geokrishi.utils.AppConstant;
import com.pathway.geokrishi.utils.AppUtils;
import com.pathway.geokrishi.utils.Config;
import com.pathway.geokrishi.utils.GPSTracker;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Login_Activity extends Activity implements View.OnClickListener {

    String usernameString, passwordString, accesstoken, facebookID, fbemail, facebookfirstname,
            facebooklastname, user_facebookimage, language;
    LoginButton login_button;
    EditText edittextUsername, edittextPassword;
    Button fb, btnSignin;
    TextView textviewRegister,textviewforgetpassword;
    ProgressDialog progressDialog;
    LinearLayout linearlayout;
    TextView textinfo, textretry;
    ImageButton image1button;
    GPSTracker gps;
    double latitude, longitude;
    String regId;
    String androidID;
    private CallbackManager callbackManager;
    private Callback<LoginResponseDto> loginResponseDtoCallback;
    private Callback<RegisterResponseDto> userRegisterResponseDtoCallback;
    private Locale locale;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        language = AppUtils.language(Login_Activity.this);
        if (language.equals("")) {
            languagesetting();
            language = AppUtils.language(Login_Activity.this);
            getlanguage();
        } else {
            getlanguage();
        }
        androidID = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        setContentView(R.layout.login_layout);
        if (!AppUtils.userId(Login_Activity.this).equals("")) {
            callintent();
        }
        getlatitudeandlongitude();
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {

                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {

                    String message = intent.getStringExtra("message");

                }
            }
        };
        displayFirebaseRegId();
        linearlayout = (LinearLayout) findViewById(R.id.linearlayout);
        progressDialog = new ProgressDialog(Login_Activity.this, R.style.AppCompatAlertDialogStyle);
        login_button = (LoginButton) findViewById(R.id.login_button);
        btnSignin = (Button) findViewById(R.id.btnSignIn);
        edittextUsername = (EditText) findViewById(R.id.edittextUsername);
        edittextPassword = (EditText) findViewById(R.id.edittextPassword);
        textviewRegister = (TextView) findViewById(R.id.textviewRegister);
        textviewforgetpassword= (TextView) findViewById(R.id.textviewforgetpassword);
        fb = (Button) findViewById(R.id.fb);
        fb.setOnClickListener(this);
        textviewforgetpassword.setOnClickListener(this);
        btnSignin.setOnClickListener(this);
        textviewRegister.setOnClickListener(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        login_button.setReadPermissions("public_profile", "email");
        callbackManager = CallbackManager.Factory.create();
        AppUtils.transparentStatusBar(getWindow());
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {

                                try {


                                    accesstoken = AccessToken.getCurrentAccessToken().getToken();
                                    facebookID = object.getString("id");

                                    try {

                                        fbemail = object.getString("email");
                                    } catch (Exception ex) {
                                        fbemail = "";
                                    }

                                    facebookfirstname = object.getString("first_name");
                                    facebooklastname = object.getString("last_name");
                                    user_facebookimage = "";
//                                    if (object.has("picture")) {
                                    user_facebookimage = "https://graph.facebook.com/" + facebookID + "/picture?width=" + 480 + "&height=" + 480;

                                    ApiManager.FacebookRegistration(userRegisterResponseDtoCallback, "", facebookfirstname, facebooklastname, facebookID, accesstoken, fbemail, user_facebookimage, regId, androidID,String.valueOf(longitude),String.valueOf(latitude));

                                } catch (Exception ex) {
                                    System.out.println();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();


            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }

        });
        loginResponseDtoCallback = new Callback<LoginResponseDto>() {
            @Override
            public void onResponse(Call<LoginResponseDto> call, Response<LoginResponseDto> response) {
                if (response.isSuccessful()) {

                    LoginResponseDto loginResponse = response.body();
                    if (loginResponse.getStatus() == 0) {
                        AppUtils.loginshareprefrence(getApplicationContext(), loginResponse.getData().getUserID() + "", loginResponse.getData().getFirstName(),
                                loginResponse.getData().getLastName(), loginResponse.getData().getUserName(), loginResponse.getData().getEmail(), loginResponse.getData().UserName,
                                loginResponse.getData().getOrganization(), loginResponse.getData().getProfession(),
                                String.valueOf(loginResponse.getData().getLongitude()), String.valueOf(loginResponse.getData().getLatitude()), loginResponse.getData().getAvatarImage());
                        callintent();
                    } else if (loginResponse.getStatus() == 1) {
                        progressDialog.dismiss();
                        AppUtils.errordialog(Login_Activity.this, loginResponse.getMessage());
                    }

                } else {
                    progressDialog.dismiss();
                    AppUtils.errordialog(Login_Activity.this, AppConstant.Server_Error);
                }

            }

            @Override
            public void onFailure(Call<LoginResponseDto> call, Throwable t) {

            }
        };

        userRegisterResponseDtoCallback = new Callback<RegisterResponseDto>() {
            @Override
            public void onResponse(Call<RegisterResponseDto> call, Response<RegisterResponseDto> response) {
                if (response.isSuccessful()) {

                    RegisterResponseDto userRegisterResponse = response.body();
                    if (userRegisterResponse.getStatus() == 0) {
                        AppUtils.loginshareprefrence(getApplicationContext(), userRegisterResponse.getData().getUserID() + "", userRegisterResponse.getData().getFirstName(),
                                userRegisterResponse.getData().getLastName(), userRegisterResponse.getData().getUserName(), userRegisterResponse.getData().getEmail(), userRegisterResponse.getData().UserName,
                                userRegisterResponse.getData().getOrganization(), userRegisterResponse.getData().getProfession(), String.valueOf(userRegisterResponse.getData().getLongitude()),
                                String.valueOf(userRegisterResponse.getData().getLatitude()),userRegisterResponse.getData().getAvatarImage());
                        callintent();
                        progressDialog.dismiss();
                    } else if (userRegisterResponse.getStatus() == 1) {
                        progressDialog.dismiss();
                        AppUtils.errordialog(Login_Activity.this, userRegisterResponse.getMessage());
                    }

                } else {
                    progressDialog.dismiss();
                    AppUtils.errordialog(Login_Activity.this, AppConstant.Server_Error);
                }
            }

            @Override
            public void onFailure(Call<RegisterResponseDto> call, Throwable t) {
                System.out.println("errorinfb==="+t.getMessage());

            }
        };


    }
    public void getstring() {
        usernameString = AppUtils.gettextstring(edittextUsername);
        passwordString = AppUtils.gettextstring(edittextPassword);
    }

    public boolean isvalidation() {
        getstring();

        if (!AppUtils.isvalid(usernameString)) {
            edittextUsername.setError(getText(R.string.reduired_field));
            return false;
        }
        if (!AppUtils.isvalid(passwordString)) {
            edittextPassword.setError(getText(R.string.reduired_field));
            return false;
        }
        return true;

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnSignIn:

                if (AppUtils.checkInternetConnection(Login_Activity.this)) {
                    if (isvalidation()) {
                        if (AppUtils.checkInternetConnection(getApplicationContext())) {
                            progressDialog.setMessage("Loading....");
                            progressDialog.setCancelable(false);
                            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                            progressDialog.show();
                            ApiManager.UserSignIn(loginResponseDtoCallback, usernameString, passwordString);

                        } else {
                            Toast.makeText(getApplicationContext(), "Please check your Internet connection", Toast.LENGTH_SHORT).show();
                        }

                    }

                } else {
                    shownonetwork();
                }

                break;
            case R.id.fb:

                if (AppUtils.checkInternetConnection(Login_Activity.this)) {
                    login_button.performClick();
                } else {
                    shownonetwork();

                }
                break;

            case R.id.textviewRegister:
                startActivity(new Intent(this,
                        SignupActivity.class));
                break;


            case R.id.textviewforgetpassword:
                startActivity(new Intent(this,
                        ForgetPasswordActivity.class));
                break;
        }
    }


    public void callintent() {
        startActivity(new Intent(Login_Activity.this, DashboardActivity.class));
        finish();
    }


    public void getlanguage() {
        if (language.equals("np")) {

            locale = new Locale(language);
            Locale.setDefault(locale);
            android.content.res.Configuration config = new android.content.res.Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        } else {

            locale = new Locale(language);
            Locale.setDefault(locale);
            android.content.res.Configuration config = new android.content.res.Configuration();
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
        }
    }

    public void languagesetting() {
        SharedPreferences sh_Pref_S1 = Login_Activity.this
                .getSharedPreferences("Language Credential",
                        Context.MODE_PRIVATE);
        SharedPreferences.Editor toEdit1 = sh_Pref_S1.edit();
        toEdit1.putString("LANGUAGE", "en");
        toEdit1.commit();
    }

    public void shownonetwork() {
        Snackbar snackbar = Snackbar.make(linearlayout, "", Snackbar.LENGTH_LONG);
        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        View snackView = View.inflate(Login_Activity.this, R.layout.snack_layout, null);
        textinfo = (TextView) snackView.findViewById(R.id.textinfo);
        textretry = (TextView) snackView.findViewById(R.id.textretry);
        image1button = (ImageButton) snackView.findViewById(R.id.image1button);
        final Animation animation = new AlphaAnimation(1, 0); // Change alpha from fully visible to invisible
        animation.setDuration(1000); // duration - half a second
        animation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
        animation.setRepeatCount(Animation.INFINITE); // Repeat animation infinitely
        animation.setRepeatMode(Animation.REVERSE);
        image1button.startAnimation(animation);
        layout.addView(snackView, 0);
        snackbar.show();

    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        regId = pref.getString("regId", null);
    }

    public void getlatitudeandlongitude() {
        if (ContextCompat.checkSelfPermission(Login_Activity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Login_Activity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Login_Activity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        } else {
            gps = new GPSTracker(Login_Activity.this, Login_Activity.this);

            if (gps.canGetLocation()) {

                latitude = gps.getLatitude();
                longitude = gps.getLongitude();

                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(this, Locale.getDefault());
                try {


                    addresses = geocoder.getFromLocation(latitude, longitude, 1);

                } catch (Exception ex) {

                }
            } else {

                gps.showSettingsAlert();
            }

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        getlatitudeandlongitude();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));
        NotificationUtils.clearNotifications(getApplicationContext());
    }
    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


}

