package com.pathway.geokrishi.dtos;

/**
 * Created by kishor on 15/05/17.
 */

public class CategoryDto {
    public String image;
    public String title;
    public boolean checkebox;


    public CategoryDto(String image,String title, boolean  checkbox){
        this.image = image;
        this.title= title;
        this.checkebox = checkbox;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isCheckebox() {
        return checkebox;
    }

    public void setCheckebox(boolean checkebox) {
        this.checkebox = checkebox;
    }



}
