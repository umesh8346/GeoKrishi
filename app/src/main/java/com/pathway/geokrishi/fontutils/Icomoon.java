package com.pathway.geokrishi.fontutils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class Icomoon extends android.support.v7.widget.AppCompatTextView {


    public static Typeface face = null;


    public Icomoon(Context context) {
        super(context);
    }

    private void initializeFont(Context context) {

        if(face==null) {
            face = Typeface.createFromAsset(context.getAssets(), "icomoon.ttf");
            this.setTypeface(face);
        }else{
            this.setTypeface(face);
        }
    }

    public Icomoon(Context context, AttributeSet attrs) {
        super(context, attrs);
        initializeFont(context);
    }

    public Icomoon(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initializeFont(context);
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

    }
}
