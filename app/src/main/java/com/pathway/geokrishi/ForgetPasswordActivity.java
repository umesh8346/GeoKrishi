package com.pathway.geokrishi;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.pathway.geokrishi.ApiController.ApiManager;
import com.pathway.geokrishi.ApiController.Networkcontroller;
import com.pathway.geokrishi.ApiController.ResponseDTO.ProfileResponseDto;
import com.pathway.geokrishi.Interface.NetworkInterFace;
import com.pathway.geokrishi.utils.AppConstant;
import com.pathway.geokrishi.utils.AppUtils;

import retrofit2.Call;
import retrofit2.Callback;

public class ForgetPasswordActivity extends  BaseActivity  implements View.OnClickListener,NetworkInterFace {
    EditText edittextEmail,edittextphone;
Button btnsubmit,btncancel;
    TextView textinfo;
    String emailstring,phonestring;
    private Callback<ProfileResponseDto> profileRespnseDtoCallback;
    ProgressDialog progressDialog;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        progressDialog = new ProgressDialog(ForgetPasswordActivity.this, R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Loading....");
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        init();

    }

    private void init() {

        edittextEmail = (EditText) findViewById(R.id.edittextEmail);
        edittextphone = (EditText) findViewById(R.id.edittextphone);
        textinfo= (TextView) findViewById(R.id.textinfo);
        textinfo.setText(Html.fromHtml(String.valueOf(getText(R.string.forgetpassworddesc))));
        btnsubmit= (Button) findViewById(R.id.btnsubmit);
        btncancel= (Button) findViewById(R.id.btncancel);
        btnsubmit.setOnClickListener(this);
        btncancel.setOnClickListener(this);
        profileRespnseDtoCallback = new Callback<ProfileResponseDto>() {
            @Override
            public void onResponse(Call<ProfileResponseDto> call, retrofit2.Response<ProfileResponseDto> response) {
                //AppLog.d(TAG, response.body().toString());
                // pDialog.dismiss();
                if (response.isSuccessful()) {

                    ProfileResponseDto ProfileResponse = response.body();
                    if (ProfileResponse.getStatus() == 0) {
                        progressDialog.dismiss();
                      showdailog("Please check your email");
                    } else if (ProfileResponse.getStatus() == 1) {
                        progressDialog.dismiss();
                        AppUtils.errordialog(ForgetPasswordActivity.this, ProfileResponse.getMessage());
                    }

                } else {
                    progressDialog.dismiss();
                    //  AppUtils.errordialog(ProfileActivity.this, AppConstant.Server_Error);

                }
            }

            @Override
            public void onFailure(Call<ProfileResponseDto> call, Throwable t) {
                progressDialog.dismiss();
                AppUtils.errordialog(ForgetPasswordActivity.this, t.getMessage());
                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        };

    }


    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }

    @Override
    protected int getActivityID() {
        return 0;
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.forget_password;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.forgetpassword_layout;
    }

    @Override
    protected int getDrawerLayout() {
        return 0;
    }

    @Override
    protected int getNavigationView() {
        return 0;
    }

    @Override
    protected Activity getInstance() {
        return null;
    }

    @Override
    public void onClick(View v) {
        if(v==btnsubmit){
            if(isvalid()) {
                Networkcontroller.checkInternetConnection(ForgetPasswordActivity.this,this,"forgetpassword");
            }
        }
        if(v==btncancel){
            finish();
        }
    }


    private void getstring(){
        emailstring= AppUtils.gettextstring(edittextEmail);
        phonestring= AppUtils.gettextstring(edittextphone);

    }


    private  Boolean isvalid(){
        getstring();

        if (!AppUtils.isEmailValid(emailstring)) {
            edittextEmail.setError(AppConstant.email_error);
            return false;
        }
        if (phonestring.length() != 10) {
            edittextphone.setError(AppConstant.phone_error);
            return false;
        }

        return true;

    }

    @Override
    public void network(Boolean aBoolean,String s) {
        if(aBoolean){
            progressDialog.show();
            ApiManager.Userforgetpassword(profileRespnseDtoCallback, emailstring, phonestring);
        }
        else{
            AppUtils.errordialog(ForgetPasswordActivity.this, AppConstant.no_network);
        }

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public   void showdailog(String message){
        new android.support.v7.app.AlertDialog.Builder(ForgetPasswordActivity.this,R.style.AppCompatAlertDialogStyle)
                .setTitle("Alert")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                   finish();
                        dialog.dismiss();


                    }
                })

                .show();
    }


}
