package com.pathway.geokrishi;

import android.app.Activity;
import android.app.Dialog;

import android.content.Intent;
import android.content.pm.PackageManager;


import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.pathway.geokrishi.ApiController.ApiManager;
import com.pathway.geokrishi.ApiController.ResponseDTO.ProfileResponseDto;
import com.pathway.geokrishi.adapter.CropAdpter;

import com.pathway.geokrishi.utils.AppConstant;
import com.pathway.geokrishi.utils.AppUtils;
import com.pathway.geokrishi.utils.HorizontalListView;
import com.pathway.geokrishi.utils.PlaceJSONParser;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GeoLocationActivity extends BaseActivity implements OnMapReadyCallback, View.OnClickListener, AdapterView.OnItemClickListener {
    SupportMapFragment mapFragment;
    private GoogleMap mMap;
    double lat,lng;


     public static double latitude, longitude;
    String currentlocationaddress, location, addresspickup;
    Button btnconfirmlocation;
    HorizontalListView horizontallistview;
    CropAdpter cropadpter;
    private AutoCompleteTextView geo_autocomplete;
    PlacesTask placesTask;
    ParserTask parserTask;
    private Integer THRESHOLD = 1;
    ImageButton imagebuttonsearch, imagebuttonClose;
    TextView textviewMonth;
    LatLng latLng;
    CheckBox checkbodlocation;
     PolygonOptions rectOptions2;
    Polygon polygon;
    private Callback<ProfileResponseDto> profileResponseDtoCallback;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        hideSoftKeyboard();
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        btnconfirmlocation = (Button) findViewById(R.id.btnconfirmlocation);
        horizontallistview = (HorizontalListView) findViewById(R.id.horizontallistview);
        btnconfirmlocation.setOnClickListener(this);
        imagebuttonsearch = (ImageButton) findViewById(R.id.imagebuttonsearch);
        imagebuttonClose = (ImageButton) findViewById(R.id.imagebuttonClose);
        checkbodlocation=(CheckBox)findViewById(R.id.checkbodlocation);
        geo_autocomplete = (AutoCompleteTextView) findViewById(R.id.googleautocomplete);
        geo_autocomplete.setThreshold(THRESHOLD);
        textviewMonth=(TextView)findViewById(R.id.textviewMonth) ;
        textviewMonth.setText(MainActivity.monthString);
        latitude=AppUtils.latitude(getApplicationContext());
        longitude=AppUtils.longitude(getApplicationContext());
        System.out.println("latlng=="+longitude+"  "+latitude);
        profileResponseDtoCallback = new Callback<ProfileResponseDto>() {
            @Override
            public void onResponse(Call<ProfileResponseDto> call, Response<ProfileResponseDto> response) {
                if (response.isSuccessful()) {

                    ProfileResponseDto locationResponseDto = response.body();
                    if (locationResponseDto.getStatus() == 0) {
                        Intent intent = new Intent(GeoLocationActivity.this,VegitableCategoryActivity.class);
                        intent.putExtra("latitude",latitude+"");
                        intent.putExtra("longitude",longitude+"");
                        AppUtils.locationshareprefrence(getApplicationContext(),location,latitude+"",longitude+"");
                        startActivity(intent);

                    } else if (locationResponseDto.getStatus() == 1) {
                        // dialog.dismiss();
                        AppUtils.errordialog(GeoLocationActivity.this, locationResponseDto.getMessage());

                    }

                } else {
                    // dialog.dismiss();
                    AppUtils.errordialog(GeoLocationActivity.this, AppConstant.Server_Error);
                }

            }

            @Override
            public void onFailure(Call<ProfileResponseDto> call, Throwable t) {

            }
        };

//
//        geo_autocomplete.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
//                }
//            }
//        });

        geo_autocomplete.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                geo_autocomplete.setError(null);
                placesTask = new PlacesTask();
                placesTask.execute(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });
        geo_autocomplete.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        imagebuttonsearch.setOnClickListener(this);
        imagebuttonClose.setOnClickListener(this);
        mapFragment.getMapAsync(this);
        horizontaladapter();
    }


    public void geomap(final GoogleMap mMap) {
        Integer resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(GeoLocationActivity.this);
        if (resultCode == ConnectionResult.SUCCESS) {
            rectOptions2= new PolygonOptions();
            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

                @Override
                public void onMapClick(LatLng latLng)




                {
                    PolylineOptions rectOptions = new PolylineOptions()
                            .add(new LatLng(latitude, longitude));
                     Polyline polyline = mMap.addPolyline(rectOptions);
                    // Creating a marker
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latLng);
                    Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                    try {
                       final  List<Address> listAddresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                        if (null != listAddresses && listAddresses.size() > 0) {
                            markerOptions.title(location);
                            location = listAddresses.get(0).getAddressLine(0);
                        latitude=latLng.latitude;
                            longitude=latLng.longitude;
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                 mMap.clear();
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                    mMap.addMarker(markerOptions);


                     lat=latitude;
                             lng=longitude;
                     System.out.println("lat====="+lat);
                     System.out.println("lng====="+lng);



// Get back the mutable Polyline

//                     Polygon polygon = mMap.addPolygon(new PolygonOptions()
//                             .add(new LatLng(lat, lng))
//                             .strokeColor(Color.RED)
//                             .fillColor(Color.BLUE));


//
//                    rectOptions2.add(new latLng(lat, lng),
//                            new latLng(lat,lng));
//
//// Get back the mutable Polygon
//
//                    polygon= mMap.addPolygon(rectOptions2);
//                    polygon.setStrokeColor(Color.RED);
//                    polygon.setFillColor(Color.BLUE);

                }
            });
        } else {
            Dialog dialog1 = GooglePlayServicesUtil.getErrorDialog(resultCode, GeoLocationActivity.this, 0);
            if (dialog1 != null) {

                //locationSearch.setVisibility(View.GONE);
                dialog1.show();
            }
      }
        geo_autocomplete.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Geocoder.isPresent();
                    if(Geocoder.isPresent()){

                    }
                    else{
                        System.out.println("no address");
                    }
                    String locations = addresspickup;
                    List<Address> addressList = null;
                    try {
                        if (locations != null || !locations.equals("")) {
                            Geocoder geocoder = new Geocoder(GeoLocationActivity.this);
                            try {
                                addressList = geocoder.getFromLocationName(locations, 1);
                                try {
                                    Address address = addressList.get(0);
                                    location = addressList.get(0).getAddressLine(0);
                                    latLng = new LatLng(address.getLatitude(), address.getLongitude());
                                    mMap.clear();
                                    mMap.addMarker(new MarkerOptions().position(latLng));
                                    latitude=address.getLatitude();
                                    longitude=address.getLongitude();
                                    mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));

                                } catch (Exception ex) {
                                    System.out.println("error=="+ex.getMessage());

                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (Exception ex) {
                        Toast.makeText(getApplicationContext(), "Please select the pick up loaction",
                                Toast.LENGTH_LONG).show();
                    }
                }
                handled = true;

                return handled;
            }
        });



    }

    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }

    @Override
    protected int getActivityID() {
        return 0;
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.your_selection;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.location_layout;
    }

    @Override
    protected int getDrawerLayout() {
        return 0;
    }

    @Override
    protected int getNavigationView() {
        return 0;
    }

    @Override
    protected Activity getInstance() {
        return null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onMapReady(GoogleMap mMap) {

        if (ContextCompat.checkSelfPermission(GeoLocationActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(GeoLocationActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(GeoLocationActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
return;
        }
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setMyLocationEnabled(true);
        mMap.setTrafficEnabled(true);
        mMap.setIndoorEnabled(true);
        mMap.setBuildingsEnabled(true);
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(true);

        LatLng lat_long = new LatLng(latitude, longitude);

        mMap.setMyLocationEnabled(true);
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());
        try {

            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            currentlocationaddress = addresses.get(0).getAddressLine(0);
        } catch (Exception ex) {

        }
        mMap.addMarker(new MarkerOptions().position(lat_long).title(currentlocationaddress));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(lat_long));
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 30));

        CameraUpdate center =
                CameraUpdateFactory.newLatLng(new LatLng(latitude,
                        longitude));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(13);

        mMap.moveCamera(center);
        mMap.animateCamera(zoom);
        geomap(mMap);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnconfirmlocation:

                if (AppUtils.checkInternetConnection(this)) {
                    if(checkbodlocation.isChecked()){

                        System.out.println("checked===checked");
                        ApiManager.Userlatlong(profileResponseDtoCallback,AppUtils.userId(GeoLocationActivity.this),
                                String.valueOf(longitude),String.valueOf(latitude));
                       // AppUtils.latlng(getApplicationContext(),String.valueOf(longitude),String.valueOf(latitude));
                    }
                    else{
                        Intent intent = new Intent(GeoLocationActivity.this,VegitableCategoryActivity.class);
                        intent.putExtra("latitude",latitude+"");
                        intent.putExtra("longitude",longitude+"");
                       // AppUtils.latlng(getApplicationContext(),String.valueOf(longitude),String.valueOf(latitude));
                        startActivity(intent);
                    }

                } else {

                }

                break;

            case R.id.imagebuttonClose:
                imagebuttonsearch.setVisibility(View.VISIBLE);
                imagebuttonClose.setVisibility(View.GONE);
                geo_autocomplete.setVisibility(View.GONE);
                setSupportActionBar(toolbar);
                getSupportActionBar().setDisplayUseLogoEnabled(true);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                toolbar.setTitle("Your Location");
                break;

            case R.id.imagebuttonsearch:
                geo_autocomplete.setText("");
                setSupportActionBar(toolbar);
                getSupportActionBar().setDisplayUseLogoEnabled(false);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                toolbar.setTitle("");
                imagebuttonsearch.setVisibility(View.GONE);
                imagebuttonClose.setVisibility(View.VISIBLE);
                geo_autocomplete.setVisibility(View.VISIBLE);
                break;
        }

    }

    public void horizontaladapter() {
        cropadpter = new CropAdpter(getApplicationContext(),MainActivity.selections1);
        horizontallistview.setAdapter(cropadpter);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        HashMap<String, Object> obj = (HashMap<String, Object>) parent.getItemAtPosition(position);
        addresspickup = (String) obj.get("description");

        //objLatLont = getLocationFromAddress(Geolocation.this, addresspickup);
    }

    private class PlacesTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... place) {
            // For storing data from web service
            String data = "";

            // Obtain browser key from https://code.google.com/apis/console
            String key = "AIzaSyAHyyMWhWxTfScriihzfiYmhaLnH7KOK-M";

            String input = "";

            try {
                input = "input=" + URLEncoder.encode(place[0], "utf-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }

            // place type to be searched

            String types = "types=geocode";

            // Sensor enabled
            String sensor = "sensor=false";

            // Building the parameters to the web service
            String parameters = input + "&" + types + "&components=country:np&" + sensor + "&" + "key=" + key;

            // Output format
            String output = "json";

            // Building the url to the web service


            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/" + output + "?" + parameters;

            System.out.println("url==="+url);

            try {
                // Fetching the data from web service in background
                data = downloadUrl(url);
            } catch (Exception e) {
                //  Log.d("Background Task",e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // Creating ParserTask
            parserTask = new ParserTask();

            // Starting Parsing the JSON string returned by Web Service
            parserTask.execute(result);
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {


            // System.out.println("exception while downloading url==" + e.toString());

            //  Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String, String>>> {

        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;

            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try {
                jObject = new JSONObject(jsonData[0]);

                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);

            } catch (Exception e) {
                //  Log.d("Exception",e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {

            String[] from = new String[]{"description"};
            int[] to = new int[]{android.R.id.text1};

            // Creating a SimpleAdapter for the AutoCompleteTextView
            SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), result, android.R.layout.simple_list_item_1, from, to);

            // Setting the adapter
            geo_autocomplete.setAdapter(adapter);
            geo_autocomplete.setOnItemClickListener(GeoLocationActivity.this);

        }
    }


//    public  void hideSoftKeyboard() {
//        (InputMethodManager) this.getSystemService(
//                GeoLocationActivity.this.INPUT_METHOD_SERVICE);
//        inputMethodManager.hideSoftInputFromWindow(
//                this.getCurrentFocus().getWindowToken(), 0);
//    }        InputMethodManager inputMethodManager =
//

}
