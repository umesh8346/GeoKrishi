package com.pathway.geokrishi.utils;

import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.preference.PreferenceManager;

import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.EditText;
import android.widget.ProgressBar;
import com.pathway.geokrishi.Interface.ProgressbarInterface;
import com.pathway.geokrishi.ProfileActivity;
import com.pathway.geokrishi.R;
import com.pathway.geokrishi.SettingActivity;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class AppUtils {

    public static String gettextstring(EditText ed) {
        String getstring = null;
        getstring = ed.getText().toString();
        return getstring;

    }

    public static boolean isvalid(String st){
        if(st.equals("")){
            return false;
        }
        else{
            return true;
        }

    }


    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static boolean checkInternetConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        try {
            NetworkInfo[] netInfo = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo) {
                if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                    if (ni.isConnected())
                        haveConnectedWifi = true;
                if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                    if (ni.isConnected())
                        haveConnectedMobile = true;
            }
        } catch (Exception e) {
            e.getStackTrace();
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

public static void loginshareprefrence(Context mcontecxt,String UserId,String Firstname,String Lastname,String Username,String Email,String MobileNumber,
                                String Organization,String Profession,String Longitude,String Latitude,String AvaterImage){
    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mcontecxt);
    SharedPreferences.Editor editor = sharedPref.edit();
    editor.putString(AppConstant.UserId, UserId);
    editor.putString(AppConstant.Firstname, Firstname);
    editor.putString(AppConstant.Lastname, Lastname);
    editor.putString(AppConstant.Username, Username);
    editor.putString(AppConstant.Email, Email);
    editor.putString(AppConstant.Mobile, MobileNumber);
    editor.putString(AppConstant.Organization, Organization);
    editor.putString(AppConstant.Professional, Profession);
    editor.putString(AppConstant.Avater, AvaterImage);
    editor.putString(AppConstant.Longitude, Longitude);
    editor.putString(AppConstant.Latitude, Latitude);
    editor.commit();

}


    public static void loginshareprefrence(Context mcontecxt) {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mcontecxt);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(AppConstant.UserId, "");
        editor.putString(AppConstant.Firstname, "");
        editor.putString(AppConstant.Lastname, "");
        editor.putString(AppConstant.Username, "");
        editor.putString(AppConstant.Email, "");
        editor.putString(AppConstant.Mobile, "");
        editor.putString(AppConstant.Organization, "");
        editor.putString(AppConstant.Professional, "");
        editor.putString(AppConstant.Avater, "");
        editor.putString(AppConstant.Longitude, "");
        editor.putString(AppConstant.Latitude, "");
        editor.commit();

    }


    public static void locationshareprefrence(Context mcontecxt,String locationame,String latitude,String longitude) {

        System.out.println("locationame=="+locationame+" "+latitude+"  "+longitude);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mcontecxt);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(AppConstant.Longitude, longitude);
        editor.putString(AppConstant.Latitude, latitude);

        editor.putString("LocaitonName", locationame);
        editor.commit();
    }

    public static String userId(Context mcontext){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mcontext);
        String token = sharedPref.getString("UserId","");
        return token;
     //   return "4";
    }

    public static double longitude(Context mcontext){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mcontext);
        String lng = sharedPref.getString(AppConstant.Longitude,"");
        double longitude = Double.parseDouble(lng);
        return longitude;
        //return 83.57;
    }

    public static String  locatiosettingname(Context mcontext){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mcontext);
        String location = sharedPref.getString("LocaitonName","");

        return location;
        //return 83.57;
    }

    public static double latitude(Context mcontext){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mcontext);
        String lng = sharedPref.getString(AppConstant.Latitude,"");
        double latitude = Double.parseDouble(lng);
        return latitude;
     // return 27.3;
    }

    public static void errordialog(Context mcontext,String message){

        new AlertDialog.Builder(mcontext, R.style.AppCompatAlertDialogStyle)
                .setTitle("Alert")
                .setMessage(message)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dialog.dismiss();
                    }
                })

                .show();
    }

    public static void transparentStatusBar(Window window){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
    }
    public static  int geinttvalue(String value){


        int i = 0;
        if (value.equals("")|| value==null) {
            i = 0;
        } else {
            i = Integer.parseInt(value);
        }
        return i;
    }
 public static String language(Context mcontecxt){
     SharedPreferences sh_PrefN = mcontecxt.getSharedPreferences(
             "Language Credential", Context.MODE_PRIVATE);
    String  language = sh_PrefN.getString("LANGUAGE", "");
     return language;
 }


    public static String apilanguage(Context mcontecxt){
        SharedPreferences sh_PrefN = mcontecxt.getSharedPreferences(
                "Language Credential", Context.MODE_PRIVATE);
        String  language = sh_PrefN.getString("LANGUAGE", "");

        if(language.equals("np")){
            return "nep";
        }
        else {
            return "eng";
        }

    }

    public  static void showdailog(String message, final Context mcontext){
        new android.support.v7.app.AlertDialog.Builder(mcontext,R.style.AppCompatAlertDialogStyle)
                .setTitle("Alert")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        mcontext.startActivity(new Intent(mcontext,
                                ProfileActivity.class));
                        dialog.dismiss();


                    }
                })

                .show();
    }

    public  static void   simulateSuccessProgress( final CircularProgressButton button) {
        ValueAnimator widthAnimation = ValueAnimator.ofInt(1, 100);
        widthAnimation.setDuration(1500);
        widthAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                button.setProgress(value);
            }
        });
        widthAnimation.start();
    }


    public static void simulateErrorProgress(final CircularProgressButton button) {
        ValueAnimator widthAnimation = ValueAnimator.ofInt(1, 99);
        widthAnimation.setDuration(1500);
        widthAnimation.setInterpolator(new AccelerateDecelerateInterpolator());
        widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                button.setProgress(value);
                if (value == 99) {
                    button.setProgress(-1);
                }
            }
        });
        widthAnimation.start();
    }


    public static void  ActivitySetting(Context context,String activity){
        SharedPreferences sh_Pref_S1 = context
                .getSharedPreferences("Activity Credential",
                        Context.MODE_PRIVATE);
        SharedPreferences.Editor toEdit1 = sh_Pref_S1.edit();
        toEdit1.putString("Activity", activity);
        toEdit1.commit();
    }

    public static String activitystring(Context mcontecxt){
        SharedPreferences sh_PrefN = mcontecxt.getSharedPreferences(
                "Activity Credential", Context.MODE_PRIVATE);
        String  activityname = sh_PrefN.getString("Activity", "");
        return activityname;
    }

    public static RequestBody toRequestBody (String value) {
        RequestBody body = RequestBody.create(MediaType.parse("text/plain"), value);
        return body ;
    }


    public static void showprogressbar(Context context , ProgressBar progressbar, ProgressbarInterface viewinterface){

        viewinterface.showprogressbar(progressbar);

    }
    public static void hideprogressbar(Context context , ProgressBar progressbar, ProgressbarInterface viewinterface){

        viewinterface.hideprogressbar(progressbar);

    }
    public static String getday(long day){
        String weeksays=null;
        long time=Long.valueOf(day*1000);

        Calendar cl = Calendar.getInstance();
        cl.setTimeInMillis(time);  //here your time in miliseconds
        String wetherdate = "" + cl.get(Calendar.DAY_OF_MONTH);

        SimpleDateFormat inFormat = new SimpleDateFormat("dd");
        try {
            Date date = inFormat.parse(wetherdate);
            SimpleDateFormat outFormat = new SimpleDateFormat("EEE");
            weeksays = outFormat.format(date);
        }
        catch(Exception ex){

        }

        return weeksays;
    }


    public static  String getweatherday(long day){
        long time=Long.valueOf(day*1000);
        Date date = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat("EEE", Locale.getDefault());
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        String formattedDate = sdf.format(date);
        return formattedDate;
    }



    public static String gethr(int day){
        String weeksays=null;
        long time=Long.valueOf(day*1000);

        long hr=TimeUnit.MILLISECONDS.toHours(time);
           long min=TimeUnit.MILLISECONDS.toMinutes(time);

//                        Calendar cl = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
//        cl.setTimeInMillis(time);  //here your time in miliseconds
        java.util.Date d = new java.util.Date(
                Long.valueOf(time) * 1000);
        SimpleDateFormat sdf = new SimpleDateFormat(
                "yyyy-MM-dd");
        String noticedate = sdf.format(d);

        Calendar c = Calendar.getInstance();
        try {
            sdf = new SimpleDateFormat("hh:mm");
            c.setTime(sdf.parse(noticedate));
            Date resultdate = new Date(c.getTimeInMillis());
            weeksays  = sdf.format(resultdate);
        }catch (Exception ex){

        }


        return weeksays;
    }


    public static String gethr(String s){

        String time = s.substring(11,16);
        return time;

    }
 public static String androidid(){
     return Settings.Secure.ANDROID_ID;
 }


  public static String gettemp(double d){

      double temp=d-272;
     String temperature=new DecimalFormat("##.##").format(temp);
      return temperature;

  }


    public static void latlng(Context mcontecxt,String Longitude,String Latitude){
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(mcontecxt);
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putString(AppConstant.Longitude, Longitude);
        editor.putString(AppConstant.Latitude, Latitude);
        editor.commit();

    }



    public static void locationsharperpresent(Context context,String location, String locationid){
        SharedPreferences sh_Pref_S1 = context
                .getSharedPreferences("location Credential",
                        Context.MODE_PRIVATE);
        SharedPreferences.Editor toEdit1 = sh_Pref_S1.edit();
        toEdit1.putString("locationid", locationid);
        toEdit1.putString("location", location);
        toEdit1.commit();
    }


    public static String locationame(Context mcontecxt){
        SharedPreferences sh_PrefN = mcontecxt.getSharedPreferences(
                "location Credential", Context.MODE_PRIVATE);
        String  locationame = sh_PrefN.getString("location", "");
        return locationame;
    }

    public static String locationid(Context mcontecxt){
        SharedPreferences sh_PrefN = mcontecxt.getSharedPreferences(
                "location Credential", Context.MODE_PRIVATE);
        String  locationid = sh_PrefN.getString("locationid", "");
        return locationid;
    }

 public static String openweatherUrl(String locationid){
     String OpenWeatherDayUrl="http://api.openweathermap.org/data/2.5/forecast/daily?id="+locationid+"&lang=en&cnt=5&appid=6023985399897109c893543cfd4ce0bb";
return OpenWeatherDayUrl;
 }



}
