package com.pathway.geokrishi.utils;


import android.content.Context;

import com.pathway.geokrishi.ApiController.ApiConfig;

public class AppConstant {
    public static String required_field="Required Field";
    public static String confirm_password_error="Password Mismatch";

public static String localurl="http://192.168.13.37/GeoKrishi/GeoKrishi/API/SuitableCrops";
    public static String UserId="UserId";


    public static String Firstname="FirstName";
    public static String Lastname="LastName";
    public static String Email="Email";
    public static String Username="UserName";
    public static String Mobile="Mobile";
    public static String Organization="Organization";
    public static String Professional="Professional";
    public static String Avater="AvaterImage";
    public static String Latitude="Latitude";
    public static String Longitude="Longitude";
    public static String Register_message="Successfully Register  click ok to login";
    public static String UserRegistration="Please Wait...........";
    public static String Server_Error="Network Connection Error please try again !";
    public static String no_network="No network connection";

    public static String password_error="Password Must be 6 or more than 6 character";
    public static String email_error="Invalid email";
    public static String phone_error="Invalid Phone";
    //database names


    public static String DATABASE_NAME="db_GeoKrishi";
    public static String DISTRICT_INFO="tbl_district";
    public static String VDC_INFO="tbl_vdc";
    public static String ID="id";
    public static String DISTRICT_ID="DistrictID";
    public static String DISTRICT_NAME="DistrictName";
    public static String VDC_ID="VDCId";
    public static String VDC_NAME="VDCName";



    public static String CommunityName="CommunityName";
    public static String District="District";
    public static String VDC="VDC";
    public static String Ward="Ward";
    public static String Contact_Details="Contact_Details";
    public static String Contact_Name="Contact_Name";
    public static String Contact_Phone="Contact_Phone";
    public static String Contact_Mobile="Contact_Mobile";
    public static String email="email";
//    public static String Latitude="Latitude";
//    public static String Longitude="Longitude";
    public static String Association="Association";
    public static String Internet_Facility="Internet_Facility";
    public static String No_of_extension_worker="No_of_extension_worker";
    public static String Service_sector="Service_sector";
    public static String Service_area_No_Of_Wards="Service_area_No_Of_Wards";
    public static String No_of_farmers="No_of_farmers";
    public static String UserID="UserID";
    public static String Type="Type";
    public static String data="data";

public static String mobile_no="9851052800";
    public static String phone_no="014101544";




}
