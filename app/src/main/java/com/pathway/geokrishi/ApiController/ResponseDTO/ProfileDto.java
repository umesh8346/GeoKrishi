package com.pathway.geokrishi.ApiController.ResponseDTO;


import com.google.gson.annotations.SerializedName;

public class ProfileDto {
    @SerializedName("Status")
    public int Status;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public Data Data;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }
    public ProfileDto.Data getData() {
        return Data;
    }

    public void setData(ProfileDto.Data data) {
        Data = data;
    }

    public static class Data {
        public int getExtensionWorkerID() {
            return ExtensionWorkerID;
        }

        public void setExtensionWorkerID(int extensionWorkerID) {
            ExtensionWorkerID = extensionWorkerID;
        }

        public int getUserID() {
            return UserID;
        }

        public void setUserID(int userID) {
            UserID = userID;
        }

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getContactDetails() {
            return ContactDetails;
        }

        public void setContactDetails(String contactDetails) {
            ContactDetails = contactDetails;
        }

        public String getQualification() {
            return Qualification;
        }

        public void setQualification(String qualification) {
            Qualification = qualification;
        }

        public String getExpertise() {
            return Expertise;
        }

        public void setExpertise(String expertise) {
            Expertise = expertise;
        }

        public String getMobileNumber() {
            return MobileNumber;
        }

        public void setMobileNumber(String mobileNumber) {
            MobileNumber = mobileNumber;
        }

        public int getPhoneType() {
            return PhoneType;
        }

        public void setPhoneType(int phoneType) {
            PhoneType = phoneType;
        }

        public int getInternet() {
            return Internet;
        }

        public void setInternet(int internet) {
            Internet = internet;
        }

        @SerializedName("AvatarImage")

        private String avatarImage;

        public String getAvatarImage() {
            return avatarImage;
        }

        public void setAvatarImage(String avatarImage) {
            this.avatarImage = avatarImage;
        }

        @SerializedName("ExtensionWorkerID")
        public int ExtensionWorkerID;
        @SerializedName("UserID")
        public int UserID;
        @SerializedName("Name")
        public String Name;
        @SerializedName("ContactDetails")
        public String ContactDetails;
        @SerializedName("Qualification")
        public String Qualification;
        @SerializedName("Expertise")
        public String Expertise;
        @SerializedName("MobileNumber")
        public String MobileNumber;
        @SerializedName("PhoneType")
        public int PhoneType;
        @SerializedName("Internet")
        public int Internet;

        @SerializedName("LandUnit")

        private String landUnit;

        public String getLandUnit() {
            return landUnit;
        }

        public void setLandUnit(String landUnit) {
            this.landUnit = landUnit;
        }

        public int getNoOfFarmers() {
            return NoOfFarmers;
        }

        public void setNoOfFarmers(int noOfFarmers) {
            NoOfFarmers = noOfFarmers;
        }

        @SerializedName("NoOfFarmers")
        public int NoOfFarmers;

        //extension

        @SerializedName("FarmerID")

        private Integer farmerID;

        @SerializedName("FarmerName")

        private String farmerName;
        @SerializedName("Age")

        private String age;
        @SerializedName("Gender")

        private String gender;
        @SerializedName("No_Of_Family_Members")

        private Integer noOfFamilyMembers;

        @SerializedName("Latitude")

        private double latitude;
        @SerializedName("Longitude")

        private double longitude;
        @SerializedName("Farmer_Representative_Name")

        private String farmerRepresentativeName;
        @SerializedName("Farmer_Representative_Relation")

        private String farmerRepresentativeRelation;
        @SerializedName("Mobile")

        private String mobile;
        @SerializedName("LandHoldingUnitArea")

        private Object landHoldingUnitArea;

        public Integer getFarmerID() {
            return farmerID;
        }

        public void setFarmerID(Integer farmerID) {
            this.farmerID = farmerID;
        }

        public String getFarmerName() {
            return farmerName;
        }

        public void setFarmerName(String farmerName) {
            this.farmerName = farmerName;
        }

        public String getAge() {
            return age;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public Integer getNoOfFamilyMembers() {
            return noOfFamilyMembers;
        }

        public void setNoOfFamilyMembers(Integer noOfFamilyMembers) {
            this.noOfFamilyMembers = noOfFamilyMembers;
        }


        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public String getFarmerRepresentativeName() {
            return farmerRepresentativeName;
        }

        public void setFarmerRepresentativeName(String farmerRepresentativeName) {
            this.farmerRepresentativeName = farmerRepresentativeName;
        }

        public String getFarmerRepresentativeRelation() {
            return farmerRepresentativeRelation;
        }

        public void setFarmerRepresentativeRelation(String farmerRepresentativeRelation) {
            this.farmerRepresentativeRelation = farmerRepresentativeRelation;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public Object getLandHoldingUnitArea() {
            return landHoldingUnitArea;
        }

        public void setLandHoldingUnitArea(Object landHoldingUnitArea) {
            this.landHoldingUnitArea = landHoldingUnitArea;
        }

//comunity



        @SerializedName("CommunityOrganizationID")

        private Integer communityOrganizationID;

        @SerializedName("CommunityName")

        private String communityName;
        @SerializedName("District")

        private String district;
        @SerializedName("VDC")

        private String vDC;
        @SerializedName("Ward")

        private Integer ward;

        @SerializedName("Contact_Name")

        private String contactName;
        @SerializedName("Contact_Phone")

        private String contactPhone;
        @SerializedName("Contact_Mobile")

        private String contactMobile;
        @SerializedName("email")

        private String email;
        @SerializedName("Contact_Details")

        private String contact_Details;

        public String getContact_Details() {
            return contact_Details;
        }

        public void setContact_Details(String contact_Details) {
            this.contact_Details = contact_Details;
        }

        @SerializedName("Association")

        private String association;
        @SerializedName("Internet_Facility")

        private Integer internetFacility;
        @SerializedName("No_of_extension_worker")

        private Integer noOfExtensionWorker;
        @SerializedName("Service_sector")

        private String serviceSector;
        @SerializedName("Service_area_No_Of_Wards")

        private Integer serviceAreaNoOfWards;
        @SerializedName("No_of_farmers")

        private Integer no_Of_Farmers;

        public Integer getNo_Of_Farmers() {
            return no_Of_Farmers;
        }

        public void setNo_Of_Farmers(Integer no_Of_Farmers) {
            this.no_Of_Farmers = no_Of_Farmers;
        }

        public Integer getCommunityOrganizationID() {
            return communityOrganizationID;
        }

        public void setCommunityOrganizationID(Integer communityOrganizationID) {
            this.communityOrganizationID = communityOrganizationID;
        }



        public String getCommunityName() {
            return communityName;
        }

        public void setCommunityName(String communityName) {
            this.communityName = communityName;
        }

        public String getDistrict() {
            return district;
        }

        public void setDistrict(String district) {
            this.district = district;
        }

        public String getVDC() {
            return vDC;
        }

        public void setVDC(String vDC) {
            this.vDC = vDC;
        }

        public Integer getWard() {
            return ward;
        }

        public void setWard(Integer ward) {
            this.ward = ward;
        }



        public String getContactName() {
            return contactName;
        }

        public void setContactName(String contactName) {
            this.contactName = contactName;
        }

        public String getContactPhone() {
            return contactPhone;
        }

        public void setContactPhone(String contactPhone) {
            this.contactPhone = contactPhone;
        }

        public String getContactMobile() {
            return contactMobile;
        }

        public void setContactMobile(String contactMobile) {
            this.contactMobile = contactMobile;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }



        public String getAssociation() {
            return association;
        }

        public void setAssociation(String association) {
            this.association = association;
        }

        public Integer getInternetFacility() {
            return internetFacility;
        }

        public void setInternetFacility(Integer internetFacility) {
            this.internetFacility = internetFacility;
        }

        public Integer getNoOfExtensionWorker() {
            return noOfExtensionWorker;
        }

        public void setNoOfExtensionWorker(Integer noOfExtensionWorker) {
            this.noOfExtensionWorker = noOfExtensionWorker;
        }

        public String getServiceSector() {
            return serviceSector;
        }

        public void setServiceSector(String serviceSector) {
            this.serviceSector = serviceSector;
        }

        public Integer getServiceAreaNoOfWards() {
            return serviceAreaNoOfWards;
        }

        public void setServiceAreaNoOfWards(Integer serviceAreaNoOfWards) {
            this.serviceAreaNoOfWards = serviceAreaNoOfWards;
        }


        @SerializedName("NumberOfComments")
        private Integer numberOfComments;
        @SerializedName("JoinedDate")
        private String joinedDate;

        public Integer getNumberOfComments() {
            return numberOfComments;
        }

        public void setNumberOfComments(Integer numberOfComments) {
            this.numberOfComments = numberOfComments;
        }

        public String getJoinedDate() {
            return joinedDate;
        }

        public void setJoinedDate(String joinedDate) {
            this.joinedDate = joinedDate;
        }
    }
}
