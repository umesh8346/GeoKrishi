package com.pathway.geokrishi;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.pathway.geokrishi.ApiController.ApiManager;
import com.pathway.geokrishi.ApiController.ResponseDTO.ProfileResponseDto;
import com.pathway.geokrishi.dtos.Item;
import com.pathway.geokrishi.utils.AppConstant;
import com.pathway.geokrishi.utils.AppUtils;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class ExtensionProfileActivity  extends  BaseActivity implements View.OnClickListener{
TextView textviewUsername,textviewProfessional,textviewaddress,textviewnooffarmer,textviewinternet,
        textviewphonetype,textviewmobile,textviewexpertise,textviewqualification,textviewcontactdetail,textviewtotalpost,textviewjoindate;

    ImageButton imagebuttoncamera,imagebuttonedit;
    String picturepath;
    static final int REQUEST_TAKE_PHOTO = 1;
    private static int RESULT_LOAD_IMAGE = 2;
    private Callback<ProfileResponseDto> profileRespnseDtoCallback;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    public void init(){
        textviewUsername=(TextView)findViewById(R.id.textviewUsername);
        textviewProfessional=(TextView)findViewById(R.id.textviewProfessional);
        textviewaddress=(TextView)findViewById(R.id.textviewaddress);
        textviewtotalpost=(TextView)findViewById(R.id.textviewtotalpost);
        textviewnooffarmer=(TextView)findViewById(R.id.textviewnooffarmer);
        textviewinternet=(TextView)findViewById(R.id.textviewinternet);
        textviewphonetype=(TextView)findViewById(R.id.textviewphonetype);
        textviewmobile=(TextView)findViewById(R.id.textviewmobile);
        textviewexpertise=(TextView)findViewById(R.id.textviewexpertise);
        textviewqualification=(TextView)findViewById(R.id.textviewqualification);
        textviewcontactdetail=(TextView)findViewById(R.id.textviewcontactdetail);
        textviewUsername.setText(ProfileActivity.profiledata.getName());
        textviewcontactdetail.setText(ProfileActivity.profiledata.getContactDetails());
        textviewexpertise.setText(ProfileActivity.profiledata.getExpertise());
        textviewmobile.setText(ProfileActivity.profiledata.getMobileNumber());
        textviewqualification.setText(ProfileActivity.profiledata.getQualification());
        textviewjoindate=(TextView)findViewById(R.id.textviewjoindate);
        imagebuttoncamera=(ImageButton)findViewById(R.id.imagebuttoncamera) ;
                imagebuttonedit=(ImageButton)findViewById(R.id.imageButtonEdituser);
        imagebuttonedit.setOnClickListener(this);
        if(ProfileActivity.profiledata.getInternet()==1){
            textviewinternet.setText("Home");
        }
        else{
            textviewinternet.setText("Mobile Data");
        }
         if(ProfileActivity.profiledata.getPhoneType()==1){
             textviewphonetype.setText("Smart Phone");
         }
         else{
             textviewphonetype.setText("Ordinary Phone");
         }
        textviewjoindate.setText(ProfileActivity.profiledata.getJoinedDate());
        textviewnooffarmer.setText(String.valueOf(ProfileActivity.profiledata.getNoOfFarmers()));
        textviewtotalpost.setText(String.valueOf(ProfileActivity.profiledata.getNumberOfComments()));
        profileRespnseDtoCallback = new Callback<ProfileResponseDto>() {
            @Override
            public void onResponse(Call<ProfileResponseDto> call, retrofit2.Response<ProfileResponseDto> response) {
                //AppLog.d(TAG, response.body().toString());
                // pDialog.dismiss();
                if (response.isSuccessful()) {

                    ProfileResponseDto ProfileResponse = response.body();
                    if (ProfileResponse.getStatus() == 0) {

                        AppUtils.showdailog("User profile Edit successfully", ExtensionProfileActivity.this);
                    } else if (ProfileResponse.getStatus() == 1) {

                        AppUtils.errordialog(ExtensionProfileActivity.this, ProfileResponse.getMessage());
                    }

                } else {
                    AppUtils.errordialog(ExtensionProfileActivity.this, AppConstant.Server_Error);

                }
            }

            @Override
            public void onFailure(Call<ProfileResponseDto> call, Throwable t) {
                // pDialog.dismiss();
                AppUtils.errordialog(ExtensionProfileActivity.this, t.getMessage());
                // Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        };

    }

    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }

    @Override
    protected int getActivityID() {
        return 0;
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.extension_worker;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.extensionprofile_layout;
    }

    @Override
    protected int getDrawerLayout() {
        return 0;
    }

    @Override
    protected int getNavigationView() {
        return 0;
    }

    @Override
    protected Activity getInstance() {
        return null;
    }

    @Override
    public void onClick(View v) {
        if(v==imagebuttonedit){
            startActivity(new Intent(this, ExtensionWorkerActivity.class));
        }

        if(v==imagebuttoncamera){


            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            else {
                showAlertDialogs();
            }
        }

    }

    private void showAlertDialogs() {
        final Item[] items = {
                new Item("Camera", android.R.drawable.ic_menu_camera),

                new Item("Gallery", android.R.drawable.ic_menu_gallery),

        };

        ListAdapter adapter = new ArrayAdapter<Item>(getApplicationContext(),
                android.R.layout.select_dialog_item, android.R.id.text1, items) {
            public View getView(int position, View convertView, ViewGroup parent) {
                // Use super class to create the View
                View v = super.getView(position, convertView, parent);
                TextView tv = (TextView) v.findViewById(android.R.id.text1);

                // Put the image on the TextView
                tv.setCompoundDrawablesWithIntrinsicBounds(
                        items[position].icon, 0, 0, 0);

                // Add margin between image and text (support various screen
                // densities)
                int dp5 = (int) (5 * getResources().getDisplayMetrics().density + 0.5f);
                tv.setCompoundDrawablePadding(dp5);

                return v;
            }
        };
        new AlertDialog.Builder(ExtensionProfileActivity.this, R.style.AppCompatAlertDialogStyle).setTitle("  Choose Option")
                .setAdapter(adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        // TODO Auto-generated method stub
                        SelectItem(item);
                    }

                }).show();

    }
    public void SelectItem(int item) {
        switch (item) {
            case 0:
                takephoto();
                break;
            case 1:
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, RESULT_LOAD_IMAGE);
                break;

        }


    }

    private void takephoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK
                && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturepath = cursor.getString(columnIndex);
            cursor.close();
            uploadimageintoserver();
        }
        if (requestCode == REQUEST_TAKE_PHOTO
                && resultCode == Activity.RESULT_OK) {
            uploadimageintoserver();
        }
    }
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date(0));
        String imageFileName = "Jpeg_" + timeStamp + "_";
        String storageDir = Environment.getExternalStorageDirectory()
                + "/picupload";
        File dir = new File(storageDir);
        if (!dir.exists())
            dir.mkdir();

        File image = new File(storageDir + "/" + imageFileName + ".jpg");


        picturepath = image.getAbsolutePath();
        System.out.println("image path==="+picturepath);
        return image;
    }
    public void uploadimageintoserver() {
        File file = new File(picturepath);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part userimage =
                MultipartBody.Part.createFormData("File", file.getName(), requestFile);

        Map<String, RequestBody> map = new HashMap<>();
        map.put("UserID", AppUtils.toRequestBody("7"));

        ApiManager.UserProfileImageResponse(profileRespnseDtoCallback, map, userimage);

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this,
                MainActivity.class));
        finish();
    }


}
