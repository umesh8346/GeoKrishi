package com.pathway.geokrishi.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pathway.geokrishi.ApiController.ApiConfig;
import com.pathway.geokrishi.ApiController.ResponseDTO.CropCategoryResponseDto;
import com.pathway.geokrishi.R;
import com.pathway.geokrishi.fragment.DashBoardFragment;

import java.util.ArrayList;
import java.util.List;

public class GridViewAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private Activity activity;
    public List selectedPositions;
    ArrayList<CropCategoryResponseDto.Data> categoryList;
    CheckBox cb;
    int id;
    DashBoardFragment fragment;
    private ArrayList<String> selections = new ArrayList<String>();
    private ArrayList<String> selectionsInteger = new ArrayList<String>();

    public GridViewAdapter(ArrayList<CropCategoryResponseDto.Data> categoryList, Activity activity,DashBoardFragment fragment) {
        this.categoryList = categoryList;
        this.activity = activity;
        this.fragment = fragment;
        selectedPositions = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return categoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return categoryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(
                    R.layout.category_grid_item, null);
            holder.imageview = (ImageView) convertView.findViewById(R.id.iv_image);
            holder.checkbox = (CheckBox) convertView.findViewById(R.id.ch_text);
            holder.textView = (TextView) convertView.findViewById(R.id.tv_title);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.checkbox.setId(position);
        holder.imageview.setId(position);
        holder.textView.setText(categoryList.get(position).getCropCategory2());
        holder.checkbox.setChecked(categoryList.get(position).isCheckedStatus());
       // holder.textView.setText(categoryList.get(position).getTitle());
        holder.id = position;
        holder.checkbox.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (holder.checkbox.isChecked()) {

                    holder.checkbox.setChecked(true);
                    selections.add(categoryList.get(position).getCropCategory2());
                    selectionsInteger.add(categoryList.get(position).getCropCategoryID()+"");
                    fragment.sendData(selections,selectionsInteger);


                } else {
                    holder.checkbox.setChecked(false);
                    selections.remove(categoryList.get(position).getCropCategory2());
                    selectionsInteger.remove(categoryList.get(position).getCropCategoryID()+"");
                    fragment.sendData(selections,selectionsInteger);
                    DashBoardFragment.checkBox_all.setChecked(false);

                }

            }
        });
        holder.imageview.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub

            }
        });


        Glide.with(activity)
                .load(ApiConfig.ImageUrl+categoryList.get(position).getCropCategoryImage())
                .into(holder.imageview );

        DashBoardFragment.checkBox_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selections.clear();
                selectionsInteger.clear();
                if (DashBoardFragment.checkBox_all.isChecked()) {

                    DashBoardFragment.checkBox_all.setChecked(true);
                    for (int i = 0; i < categoryList.size(); i++) {
                        selections.add(categoryList.get(i).getCropCategory2());
                        selectionsInteger.add(categoryList.get(i).getCropCategoryID()+"");
                        categoryList.get(i).setCheckedStatus(true);

                    }
                    notifyDataSetChanged();
                    fragment.sendData(selections,selectionsInteger);


                } else {
                    //checkBox.setChecked(false);
                    for (int i = 0; i < categoryList.size(); i++) {
                        // favList.get(i).setSelected(isChecked);
                        selections.remove(categoryList.get(i).getCropCategory2());
                        selectionsInteger.remove(categoryList.get(i).getCropCategoryID()+"");
                        categoryList.get(i).setCheckedStatus(false);
                    }
                    fragment.sendData(selections,selectionsInteger);
                    notifyDataSetChanged();

                }
            }

        });

        return convertView;
    }

    class ViewHolder {
        ImageView imageview;
        CheckBox checkbox;
        TextView textView;
        int id;
    }
}
