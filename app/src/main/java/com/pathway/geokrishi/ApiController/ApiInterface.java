package com.pathway.geokrishi.ApiController;


import com.google.gson.JsonObject;
import com.pathway.geokrishi.ApiController.ResponseDTO.CommentResponseDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.CropCalenderDTO;
import com.pathway.geokrishi.ApiController.ResponseDTO.CropCategoryResponseDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.CropExcelDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.CropListDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.CropVaritiesDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.LoginResponseDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.ProfileDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.ProfileResponseDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.RegisterResponseDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.SuitableCropsDetail;
import com.pathway.geokrishi.ApiController.ResponseDTO.SuitableListResponseDto;
import com.pathway.geokrishi.Notification.DTO.NotificationDTO;
import com.pathway.geokrishi.Openweather.WeatherCurrentDto.WeatherCurrentDto;
import com.pathway.geokrishi.Openweather.WeatherDTO.WeatherDayDto;
import com.pathway.geokrishi.Openweather.WeatherHourDTO.WeatherHourDto;


import org.json.JSONArray;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ApiInterface{


    @FormUrlEncoded
    @POST(ApiConfig.CropCategory)
    Call<CropCategoryResponseDto> getCropCatergory(@Field("Language") String language);
    @FormUrlEncoded
    @POST(ApiConfig.CropListUrl)
    Call<CropListDto> getCropCatergory(@Field("Language") String language,@Field("CropCategoryID") String categoryid);

    @FormUrlEncoded
   // @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @POST(ApiConfig.FacebookRegistration)
    Call<RegisterResponseDto> FacebookRegistration(@Field("UserName") String username, @Field("FirstName") String firstname, @Field("LastName") String lastname, @Field("FacebookID") String fbid, @Field("FacebookToken") String fbtoken,@Field("Email") String email,@Field("AvatarImage") String userimage,
            @Field("DeviceID") String DeviceID,@Field("RegistrationToken") String AndroidID,@Field("Longitude") String Longitude,@Field("Latitude") String Latitude);


    @FormUrlEncoded
    // @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @POST(ApiConfig.UserRegesterationMobile)
    Call<RegisterResponseDto> UserRegistration(@Field("UserName") String username, @Field("Email") String email,
                                               @Field("Password") String Password,@Field("DeviceID") String DeviceID,@Field("RegistrationToken") String AndroidID,@Field("Longitude") String Longitude,@Field("Latitude") String Latitude);

    @FormUrlEncoded
    // @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @POST(ApiConfig.UserSignIn)
    Call<LoginResponseDto> UserSign(@Field("UserName") String username, @Field("Password") String password);


    @FormUrlEncoded
    // @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @POST(ApiConfig.SuitableCrops)
    Call<SuitableListResponseDto> getSuitableCrops(@Field("CropTypeID") String cropid, @Field("Latitude") String latitude, @Field("Longitude") String longitude, @Field("Month") String month,@Field("Language") String language);


    @FormUrlEncoded
    // @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @POST(ApiConfig.DetailSuitableCrops)
    Call<SuitableCropsDetail> getSuitableCropsDetail(@Field("CropVarietyCategoryID") String cropDetailId,@Field("Language") String language);


    @FormUrlEncoded
    // @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @POST(ApiConfig.Comment)
    Call<CommentResponseDto> postComment(@Field("UserID") String userid,@Field("CropVarietyCategoryID") String cropDetailId,@Field("Comment") String Comment,@Field("Rating") String Rating);



    @FormUrlEncoded
    // @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @POST(ApiConfig.Remark)
    Call<CommentResponseDto> postRemark(@Field("UserID") String userid,@Field("CropVarietyCategoryID") String cropDetailId,@Field("RemarkType") String Comment,@Field("UserRemark") String Rating);



    @FormUrlEncoded
    // @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @POST(ApiConfig.Userprofile)
    Call<ProfileDto> getUserProfile(@Field("UserID") String userid);


    @FormUrlEncoded
    // @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @POST(ApiConfig.ProfileUrl)
    Call<ProfileResponseDto> getUserProfileResponse(@Field("Type") String type, @Field("UserData") JsonObject json);


    @FormUrlEncoded
    // @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @POST(ApiConfig.ChangePAsswordUrl)
    Call<ProfileResponseDto> getChangepasswordResponse(@Field("OldPassword") String oldpassword, @Field("NewPaasword") String  newpassword,@Field("UserID") String userid);


    @Multipart
    @POST(ApiConfig.UserImage)
    Call<ProfileResponseDto> getuserimageresponse(@PartMap Map<String, RequestBody> partMap, @Part MultipartBody.Part file);




    @FormUrlEncoded
    // @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @POST(ApiConfig.ListOfCrops)
    Call<SuitableListResponseDto> getSuitableCropsTool(@Field("CropTypeID") String cropid, @Field("Language") String language);

//    @GET(ApiConfig.WeatherUrl)
//    Call<WeatherDTO> getWeather5day();
//
//
//    @GET(ApiConfig.WeatherHrly)
//    Call<ArrayList<WeatherHourDto>> getWeatherHour();



    @GET
    Call<WeatherDayDto> getdatweather(@Url String url);

    @GET
   Call<WeatherHourDto> getWeatherHour(@Url String url);

    @GET
    Call<WeatherCurrentDto> getCurrentWeather(@Url String url);

    @FormUrlEncoded

    @POST(ApiConfig.latlngUrl)
    Call<ProfileResponseDto> getuserlatlng(@Field("UserID") String userid, @Field("Latitude") String latitude, @Field("Longitude") String longitude);
    @FormUrlEncoded

    @POST(ApiConfig.SuitableCropsListUrl)
    Call<SuitableListResponseDto> getSutaibleCropList(@Field("CropID") String cropid, @Field("Latitude") String latitude, @Field("Longitude") String longitude,@Field("Language") String language);


    @FormUrlEncoded

    @POST(ApiConfig.CalenderListUrl)
    Call<CropCalenderDTO> getcalendar(@Field("coordinates") String latlng, @Field("type") String type, @Field("language") String language);

    @FormUrlEncoded
    @POST(ApiConfig.CropvaritiesUrl)
    Call<CropVaritiesDto> getCropvarties(@Field("CropID") String CropId, @Field("Language") String language);



    @FormUrlEncoded
    @POST(ApiConfig.NotificationUrl)
    Call<NotificationDTO> getnotification(@Field("UserID") String UserID);

    @FormUrlEncoded
    @POST(ApiConfig.NotificationDetailUrl)
    Call<NotificationDTO> getnotificationDetail(@Field("UserID") String UserID,@Field("NotificationID") String NotificationID);


    @FormUrlEncoded
    // @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @POST(ApiConfig.UserInputUrl)
    Call<ProfileResponseDto> getcropFeedback(@Field("Latitude") String latitude, @Field("Longitude") String longitude, @Field("UserID") String UserID,@Field("UserInput") String UserInputString);

    //crop excel data


    @FormUrlEncoded
    // @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @POST(ApiConfig.CropExcelUrl)
    Call<CropExcelDto> getCropExcel(@Field("coordinates") String  latlng,@Field("type") String type, @Field("language") String language);


    @FormUrlEncoded
    // @Headers({"Content-Type: application/x-www-form-urlencoded"})
    @POST(ApiConfig.ForgetPasswordUrl)
    Call<ProfileResponseDto> getforgetpassword(@Field("Email") String email, @Field("UserName") String  phone);


}
