
package com.pathway.geokrishi.Notification.DTO;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum implements Parcelable
{

    @SerializedName("NotificationID")
    @Expose
    private Integer notificationID;
    @SerializedName("Notification")
    @Expose
    private String notification;
    @SerializedName("NotificationDetail")
    @Expose
    private Object notificationDetail;
    @SerializedName("NotificationDate")
    @Expose
    private String notificationDate;
    public final static Creator<Datum> CREATOR = new Creator<Datum>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Datum createFromParcel(Parcel in) {
            Datum instance = new Datum();
            instance.notificationID = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.notification = ((String) in.readValue((String.class.getClassLoader())));
            instance.notificationDetail = ((Object) in.readValue((Object.class.getClassLoader())));
            instance.notificationDate = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public Datum[] newArray(int size) {
            return (new Datum[size]);
        }

    }
    ;

    public Integer getNotificationID() {
        return notificationID;
    }

    public void setNotificationID(Integer notificationID) {
        this.notificationID = notificationID;
    }

    public String getNotification() {
        return notification;
    }

    public void setNotification(String notification) {
        this.notification = notification;
    }

    public Object getNotificationDetail() {
        return notificationDetail;
    }

    public void setNotificationDetail(Object notificationDetail) {
        this.notificationDetail = notificationDetail;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(notificationID);
        dest.writeValue(notification);
        dest.writeValue(notificationDetail);
        dest.writeValue(notificationDate);
    }

    public int describeContents() {
        return  0;
    }

}
