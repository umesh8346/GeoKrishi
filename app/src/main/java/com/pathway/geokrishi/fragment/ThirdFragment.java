package com.pathway.geokrishi.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.pathway.geokrishi.Login_Activity;
import com.pathway.geokrishi.MainActivity;
import com.pathway.geokrishi.R;
import com.pathway.geokrishi.utils.AppUtils;


public class ThirdFragment extends Fragment {
    Context mContext;
    static String type;

    // if value needed
    public static ThirdFragment getInstanceFragment() {
        ThirdFragment fragment = new ThirdFragment();

        return fragment;

    }

    @Override
    public void onAttach(Activity activity) {
        // TODO Auto-generated method stub
        super.onAttach(activity);
        mContext = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;

        view = inflater.inflate(R.layout.first_layout, container,
                false);
        ImageView imageview=(ImageView)view.findViewById(R.id.imageview);
        imageview.setImageResource(R.drawable.slide3);
        AppUtils.transparentStatusBar(getActivity().getWindow());
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        imageview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), Login_Activity.class));
               getActivity(). finish();
            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);

    }
}
