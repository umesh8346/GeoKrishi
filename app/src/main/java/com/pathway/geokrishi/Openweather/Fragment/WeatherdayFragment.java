package com.pathway.geokrishi.Openweather.Fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.pathway.geokrishi.ApiController.ApiConfig;
import com.pathway.geokrishi.ApiController.ApiManager;
import com.pathway.geokrishi.Openweather.WeatherAdapter;
import com.pathway.geokrishi.Openweather.WeatherDTO.List;
import com.pathway.geokrishi.Openweather.WeatherDTO.WeatherDayDto;
import com.pathway.geokrishi.R;


import com.pathway.geokrishi.utils.AppConstant;
import com.pathway.geokrishi.utils.AppUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;


public class WeatherdayFragment extends Fragment {
FragmentManager  fragManager;
    ViewPager  viewPager;
    ProgressBar progressBar;
    TabLayout viewPagerTab;
    private Callback<WeatherDayDto> dayWeatherDTOCallback;
    ArrayList<List> daylist = new ArrayList<>();
    WeatherAdapter mAdapter;
    Toolbar toolbar;
 String locationId;
    String latitude,longitude;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.crop_viewpager_layout, container, false);
        fragManager = getActivity().getSupportFragmentManager();
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        progressBar= (ProgressBar) view.findViewById(R.id.progressBar);
        viewPagerTab = (TabLayout) view.findViewById(R.id.viewpagertab);
        toolbar=(Toolbar)view.findViewById(R.id.toolbar) ;
        toolbar.setVisibility(View.GONE);
        viewPager.addOnPageChangeListener(new
                TabLayout.TabLayoutOnPageChangeListener(viewPagerTab));
        latitude=String.valueOf(AppUtils.latitude(getActivity()));
        longitude=String.valueOf(AppUtils.longitude(getActivity()));
        dayWeatherDTOCallback = new Callback<WeatherDayDto>() {
            @Override
            public void onResponse(Call<WeatherDayDto> call, retrofit2.Response<WeatherDayDto> response) {
                System.out.println("weatherResponse==="+response);
                if (response.isSuccessful()) {

                    WeatherDayDto weatherResponse = response.body();
                    System.out.println("weatherResponse==="+weatherResponse);
                    daylist=weatherResponse.getList();
                    for (int i = 0; i < daylist.size(); i++) {

                        viewPagerTab.addTab(viewPagerTab.newTab().setText(AppUtils.getweatherday(daylist.get(i).getDt())));
                    }
                    mAdapter= new WeatherAdapter(
                            getActivity(), daylist);
                    viewPager.setAdapter(mAdapter);
                    viewPager.addOnPageChangeListener(new
                            TabLayout.TabLayoutOnPageChangeListener(viewPagerTab));
                    viewPagerTab.setOnTabSelectedListener(new
                                                                  TabLayout.OnTabSelectedListener() {
                                                                      @Override
                                                                      public void onTabSelected(TabLayout.Tab tab) {
                                                                          viewPager.setCurrentItem(tab.getPosition());

                                                                      }

                                                                      @Override
                                                                      public void onTabUnselected(TabLayout.Tab tab) {

                                                                      }

                                                                      @Override
                                                                      public void onTabReselected(TabLayout.Tab tab) {

                                                                      }

                                                                  });





            }
            }

            @Override
            public void onFailure(Call<WeatherDayDto> call, Throwable t) {
                System.out.println("error=="+t);

                 Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        };

        if (AppUtils.checkInternetConnection(getActivity())) {


       locationId=AppUtils.locationid(getActivity());
            System.out.println("location=="+locationId);
            if(locationId.equals("")){
                locationId="7649119";
            }



            String url= ApiConfig.OpenWeatherDayUrl.replace("{lat}","lat="+latitude+"&lon="+longitude+"&lang=en&cnt=5&appid=6023985399897109c893543cfd4ce0bb");


            System.out.println("url=="+url);
            ApiManager.DayOpenWeather(dayWeatherDTOCallback,url);

        } else {
            AppUtils.errordialog(getActivity(), AppConstant.no_network);
        }





        return view;
    }

}
