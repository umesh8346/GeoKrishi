package com.pathway.geokrishi;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;

abstract class BaseActivityOther extends AppCompatActivity  implements
        NavigationView.OnNavigationItemSelectedListener{

    public Toolbar toolbar;
    private float lastTranslate = 0.0f;
    public static DrawerLayout drawer = null;
    int PERMISSION_ALL = 1;
    public static NavigationView navigationView;
    protected ActionBarDrawerToggle toggle;
    private  CoordinatorLayout sliderContent = null;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getResourceLayout());

        toolbar = (Toolbar) findViewById(getToolbar());
        toolbar.setTitle(getToolbarTitle());
        setSupportActionBar(toolbar);
        if(getActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //initNavigationDrawer();


        String[] PERMISSIONS = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION};

//        if(!hasPermissions(this, PERMISSIONS)){
//            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
//        }

    }


   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            // Handle the camera action

        }else if(id == R.id.nav_logout){
            startActivity(new Intent(BaseActivityOther.this,Login_Activity.class));
        }
        else if(id==R.id.nav_language_setting){
            startActivity(new Intent(BaseActivityOther.this,SettingActivity.class));
        }
        drawer.closeDrawer(Gravity.RIGHT);
        return true;
    }

    protected abstract int getToolbar();

    protected abstract int getActivityID();

    protected abstract int getToolbarTitle();

    protected abstract int getResourceLayout();



    protected abstract Activity getInstance();


}
