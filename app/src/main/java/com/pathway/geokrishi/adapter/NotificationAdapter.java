package com.pathway.geokrishi.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.style.SuperscriptSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.pathway.geokrishi.Notification.DTO.Datum;
import com.pathway.geokrishi.Openweather.WeatherHourDTO.List;
import com.pathway.geokrishi.R;
import com.pathway.geokrishi.utils.DonutProgress;

import java.util.ArrayList;


public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    private Context mContext;
    ArrayList<Datum> notificationlist = new ArrayList<>();

    public NotificationAdapter(Context c, ArrayList<Datum> p) {
        this.mContext = c;
        this.notificationlist = p;
    }

    @Override
    public int getItemCount() {
        return notificationlist.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView textviewnotification;
        public View mTextView;
        public ViewHolder(View v) {
            super(v);
            mTextView = v;
        }

    }


    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                             int viewType) {
        // create a new view
        View vi = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_item, parent, false);
        ViewHolder vh = new ViewHolder(vi);
        vh.textviewnotification = (TextView) vi.findViewById(R.id.textviewNotifications);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String text = null;
        holder.textviewnotification.setText((notificationlist.get(position).getNotification()));

    }
}