package com.pathway.geokrishi.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.pathway.geokrishi.ApiController.ApiManager;
import com.pathway.geokrishi.ApiController.ResponseDTO.CropCategoryResponseDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.CropListDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.CropVaritiesDto;
import com.pathway.geokrishi.DetailActivity;
import com.pathway.geokrishi.Login_Activity;
import com.pathway.geokrishi.R;
import com.pathway.geokrishi.utils.AppConstant;
import com.pathway.geokrishi.utils.AppUtils;
import com.pathway.geokrishi.utils.Calculation;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.logging.Handler;

import retrofit2.Call;
import retrofit2.Callback;

public class NPKCalculationFragment extends Fragment  implements View.OnClickListener{

    private Callback<CropCategoryResponseDto> cropCategoryeResponseDtoCallback;
    private Callback<CropListDto> croplistResponseDtoCallback;
    private Callback<CropVaritiesDto> cropVaritiesDtoCallback;
    ArrayList<CropListDto.Data> Croplist;
    ArrayList<CropVaritiesDto.Data> cropvaritiesList;
    ArrayList<CropCategoryResponseDto.Data> categoryList;
    Spinner CropCategoryspinner,SpinnerCrop,SpinnerCropVarities;
    String  CategoryIdString, CropIdString;
    Dialog calculationdailog;
    EditText EdittextNitrogenGiven,EdittextPhosphorousGiven,EdittextPotassiumGiven,EdittextPotassium,EdittextNitrogen,EdittextPhosphorous;
    String nitrogenString,phosphorousString,potassiumString,defaultnitrogen,defaultphosphorous,defaultpotassium;
    Button buttonok,btncalculation2;
    TextView TextViewUrea,TextViewDAP,TextViewMOP;
    double nitogen,phosphoraous,potassium;
    ProgressDialog progressDialog;

    public static NPKCalculationFragment newInstance() {
        // TODO Auto-generated method stub
        NPKCalculationFragment fragment = new NPKCalculationFragment();
        Bundle bundle = new Bundle();
      fragment.setArguments(bundle);

        return fragment;
    } @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method
        View view = inflater.inflate(R.layout.npk_calculation_layout, null);
        CropCategoryspinner = (Spinner)view.findViewById(R.id.CropCategoryspinner);
        SpinnerCrop = (Spinner) view.findViewById(R.id.SpinnerCrop);
        progressDialog = new ProgressDialog(getActivity(), R.style.AppCompatAlertDialogStyle);
        progressDialog.setMessage("Loading....");
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        SpinnerCropVarities= (Spinner) view.findViewById(R.id.SpinnerCropVarities);
        EdittextNitrogenGiven=(EditText)view.findViewById(R.id.EdittextNitrogenGiven) ;
                EdittextPhosphorousGiven=(EditText)view.findViewById(R.id.EdittextPhosphorousGiven) ;
        EdittextPotassiumGiven=(EditText)view.findViewById(R.id.EdittextPotassiumGiven) ;
                EdittextPotassium=(EditText)view.findViewById(R.id.EdittextPotassium) ;
        EdittextNitrogen=(EditText)view.findViewById(R.id.EdittextNitrogen) ;
                EdittextPhosphorous=(EditText)view.findViewById(R.id.EdittextPhosphorous) ;
      //  btncalculation=(Button)view.findViewById(R.id.btncalculation);
        btncalculation2=(Button)view.findViewById(R.id.btncalculation2);
       // btncalculation.setOnClickListener(this);
        btncalculation2.setOnClickListener(this);
        EdittextPotassium.setText("0.0");
        EdittextNitrogen.setText("0.0");
        EdittextPhosphorous.setText("0.0");
        cropCategoryeResponseDtoCallback = new Callback<CropCategoryResponseDto>() {
            @Override
            public void onResponse(Call<CropCategoryResponseDto> call, retrofit2.Response<CropCategoryResponseDto> response) {
                //AppLog.d(TAG, response.body().toString());

                if (response.isSuccessful()) {

                    CropCategoryResponseDto cropCategoryResponse = response.body();
                    if (cropCategoryResponse.getStatus() == 0) {

                        categoryList = cropCategoryResponse.getData();
                        cropcatagoryadapter();
                    } else if (cropCategoryResponse.getStatus() == 1) {
                        progressDialog.dismiss();
                        AppUtils.errordialog(getActivity(), AppConstant.Server_Error);
                    }


                } else {
                    progressDialog.dismiss();
//                AppUtils.hideprogressbar(getApplicationContext(),progressBar,progressbarInterface);
                    AppUtils.errordialog(getActivity(), AppConstant.Server_Error);

                }
            }

            @Override
            public void onFailure(Call<CropCategoryResponseDto> call, Throwable t) {
                progressDialog.dismiss();
                AppUtils.errordialog(getActivity(), t.getMessage());
            }
        };

        //crop list
        croplistResponseDtoCallback = new Callback<CropListDto>() {
            @Override
            public void onResponse(Call<CropListDto> call, retrofit2.Response<CropListDto> response) {
                //AppLog.d(TAG, response.body().toString());

                if (response.isSuccessful()) {

                    CropListDto croplistResponse = response.body();
                    if (croplistResponse.getStatus() == 0) {

                        Croplist = croplistResponse.getData();
                        croplistdapter();
                    } else if (croplistResponse.getStatus() == 1) {
                        progressDialog.dismiss();
                        AppUtils.errordialog(getActivity(), AppConstant.Server_Error);
                    }


                } else {
                    progressDialog.dismiss();
//                AppUtils.hideprogressbar(getApplicationContext(),progressBar,progressbarInterface);
                    AppUtils.errordialog(getActivity(), AppConstant.Server_Error);

                }
            }

            @Override
            public void onFailure(Call<CropListDto> call, Throwable t) {
                progressDialog.dismiss();
                AppUtils.errordialog(getActivity(), t.getMessage());
            }
        };


        //crop vatities


        cropVaritiesDtoCallback = new Callback<CropVaritiesDto>() {
            @Override
            public void onResponse(Call<CropVaritiesDto> call, retrofit2.Response<CropVaritiesDto> response) {
                //AppLog.d(TAG, response.body().toString());

                if (response.isSuccessful()) {

                    CropVaritiesDto cropvaritiesresponse = response.body();
                    if (cropvaritiesresponse.getStatus() == 0) {

                        cropvaritiesList = cropvaritiesresponse.getData();
                        CropvaritiesListadapter();
                    } else if (cropvaritiesresponse.getStatus() == 1) {
                        progressDialog.dismiss();
                        AppUtils.errordialog(getActivity(), AppConstant.Server_Error);
                    }


                } else {
                    progressDialog.dismiss();
//                AppUtils.hideprogressbar(getApplicationContext(),progressBar,progressbarInterface);
                    AppUtils.errordialog(getActivity(), AppConstant.Server_Error);

                }
            }

            @Override
            public void onFailure(Call<CropVaritiesDto> call, Throwable t) {
                progressDialog.dismiss();
                AppUtils.errordialog(getActivity(), t.getMessage());
            }
        };




        if (AppUtils.checkInternetConnection(getActivity())) {
            ApiManager.getCropCategory(cropCategoryeResponseDtoCallback, AppUtils.apilanguage(getActivity()));

        } else {
            AppUtils.errordialog(getActivity(), AppConstant.no_network);
        }


        return view;
    }






    public void cropcatagoryadapter() {

        final ArrayAdapter<CropCategoryResponseDto.Data> adapter = new ArrayAdapter<>(
                getActivity(), R.layout.spinner_list_item,
                categoryList);
        CropCategoryspinner.setAdapter(adapter);

        CropCategoryspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub

                CategoryIdString = String.valueOf(categoryList.get(arg2).getCropCategoryID());
                System.out.println("CategoryIdString===" + CategoryIdString);
                ApiManager.getCropList(croplistResponseDtoCallback, CategoryIdString, AppUtils.apilanguage(getActivity()));

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });


    }

    public void croplistdapter() {

        final ArrayAdapter<CropListDto.Data> adapter = new ArrayAdapter<>(
              getActivity(), R.layout.spinner_list_item,
                Croplist);
        SpinnerCrop.setAdapter(adapter);

        SpinnerCrop.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub

                CropIdString = String.valueOf(Croplist.get(arg2).getCropID());
                System.out.println("CropIdString===" + CropIdString);
                ApiManager.getCropVarities(cropVaritiesDtoCallback, CropIdString, AppUtils.apilanguage(getActivity()));

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });


    }

    public void CropvaritiesListadapter() {

        final ArrayAdapter<CropVaritiesDto.Data> adapter = new ArrayAdapter<>(
                getActivity(), R.layout.spinner_list_item,
                cropvaritiesList);
        SpinnerCropVarities.setAdapter(adapter);

        SpinnerCropVarities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub

                defaultnitrogen=cropvaritiesList.get(arg2).getNitrogen();
                        defaultphosphorous=cropvaritiesList.get(arg2).getPhosphor();
                defaultpotassium=cropvaritiesList.get(arg2).getPotash();

                EdittextNitrogenGiven.setText(defaultnitrogen);
                        EdittextPhosphorousGiven.setText(defaultphosphorous);
                EdittextPotassiumGiven.setText(defaultpotassium);
                progressDialog.dismiss();

                System.out.println("CropIdString===" + CropIdString);
                //ApiManager.getCropList(croplistResponseDtoCallback, CategoryIdString, AppUtils.apilanguage(CropCalenderActivity.this));

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });


    }




    private void gettext(){

        nitrogenString=AppUtils.gettextstring(EdittextNitrogen);
        phosphorousString=AppUtils.gettextstring(EdittextPhosphorous);
        potassiumString=AppUtils.gettextstring(EdittextPotassium);

    }
    private boolean isvalidation(){
        gettext();
        if (!AppUtils.isvalid(nitrogenString)||Calculation.getdoublevalue(nitrogenString)>=1) {
            EdittextNitrogen.setError(getText(R.string.error_msg));

            return false;
        }
        if (!AppUtils.isvalid(phosphorousString)||Calculation.getdoublevalue(phosphorousString)>=1) {
            EdittextPhosphorous.setError(getText(R.string.error_msg));

            return false;
        }
        if (!AppUtils.isvalid(potassiumString)||Calculation.getdoublevalue(potassiumString)>=1) {
            EdittextPotassium.setError(getText(R.string.error_msg));

            return false;
        }
     return true;
    }

    @Override
    public void onClick(View v) {
//        if(v==btncalculation){
//
//                calculationdailog();
//
//        }
        if(v==buttonok){
            calculationdailog.dismiss();
        }
        if(v==btncalculation2){
            if(isvalidation()){



                calculationdailog2();
            }
        }

    }


    private  void calculationdailog(){
        calculationdailog= new Dialog(getActivity());
        calculationdailog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        calculationdailog.setContentView(R.layout.calculation_result_layout);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(calculationdailog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        TextViewUrea=(TextView) calculationdailog.findViewById(R.id.TextViewUrea);
                TextViewDAP=(TextView) calculationdailog.findViewById(R.id.TextViewDAP);
        TextViewMOP=(TextView) calculationdailog.findViewById(R.id.TextViewMOP);
        buttonok=(Button) calculationdailog.findViewById(R.id.buttonok);
        buttonok.setOnClickListener(this);
       // TextViewUrea.setText(String.valueOf(Calculation.ureaCalculation( Double.parseDouble(defaultnitrogen),Double.parseDouble(defaultphosphorous))));
        TextViewDAP.setText(String.valueOf(Calculation.dapcalculation(Double.parseDouble(defaultphosphorous))));
        TextViewMOP.setText(String.valueOf(Calculation.mopcalculation(Double.parseDouble(defaultpotassium))));

        TextViewUrea.setText(new DecimalFormat("##.##").format(Calculation.ureaCalculation( Double.parseDouble(defaultnitrogen),Double.parseDouble(defaultphosphorous))));

        calculationdailog.getWindow().setAttributes(lp);
        calculationdailog.show();
    }


    private  void calculationdailog2(){
        calculationdailog= new Dialog(getActivity());
        calculationdailog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        calculationdailog.setContentView(R.layout.calculation_result_layout);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(calculationdailog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        TextViewUrea=(TextView) calculationdailog.findViewById(R.id.TextViewUrea);
        TextViewDAP=(TextView) calculationdailog.findViewById(R.id.TextViewDAP);
        TextViewMOP=(TextView) calculationdailog.findViewById(R.id.TextViewMOP);
        buttonok=(Button) calculationdailog.findViewById(R.id.buttonok);
        buttonok.setOnClickListener(this);
        nitogen=Double.parseDouble(defaultnitrogen) -Calculation.getnpksoil(Double.parseDouble(nitrogenString));
                phosphoraous=Double.parseDouble(defaultphosphorous) -Calculation.getnpksoil(Double.parseDouble(phosphorousString));
        potassium=Double.parseDouble(defaultpotassium) -Calculation.getnpksoil(Double.parseDouble(potassiumString));
       // TextViewUrea.setText(String.valueOf(Calculation.ureaCalculation( nitogen,phosphoraous)));
        TextViewUrea.setText(new DecimalFormat("##.##").format(Calculation.ureaCalculation( nitogen,phosphoraous)));
        TextViewDAP.setText(String.valueOf(Calculation.dapcalculation(phosphoraous)));
        TextViewDAP.setText(new DecimalFormat("##.##").format(Calculation.dapcalculation(phosphoraous)));
        TextViewMOP.setText(new DecimalFormat("##.##").format(Calculation.mopcalculation(potassium)));
        calculationdailog.getWindow().setAttributes(lp);
        calculationdailog.show();
    }




}
