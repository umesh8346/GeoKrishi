package com.pathway.geokrishi.ApiController.ResponseDTO;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CropVaritiesDto {

    @SerializedName("Status")
    public int Status;
    @SerializedName("Message")
    public String Message;
    @SerializedName("Data")
    public ArrayList<Data> Data;

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public ArrayList<CropVaritiesDto.Data> getData() {
        return Data;
    }

    public void setData(ArrayList<CropVaritiesDto.Data> data) {
        Data = data;
    }

    public static class Data {
        @SerializedName("CropVarietyCategoryID")
        public int CropVarietyCategoryID;
        @SerializedName("CropVarietyCategory")
        public String CropVarietyCategory;
        @SerializedName("Phosphor")
        public String Phosphor;
        @SerializedName("Nitrogen")
        public String Nitrogen;
        @SerializedName("Potash")
        public String Potash;

        public int getCropVarietyCategoryID() {
            return CropVarietyCategoryID;
        }

        public void setCropVarietyCategoryID(int cropVarietyCategoryID) {
            CropVarietyCategoryID = cropVarietyCategoryID;
        }

        public String getCropVarietyCategory() {
            return CropVarietyCategory;
        }

        public void setCropVarietyCategory(String cropVarietyCategory) {
            CropVarietyCategory = cropVarietyCategory;
        }

        public String getPhosphor() {
            return Phosphor;
        }

        public void setPhosphor(String phosphor) {
            Phosphor = phosphor;
        }

        public String getNitrogen() {
            return Nitrogen;
        }

        public void setNitrogen(String nitrogen) {
            Nitrogen = nitrogen;
        }

        public String getPotash() {
            return Potash;
        }

        public void setPotash(String potash) {
            Potash = potash;
        }

        @Override
        public String toString()
        {
            return this.CropVarietyCategory;

        }
    }
}
