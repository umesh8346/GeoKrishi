package com.pathway.geokrishi.Notification;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import com.pathway.geokrishi.ApiController.ApiManager;
import com.pathway.geokrishi.BaseActivity;
import com.pathway.geokrishi.MainDashBoard.DashboardActivity;
import com.pathway.geokrishi.Notification.DTO.Datum;
import com.pathway.geokrishi.Notification.DTO.NotificationDTO;
import com.pathway.geokrishi.R;
import com.pathway.geokrishi.adapter.NotificationAdapter;
import com.pathway.geokrishi.utils.AppUtils;
import com.pathway.geokrishi.utils.ItemClickSupport;
import java.util.ArrayList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
public class NotificationActivity extends BaseActivity implements View.OnClickListener {
    RecyclerView recycleviewHourWeather;
    LinearLayoutManager mLayoutManager;
    ArrayList<Datum> myList = new ArrayList<>();
    private Callback<NotificationDTO> NotificationDTOCallback;
    ArrayList<Datum> notificationlist = new ArrayList<>();
    String notificatioID;
    TextView textviewNotificationtitle, textviewNotificationdetail;
    Dialog NotificationDailog;
    String notificationtitleString, notificationdetailString;
    Button buttonok;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recycleviewHourWeather = (RecyclerView) findViewById(R.id.recycleviewHourWeather);
        myList = (ArrayList<Datum>) getIntent().getSerializableExtra("mylist");
        hrweatheradapter();
        NotificationDTOCallback = new Callback<NotificationDTO>() {
            @Override
            public void onResponse(Call<NotificationDTO> call, Response<NotificationDTO> response) {
                System.out.println("weatherResponse===" + response.body());
                if (response.isSuccessful()) {

                    NotificationDTO notificationresponse = response.body();
                    notificationlist = notificationresponse.getData();
                    if (notificationlist.size() > 0) {
                        notificationtitleString = notificationlist.get(0).getNotification();
                        System.out.println("notificationtitleString===" + notificationtitleString);
                        notificationdetailString = String.valueOf(notificationlist.get(0).getNotificationDetail());
                        notificationdailog();
                    } else {

                    }

                }

            }

            @Override
            public void onFailure(Call<NotificationDTO> call, Throwable t) {
                System.out.println("error==" + t);

            }
        };


    }

    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }

    @Override
    protected int getActivityID() {
        return 0;
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.notification_setting;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.notification_layout;
    }

    @Override
    protected int getDrawerLayout() {
        return 0;
    }

    @Override
    protected int getNavigationView() {
        return 0;
    }

    @Override
    protected Activity getInstance() {
        return null;
    }

    private void hrweatheradapter() {
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        NotificationAdapter notificationadapter = new NotificationAdapter(getApplicationContext(), myList);
        recycleviewHourWeather.setLayoutManager(mLayoutManager);
        recycleviewHourWeather.setAdapter(notificationadapter);
        ItemClickSupport.addTo(recycleviewHourWeather).setOnItemClickListener
                (new ItemClickSupport.OnItemClickListener() {
                     @Override
                     public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                         notificatioID = String.valueOf(myList.get(position).getNotificationID());
                         ApiManager.getNotificationdetail(NotificationDTOCallback,  AppUtils.userId(NotificationActivity.this), notificatioID);

                     }


                 }
                );
    }

    private void notificationdailog() {
        NotificationDailog = new Dialog(NotificationActivity.this);
        NotificationDailog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        NotificationDailog.setContentView(R.layout.notification_detail_layout);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(NotificationDailog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;

        textviewNotificationtitle = (TextView) NotificationDailog.findViewById(R.id.textviewNotificationtitle);
        textviewNotificationdetail = (TextView) NotificationDailog.findViewById(R.id.textviewNotificationdetail);
        textviewNotificationtitle.setText(notificationtitleString);
          textviewNotificationdetail.setText(notificationdetailString);

        buttonok = (Button) NotificationDailog.findViewById(R.id.buttonok);
        buttonok.setOnClickListener(this);

        NotificationDailog.getWindow().setAttributes(lp);
        NotificationDailog.show();
    }

    @Override
    public void onClick(View v) {
        if (v == buttonok) {
            NotificationDailog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
