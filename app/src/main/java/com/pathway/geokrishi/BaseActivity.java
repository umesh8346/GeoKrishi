package com.pathway.geokrishi;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;

import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.pathway.geokrishi.Interface.NetworkInterFace;
import com.pathway.geokrishi.Interface.Sidemenu;
import com.pathway.geokrishi.utils.AppConstant;
import com.pathway.geokrishi.utils.AppUtils;

import java.util.Locale;


public abstract class BaseActivity extends AppCompatActivity  implements
        NavigationView.OnNavigationItemSelectedListener{

    public Toolbar toolbar;
    private float lastTranslate = 0.0f;
    public static DrawerLayout drawer = null;
    int PERMISSION_ALL = 1;
    public static NavigationView navigationView;
    protected ActionBarDrawerToggle toggle;
    private  CoordinatorLayout sliderContent = null;
    Profile profile;
    String language;
    private Locale locale;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getResourceLayout());
        toolbar = (Toolbar) findViewById(getToolbar());
        toolbar.setTitle(getToolbarTitle());
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(getDrawerLayout());
        if(getActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initNavigationDrawer();
        String[] PERMISSIONS = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION};
        language=AppUtils.language(BaseActivity.this);

        if(language.equals("")){
            languagesetting();
            language=AppUtils.language(BaseActivity.this);
            getlanguage();
        }
        else {
            getlanguage();
        }

    }

    @Override
    public void onBackPressed() {

        if(getDrawerLayout() != 0){
            if (drawer.isDrawerOpen(Gravity.RIGHT)) {
               // drawer.closeDrawer(Gravity.RIGHT);
            } else {
                super.onBackPressed();
            }
        }

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            if (AppUtils.checkInternetConnection(BaseActivity.this)){
                startActivity(new Intent(BaseActivity.this,
                        ProfileActivity.class));
        }
        else {
            AppUtils.errordialog(BaseActivity.this, AppConstant.no_network);
        }





        }else if(id == R.id.nav_logout){

            if (AppUtils.checkInternetConnection(BaseActivity.this)) {


                profile = Profile.getCurrentProfile().getCurrentProfile();
                if (profile != null) {
                    LoginManager.getInstance().logOut();
                    showdailog(getText(R.string.logout)+"");

                } else {
                    showdailog(getText(R.string.logout)+"");
                    // user has not logged in
                }

                //  login_button.performClick();
            } else {
            AppUtils.errordialog(BaseActivity.this, AppConstant.no_network);
            }



          //  showdailog("Are you sure to log out?");

        }
        else if(id==R.id.nav_language_setting){
            startActivity(new Intent(BaseActivity.this,SettingActivity.class));
        }

        if (id == R.id.nav_location) {

            startActivity(new Intent(BaseActivity.this,
                    LocationSettingActivity.class));

        }

        if (id == R.id.nav_aboutus) {

            startActivity(new Intent(BaseActivity.this,
                    AboutActivity.class));

        }


        if(id==R.id.nav_change_password){
            startActivity(new Intent(BaseActivity.this,ChangepasswordActivity.class));
        }
        drawer.closeDrawer(Gravity.RIGHT);
        return true;
    }

    protected abstract int getToolbar();

    protected abstract int getActivityID();

    protected abstract int getToolbarTitle();

    protected abstract int getResourceLayout();

    protected abstract int getDrawerLayout();
    protected abstract int getNavigationView();
    protected abstract Activity getInstance();
    protected void initNavigationDrawer() {

        if (getDrawerLayout() != 0) {
            drawer = (DrawerLayout) findViewById(getDrawerLayout());
            toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open,
                    R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.syncState();
            AppUtils.transparentStatusBar(getWindow());
            navigationView = (NavigationView) findViewById(getNavigationView());
            drawer.closeDrawer(navigationView);
            View headerview = navigationView.getHeaderView(0);
            navigationView.setNavigationItemSelectedListener(this);
        }
    }

    public  void showdailog(String message){
        new AlertDialog.Builder(BaseActivity.this,R.style.AppCompatAlertDialogStyle)
                .setTitle("Alert")
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(BaseActivity.this,
                                Login_Activity.class));
                        CookieSyncManager.createInstance(BaseActivity.this);
                        CookieManager cookieManager = CookieManager.getInstance();
                        cookieManager.removeAllCookie();
                       AppUtils.loginshareprefrence(BaseActivity.this);
                        dialog.dismiss();
                        finish();
                    }
                })
                .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

     public void getlanguage(){
         if (language.equals("np")) {
             locale = new Locale(language);
             Locale.setDefault(locale);
             android.content.res.Configuration config = new android.content.res.Configuration();
             config.locale = locale;
             getBaseContext().getResources().updateConfiguration(config,
                     getBaseContext().getResources().getDisplayMetrics());
         } else {

             locale = new Locale(language);
             Locale.setDefault(locale);
             android.content.res.Configuration config = new android.content.res.Configuration();
             config.locale = locale;
             getBaseContext().getResources().updateConfiguration(config,
                     getBaseContext().getResources().getDisplayMetrics());
         }
     }
public void languagesetting(){
    SharedPreferences sh_Pref_S1 = BaseActivity.this
            .getSharedPreferences("Language Credential",
                    Context.MODE_PRIVATE);
    SharedPreferences.Editor toEdit1 = sh_Pref_S1.edit();
    toEdit1.putString("LANGUAGE", "en");
    toEdit1.commit();
}




}
