package com.pathway.geokrishi.Boardcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.pathway.geokrishi.ApiController.Networkcontroller;
import com.pathway.geokrishi.Interface.NetworkInterFace;
import com.pathway.geokrishi.MainActivity;
import com.pathway.geokrishi.utils.AppConstant;
import com.pathway.geokrishi.utils.AppUtils;



public class NetworkChangeReceiver extends BroadcastReceiver{
    NetworkInterFace networkInterFace;
    Context context;
    @Override
    public void onReceive(final Context context, final Intent intent) {


       if(AppUtils.checkInternetConnection(context)){
           Intent serviceIntent = new Intent(context,AppService.class);
           context.startService(serviceIntent);
       }
       else{
        AppUtils.errordialog(context, AppConstant.no_network);
       }

    }



}
