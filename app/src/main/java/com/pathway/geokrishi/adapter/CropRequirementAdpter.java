package com.pathway.geokrishi.adapter;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.pathway.geokrishi.ApiController.ResponseDTO.SuitableCropsDetail;
import com.pathway.geokrishi.Interface.EditInterface;
import com.pathway.geokrishi.R;
import java.util.ArrayList;

public class CropRequirementAdpter extends BaseAdapter {
    Context mcontect;
    EditInterface editInterface;
    ArrayList<SuitableCropsDetail.Containers> ContainerList = new ArrayList<>();

    public  CropRequirementAdpter(Context c, EditInterface editInterface, ArrayList<SuitableCropsDetail.Containers> ContainerList ){
        this.mcontect=c;
        this.editInterface =editInterface;
        this.ContainerList=ContainerList;

    }
    @Override
    public int getCount() {
        return ContainerList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }
    @Override
    public long getItemId(int position) {
        return 0;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Button btnEditcroprequirement;
        TextView textviewCropRequirement,textviewCropdes;
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.crop_item_layout, null);
        btnEditcroprequirement=(Button)view.findViewById(R.id.btnEditcroprequirement);
        textviewCropRequirement=(TextView)view.findViewById(R.id.textviewCropRequirement) ;
        textviewCropdes=(TextView)view.findViewById(R.id.textviewCropdes) ;
        textviewCropRequirement.setText(Html.fromHtml(ContainerList.get(position).getContainerName()));
        textviewCropdes.setText(ContainerList.get(position).getContainerValue());
        btnEditcroprequirement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editInterface.EditButtonListner(v,position);
            }
        });

        return view;

    }
}
