package com.pathway.geokrishi;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.pathway.geokrishi.ApiController.ApiManager;
import com.pathway.geokrishi.ApiController.CustomDialogs;
import com.pathway.geokrishi.ApiController.ResponseDTO.CropCategoryResponseDto;
import com.pathway.geokrishi.ApiController.ResponseDTO.RegisterResponseDto;
import com.pathway.geokrishi.Fcm.Configs;
import com.pathway.geokrishi.Fcm.NotificationUtils;
import com.pathway.geokrishi.MainDashBoard.DashboardActivity;
import com.pathway.geokrishi.utils.AppConstant;
import com.pathway.geokrishi.utils.AppUtils;
import com.pathway.geokrishi.utils.Config;
import com.pathway.geokrishi.utils.GPSTracker;

import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SignupActivity extends BaseActivity implements View.OnClickListener {
    EditText edittextEmail, edittextPhone, edittextPassword, edittextConfirmpassword;

    String emailString, phoneString, passwordString, confirmpasswordString;
    RelativeLayout relativelayoutSignup;

    private Callback<RegisterResponseDto> userRegisterResponseDtoCallback;
    ProgressDialog progressDialog;
    GPSTracker gps;
    double latitude, longitude;
    SharedPreferences sharedPreferences;
    String regId;
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    String androidID;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressDialog = new ProgressDialog(SignupActivity.this);
        androidID= Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        init();
        getlatitudeandlongitude();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {

                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {

                    String message = intent.getStringExtra("message");
                }
            }
        };
        displayFirebaseRegId();
        userRegisterResponseDtoCallback = new Callback<RegisterResponseDto>() {
            @Override
            public void onResponse(Call<RegisterResponseDto> call, Response<RegisterResponseDto> response) {
                if (response.isSuccessful()) {

                    RegisterResponseDto userRegisterResponse = response.body();
                    if (userRegisterResponse.getStatus() == 0) {
                        AppUtils.loginshareprefrence(getApplicationContext(), userRegisterResponse.getData().getUserID() + "", userRegisterResponse.getData().getFirstName(),
                                userRegisterResponse.getData().getLastName(), userRegisterResponse.getData().getUserName(), userRegisterResponse.getData().getEmail(), userRegisterResponse.getData().UserName,
                                userRegisterResponse.getData().getOrganization(), userRegisterResponse.getData().getProfession(), String.valueOf(userRegisterResponse.getData().getLongitude()), String.valueOf(userRegisterResponse.getData().getLatitude()), userRegisterResponse.getData().getAvatarImage());
                        showdailog(AppConstant.Register_message);
                    } else if (userRegisterResponse.getStatus() == 1) {
                        progressDialog.dismiss();
                        AppUtils.errordialog(SignupActivity.this, userRegisterResponse.getMessage());
                    }

                } else {
                    progressDialog.dismiss();
                    AppUtils.errordialog(SignupActivity.this, AppConstant.Server_Error);
                }
            }

            @Override
            public void onFailure(Call<RegisterResponseDto> call, Throwable t) {

            }
        };
    }

    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }

    @Override
    protected int getActivityID() {
        return 0;
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.sign_up;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.signup_layout;
    }

    @Override
    protected int getDrawerLayout() {
        return 0;
    }

    @Override
    protected int getNavigationView() {
        return 0;
    }

    @Override
    protected Activity getInstance() {
        return null;
    }

    private void init() {
//        edittextFirstname = (EditText) findViewById(R.id.edittextFirstname);
//        edittextLastname = (EditText) findViewById(R.id.edittextLastname);
        edittextEmail = (EditText) findViewById(R.id.edittextEmail);
//        edittextOrganization = (EditText) findViewById(R.id.edittextOrganization);
//        edittextProfession = (EditText) findViewById(R.id.edittextProfession);
        edittextPhone = (EditText) findViewById(R.id.edittextPhone);
        edittextPassword = (EditText) findViewById(R.id.edittextPassword);
        edittextConfirmpassword = (EditText) findViewById(R.id.edittextConfirmpassword);
        relativelayoutSignup = (RelativeLayout) findViewById(R.id.relativelayoutSignup);
//        textviewSignIn = (TextView) findViewById(R.id.textviewSignIn);
        relativelayoutSignup.setOnClickListener(this);
//        textviewSignIn.setOnClickListener(this);
        edittextPassword.setTypeface(Typeface.DEFAULT);
        edittextPassword.setTransformationMethod(new PasswordTransformationMethod());
        edittextConfirmpassword.setTypeface(Typeface.DEFAULT);
        edittextConfirmpassword.setTransformationMethod(new PasswordTransformationMethod());

    }

    private void getstring() {

//        firstnameString = AppUtils.gettextstring(edittextFirstname);
//        lastnameString = AppUtils.gettextstring(edittextLastname);
        emailString = AppUtils.gettextstring(edittextEmail);
//        organizationString = AppUtils.gettextstring(edittextOrganization);
//        professionString = AppUtils.gettextstring(edittextProfession);
        phoneString = AppUtils.gettextstring(edittextPhone);
        passwordString = AppUtils.gettextstring(edittextPassword);
        confirmpasswordString = AppUtils.gettextstring(edittextConfirmpassword);
    }

    private boolean isvalidation() {
        getstring();
//        if (!AppUtils.isvalid(firstnameString)) {
//            edittextFirstname.setError(AppConstant.required_field);
//            return false;
//        }

//        if (!AppUtils.isvalid(lastnameString)) {
//            edittextLastname.setError(AppConstant.required_field);
//            return false;
//        }

        if (!emailString.equals("")&&!AppUtils.isEmailValid(emailString)) {
            edittextEmail.setError(AppConstant.email_error);
            return false;
        }
//        if (!AppUtils.isvalid(organizationString)) {
//            edittextOrganization.setError(AppConstant.required_field);
//            return false;
//        }
//        if (!AppUtils.isvalid(professionString)) {
//            edittextProfession.setError(AppConstant.required_field);
//            return false;
//        }
        if (phoneString.length() != 10) {
            edittextPhone.setError(AppConstant.phone_error);
            return false;
        }
        if (passwordString.length() < 6) {
            edittextPassword.setError(AppConstant.password_error);
            return false;
        }
        if (!passwordString.equals(confirmpasswordString)) {
            edittextConfirmpassword.setError(AppConstant.confirm_password_error);
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v == relativelayoutSignup) {
            if (AppUtils.checkInternetConnection(SignupActivity.this)) {
                if (isvalidation()) {


                    progressDialog.setMessage("Loading....");
                    progressDialog.setCancelable(false);
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.show();
                    ApiManager.UserRegistration(userRegisterResponseDtoCallback,
                            phoneString,   emailString,
                             passwordString,regId,androidID,String.valueOf(longitude),String.valueOf(latitude));
                }

            } else {
                AppUtils.errordialog(SignupActivity.this, AppConstant.no_network);

            }
//            if (v == textviewSignIn) {
//                startActivity(new Intent(this,
//                        Login_Activity.class));
//                finish();
//            }
        }
    }

    public void showdailog(String message) {
        new AlertDialog.Builder(SignupActivity.this, R.style.AppCompatAlertDialogStyle)
                .setTitle("Alert")
                .setMessage(message)

                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(SignupActivity.this,
                                DashboardActivity.class));

                        dialog.dismiss();
                        finish();

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(SignupActivity.this,
                                Login_Activity.class));
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public void getlatitudeandlongitude() {
        if (ContextCompat.checkSelfPermission(SignupActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(SignupActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SignupActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        } else {

            gps = new GPSTracker(SignupActivity.this, SignupActivity.this);
            if (gps.canGetLocation()) {

                latitude = gps.getLatitude();
                longitude = gps.getLongitude();

                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(this, Locale.getDefault());
                try {


                    addresses = geocoder.getFromLocation(latitude, longitude, 1);
                } catch (Exception ex) {

                }

            } else {

                gps.showSettingsAlert();
            }


        }

    }

    @Override
    public void onResume() {
        super.onResume();
        getlatitudeandlongitude();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));
        NotificationUtils.clearNotifications(getApplicationContext());


    }
    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
    regId = pref.getString("regId", null);

    }
}
