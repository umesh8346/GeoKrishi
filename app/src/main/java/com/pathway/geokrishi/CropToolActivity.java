package com.pathway.geokrishi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ProgressBar;

import com.google.gson.JsonObject;
import com.pathway.geokrishi.ApiController.ApiManager;
import com.pathway.geokrishi.ApiController.ResponseDTO.CropCategoryResponseDto;
import com.pathway.geokrishi.Interface.ProfileInterface;
import com.pathway.geokrishi.Interface.ProgressbarInterface;
import com.pathway.geokrishi.adapter.CropListAdapter;
import com.pathway.geokrishi.utils.AppConstant;
import com.pathway.geokrishi.utils.AppUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;


public class CropToolActivity extends BaseActivity implements ProgressbarInterface{
    private Callback<CropCategoryResponseDto> cropCategoryeResponseDtoCallback;
    CropListAdapter cropadpter;
    ArrayList<CropCategoryResponseDto.Data> categoryList;
    ViewPager viewPager;
    TabLayout viewPagerTab;
    FragmentManager fragManager;
    ProgressBar progressBar;
    ProgressbarInterface progressbarInterface;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    public void init() {
        progressbarInterface=this;
        fragManager = getSupportFragmentManager();
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        progressBar= (ProgressBar) findViewById(R.id.progressBar);
        AppUtils.showprogressbar(getApplicationContext(),progressBar,this);
        viewPagerTab = (TabLayout) findViewById(R.id.viewpagertab);

        categoryList = new ArrayList<>();
        cropCategoryeResponseDtoCallback = new Callback<CropCategoryResponseDto>() {
            @Override
            public void onResponse(Call<CropCategoryResponseDto> call, retrofit2.Response<CropCategoryResponseDto> response) {
                //AppLog.d(TAG, response.body().toString());

                if (response.isSuccessful()) {
                    AppUtils.hideprogressbar(getApplicationContext(),progressBar,progressbarInterface);
                    CropCategoryResponseDto cropCategoryResponse = response.body();
                    if (cropCategoryResponse.getStatus() == 0) {

                        categoryList = cropCategoryResponse.getData();

                        for (int i = 0; i < categoryList.size(); i++) {
                            viewPagerTab.addTab(viewPagerTab.newTab().setText(categoryList.get(i).getCropCategory2()));

                        }
                        cropadpter = new CropListAdapter(fragManager, categoryList, viewPagerTab.getTabCount());
                        viewPager.setAdapter(cropadpter);
                        viewPager.addOnPageChangeListener(new
                                TabLayout.TabLayoutOnPageChangeListener(viewPagerTab));
                        viewPager.setOffscreenPageLimit(2);

                    } else if (cropCategoryResponse.getStatus() == 1) {
                        AppUtils.hideprogressbar(getApplicationContext(),progressBar,progressbarInterface);
                        AppUtils.errordialog(CropToolActivity.this, AppConstant.Server_Error);
                    }


                } else {
                    AppUtils.hideprogressbar(getApplicationContext(),progressBar,progressbarInterface);
                    AppUtils.errordialog(CropToolActivity.this, AppConstant.Server_Error);

                }
            }

            @Override
            public void onFailure(Call<CropCategoryResponseDto> call, Throwable t) {

                AppUtils.errordialog(CropToolActivity.this, t.getMessage());
            }
        };

        if (AppUtils.checkInternetConnection(CropToolActivity.this)) {
            ApiManager.getCropCategory(cropCategoryeResponseDtoCallback, AppUtils.apilanguage(CropToolActivity.this));

        } else {
            AppUtils.errordialog(CropToolActivity.this, AppConstant.no_network);
        }
        viewPagerTab.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                getSupportActionBar().setTitle(categoryList.get(tab.getPosition()).getCropCategory2());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }

        });
    }


    @Override
    protected int getToolbar() {
        return R.id.toolbar;
    }

    @Override
    protected int getActivityID() {
        return 0;
    }

    @Override
    protected int getToolbarTitle() {
        return R.string.crop_type;
    }

    @Override
    protected int getResourceLayout() {
        return R.layout.crop_viewpager_layout;
    }

    @Override
    protected int getDrawerLayout() {
        return 0;
    }

    @Override
    protected int getNavigationView() {
        return 0;
    }

    @Override
    protected Activity getInstance() {
        return null;
    }
    @Override
    public void showprogressbar(ProgressBar progressBar) {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideprogressbar(ProgressBar progressBar) {
        progressBar.setVisibility(View.INVISIBLE);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
