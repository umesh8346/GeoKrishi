package com.pathway.geokrishi.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.pathway.geokrishi.ApiController.ResponseDTO.CropCategoryResponseDto;
import com.pathway.geokrishi.fragment.CropFragment;

import java.util.ArrayList;
public class CropListAdapter extends FragmentStatePagerAdapter {
    ArrayList<CropCategoryResponseDto.Data> categoryList;
    int NumOfTabs;
    public CropListAdapter(FragmentManager fm, ArrayList<CropCategoryResponseDto.Data> p,int NumOfTabs) {
        super(fm);
        // TODO Auto-generated constructor stub
        this.categoryList=p;
        this.NumOfTabs=NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        return CropFragment.newInstance(categoryList.get(position).getCropCategoryID());
    }

    @Override
    public int getCount() {
        return categoryList.size();
    }
    @Override
    public int getItemPosition(Object object) {
        // TODO Auto-generated method stub


        return POSITION_NONE;
    }

}
