package com.pathway.geokrishi.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import com.pathway.geokrishi.ApiController.ResponseDTO.CropCalenderDTO;
import com.pathway.geokrishi.R;
import java.util.ArrayList;


public class CropCalenderRecycleAdapter extends RecyclerView.Adapter<CropCalenderRecycleAdapter.ViewHolder> {
    private Context mContext;
    ArrayList<CropCalenderDTO.Crop> croplist = new ArrayList<>();
    String temp;

    public CropCalenderRecycleAdapter(Context c,  ArrayList<CropCalenderDTO.Crop>p) {
        this.mContext = c;
        this.croplist = p;
    }


    @Override
    public int getItemCount() {
        return croplist.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView textcropname;
        public View mTextView;
        public ViewHolder(View v) {
            super(v);
            mTextView = v;
        }

    }


    @Override
    public CropCalenderRecycleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                    int viewType) {
        View vi = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.calender_list_layout, parent, false);
        ViewHolder vh = new ViewHolder(vi);
        vh.textcropname = (TextView) vi.findViewById(R.id.textviewCropName);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textcropname.setText(croplist.get(position).getCropName());

    }
}